import java.io.IOException;
import java.net.Socket;

class Class127 {

   static int[] anIntArray1679 = new int[14];
   static JS5Index identity_kit_config_data_530, identity_kit_config_data_osrs;
   static int[] anIntArray1681;


   static final void handleLogin(byte var0) {
      try {
         if(0 != Class3_Sub13_Sub25.loginStage && 5 != Class3_Sub13_Sub25.loginStage) {
            try {
               if(~(++Class50.anInt820) < -2001) {
                  if(Class3_Sub15.aClass89_2429 != null) {
                     Class3_Sub15.aClass89_2429.close(14821);
                     Class3_Sub15.aClass89_2429 = null;
                  }

                  if(-2 >= ~Class166.anInt2079) {
                     Class158.anInt2005 = -5;
                     Class3_Sub13_Sub25.loginStage = 0;
                     return;
                  }

                  Class50.anInt820 = 0;
                  if(Class140_Sub6.anInt2894 != Class162.anInt2036) {
                     Class140_Sub6.anInt2894 = Class162.anInt2036;
                  } else {
                     Class140_Sub6.anInt2894 = WorldListCountry.anInt506;
                  }

                  Class3_Sub13_Sub25.loginStage = 1;
                  ++Class166.anInt2079;
               }

               if(Class3_Sub13_Sub25.loginStage == 1) {
                  Class3_Sub9.aClass64_2318 = Class38.aClass87_665.method1441((byte)8, Class38_Sub1.aString2611, Class140_Sub6.anInt2894);
                  Class3_Sub13_Sub25.loginStage = 2;
               }

               if(-3 == ~Class3_Sub13_Sub25.loginStage) {
                  if(~Class3_Sub9.aClass64_2318.anInt978 == -3) {
                     throw new IOException();
                  }

                  if(1 != Class3_Sub9.aClass64_2318.anInt978) {
                     return;
                  }

                  Class3_Sub15.aClass89_2429 = new IOHandler((Socket)Class3_Sub9.aClass64_2318.anObject974, Class38.aClass87_665);
                  Class3_Sub9.aClass64_2318 = null;
                  long var1 = Class3_Sub13_Sub16.aLong3202 = Class3_Sub28_Sub14.username.toLong(-106);
                  Class3_Sub13_Sub1.outgoingBuffer.pos = 0;
                  Class3_Sub13_Sub1.outgoingBuffer.putByte((byte)-40, 14);
                  int nameHash = (int)(var1 >> 16 & 31L);
                  Class3_Sub13_Sub1.outgoingBuffer.putByte((byte)-39, nameHash);
                  Class3_Sub15.aClass89_2429.sendBytes(false, 0, Class3_Sub13_Sub1.outgoingBuffer.buffer, 2);
                  if(WorldListEntry.aClass155_2627 != null) {
                     WorldListEntry.aClass155_2627.method2159(106);
                  }

                  if(Class3_Sub21.aClass155_2491 != null) {
                     Class3_Sub21.aClass155_2491.method2159(var0 + 88);
                  }

                  int var4 = Class3_Sub15.aClass89_2429.readByte(var0 ^ -9);
                  if(WorldListEntry.aClass155_2627 != null) {
                     WorldListEntry.aClass155_2627.method2159(68);
                  }

                  if(null != Class3_Sub21.aClass155_2491) {
                     Class3_Sub21.aClass155_2491.method2159(109);
                  }

                  if(~var4 != -1) {
                     Class158.anInt2005 = var4;
                     Class3_Sub13_Sub25.loginStage = 0;
                     Class3_Sub15.aClass89_2429.close(var0 + 14830);
                     Class3_Sub15.aClass89_2429 = null;
                     return;
                  }

                  Class3_Sub13_Sub25.loginStage = 3;
               }

               if(Class3_Sub13_Sub25.loginStage == 3) {
                  if(~Class3_Sub15.aClass89_2429.availableBytes(-18358) > -9) {
                     return;
                  }

                  Class3_Sub15.aClass89_2429.readBytes(0, 8, -18455, GraphicDefinition.incomingBuffer.buffer);
                  GraphicDefinition.incomingBuffer.pos = 0;
                  Class3_Sub13_Sub27.isaacServerKey = GraphicDefinition.incomingBuffer.getLong(-88);
                  int[] var9 = new int[4];
                  Class3_Sub13_Sub1.outgoingBuffer.pos = 0;
                  var9[2] = (int)(Class3_Sub13_Sub27.isaacServerKey >> 32);
                  var9[3] = (int)Class3_Sub13_Sub27.isaacServerKey;
                  var9[1] = (int)(Math.random() * 9.9999999E7D);
                  var9[0] = (int)(Math.random() * 9.9999999E7D);
                  Class3_Sub13_Sub1.outgoingBuffer.putByte((byte)-30, 10);
                  Class3_Sub13_Sub1.outgoingBuffer.putInt(-120, var9[0]);
                  Class3_Sub13_Sub1.outgoingBuffer.putInt(-125, var9[1]);
                  Class3_Sub13_Sub1.outgoingBuffer.putInt(-127, var9[2]);
                  Class3_Sub13_Sub1.outgoingBuffer.putInt(var0 + -111, var9[3]);
                  Class3_Sub13_Sub1.outgoingBuffer.putLong(Class3_Sub28_Sub14.username.toLong(var0 + -116), var0 + -2037491431);
                  Class3_Sub13_Sub1.outgoingBuffer.putString(0, Class3_Sub28_Sub14.password);
                  Class3_Sub13_Sub1.outgoingBuffer.encryptRSA(Class3_Sub13_Sub14.aBigInteger3162, Class3_Sub13_Sub37.aBigInteger3441, -296);
                  class63.aClass3_Sub30_Sub1_2942.pos = 0;
                  if(40 == Class143.anInt1875) {
                     class63.aClass3_Sub30_Sub1_2942.putByte((byte)-81, 18);
                  } else {
                     class63.aClass3_Sub30_Sub1_2942.putByte((byte)-100, 16);
                  }

                  class63.aClass3_Sub30_Sub1_2942.putShort(Class3_Sub13_Sub1.outgoingBuffer.pos + 163 - -Class3_Sub13_Sub33.method326((byte)111, Class163_Sub2.aClass94_2996));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 113, 530);
                  class63.aClass3_Sub30_Sub1_2942.putByte((byte)-114, Class7.anInt2161);
                  class63.aClass3_Sub30_Sub1_2942.putByte((byte)-122, !Class3_Sub28_Sub19.aBoolean3779?0:1);
                  class63.aClass3_Sub30_Sub1_2942.putByte((byte)-103, 1);
                  class63.aClass3_Sub30_Sub1_2942.putByte((byte)-88, Class83.method1411(0));
                  class63.aClass3_Sub30_Sub1_2942.putShort(Class23.anInt454);
                  class63.aClass3_Sub30_Sub1_2942.putShort(Class140_Sub7.anInt2934);
                  class63.aClass3_Sub30_Sub1_2942.putByte((byte)-39, Class3_Sub28_Sub14.anInt3671);
                  Class81.putRandomDataFile(class63.aClass3_Sub30_Sub1_2942, true);
                  class63.aClass3_Sub30_Sub1_2942.putString(0, Class163_Sub2.aClass94_2996);
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 118, Class3_Sub26.anInt2554);
                  class63.aClass3_Sub30_Sub1_2942.putInt(-121, Class84.method1421(-2));
                  Class140_Sub2.aBoolean2705 = true;
                  class63.aClass3_Sub30_Sub1_2942.putShort(Class113.interfacePacketCounter);
                  class63.aClass3_Sub30_Sub1_2942.putInt(-122, JS5Group.index0.getCRCValue((byte)-126));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 115, JS5Group.index1.getCRCValue((byte)-125));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-123, JS5Group.index2.getCRCValue((byte)-128));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-123, JS5Group.index3.getCRCValue((byte)-128));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-123, JS5Group.index4.getCRCValue((byte)-125));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-125, JS5Group.index5.getCRCValue((byte)-123));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-122, JS5Group.index6.getCRCValue((byte)-126));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-121, JS5Group.index7.getCRCValue((byte)-125));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 127, JS5Group.index8.getCRCValue((byte)-125));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 117, JS5Group.index9.getCRCValue((byte)-127));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-123, JS5Group.index10.getCRCValue((byte)-127));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-124, JS5Group.index11.getCRCValue((byte)-118));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-126, JS5Group.index12.getCRCValue((byte)-122));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 115, JS5Group.index13.getCRCValue((byte)-118));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-122, JS5Group.index14.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-121, JS5Group.index15.getCRCValue((byte)-122));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-124, JS5Group.index16.getCRCValue((byte)-123));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 + -117, JS5Group.index17.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-128, JS5Group.index18.getCRCValue((byte)-122));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-124, JS5Group.index19.getCRCValue((byte)-127));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index20.getCRCValue((byte)-123));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index21.getCRCValue((byte)-117));
                  class63.aClass3_Sub30_Sub1_2942.putInt(var0 ^ 127, JS5Group.index22.getCRCValue((byte)-117));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-125, JS5Group.index23.getCRCValue((byte)-122));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-127, JS5Group.index24.getCRCValue((byte)-118));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-126, JS5Group.index25.getCRCValue((byte)-128));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index26.getCRCValue((byte)-123));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index27.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index28.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index29.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index30.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index31.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putInt(-120, JS5Group.index32.getCRCValue((byte)-124));
                  class63.aClass3_Sub30_Sub1_2942.putBytes(Class3_Sub13_Sub1.outgoingBuffer.buffer, 0, Class3_Sub13_Sub1.outgoingBuffer.pos, var0 + 117);
                  Class3_Sub15.aClass89_2429.sendBytes(false, 0, class63.aClass3_Sub30_Sub1_2942.buffer, class63.aClass3_Sub30_Sub1_2942.pos);
                  Class3_Sub13_Sub1.outgoingBuffer.method814(var9, false);

                  for(int var2 = 0; ~var2 > -5; ++var2) {
                     var9[var2] += 50;
                  }

                  GraphicDefinition.incomingBuffer.method814(var9, false);
                  Class3_Sub13_Sub25.loginStage = 4;
               }

               if(-5 == ~Class3_Sub13_Sub25.loginStage) {
                  if(~Class3_Sub15.aClass89_2429.availableBytes(-18358) > -2) {
                     return;
                  }

                  int opcode = Class3_Sub15.aClass89_2429.readByte(0);
                  if(~opcode != -22) {
                     if(opcode != 29) {
                        if(opcode == 1) {
                           Class3_Sub13_Sub25.loginStage = 5;
                           Class158.anInt2005 = opcode;
                           return;
                        }

                        if(2 != opcode) {
                           if(~opcode != -16) {
                              if(23 == opcode && ~Class166.anInt2079 > -2) {
                                 Class3_Sub13_Sub25.loginStage = 1;
                                 ++Class166.anInt2079;
                                 Class50.anInt820 = 0;
                                 Class3_Sub15.aClass89_2429.close(14821);
                                 Class3_Sub15.aClass89_2429 = null;
                                 return;
                              }

                              Class158.anInt2005 = opcode;
                              Class3_Sub13_Sub25.loginStage = 0;
                              Class3_Sub15.aClass89_2429.close(var0 + 14830);
                              Class3_Sub15.aClass89_2429 = null;
                              return;
                           }

                           Class3_Sub13_Sub25.loginStage = 0;
                           Class158.anInt2005 = opcode;
                           return;
                        }

                        Class3_Sub13_Sub25.loginStage = 8;
                     } else {
                        Class3_Sub13_Sub25.loginStage = 10;
                     }
                  } else {
                     Class3_Sub13_Sub25.loginStage = 7;
                  }
               }

               if(6 == Class3_Sub13_Sub25.loginStage) {
                  Class3_Sub13_Sub1.outgoingBuffer.pos = 0;
                  Class3_Sub13_Sub1.outgoingBuffer.putOpcode(17);
                  Class3_Sub15.aClass89_2429.sendBytes(false, 0, Class3_Sub13_Sub1.outgoingBuffer.buffer, Class3_Sub13_Sub1.outgoingBuffer.pos);
                  Class3_Sub13_Sub25.loginStage = 4;
                  return;
               }

               if(Class3_Sub13_Sub25.loginStage == 7) {
                  if(-2 >= ~Class3_Sub15.aClass89_2429.availableBytes(var0 + -18349)) {
                     Class3_Sub13_Sub34.anInt3413 = 60 * (3 + Class3_Sub15.aClass89_2429.readByte(var0 + 9));
                     Class3_Sub13_Sub25.loginStage = 0;
                     Class158.anInt2005 = 21;
                     Class3_Sub15.aClass89_2429.close(var0 + 14830);
                     Class3_Sub15.aClass89_2429 = null;
                     return;
                  }

                  return;
               }

               if(-11 == ~Class3_Sub13_Sub25.loginStage) {
                  if(1 <= Class3_Sub15.aClass89_2429.availableBytes(var0 + -18349)) {
                     Class3_Sub26.anInt2561 = Class3_Sub15.aClass89_2429.readByte(var0 ^ -9);
                     Class3_Sub13_Sub25.loginStage = 0;
                     Class158.anInt2005 = 29;
                     Class3_Sub15.aClass89_2429.close(14821);
                     Class3_Sub15.aClass89_2429 = null;
                     return;
                  }

                  return;
               }

               if(Class3_Sub13_Sub25.loginStage == 8) {
                  if(~Class3_Sub15.aClass89_2429.availableBytes(-18358) > -15) {
                     return;
                  }

                  Class3_Sub15.aClass89_2429.readBytes(0, 14, -18455, GraphicDefinition.incomingBuffer.buffer);
                  GraphicDefinition.incomingBuffer.pos = 0;
                  Client.rights = GraphicDefinition.incomingBuffer.readUnsignedByte();
                  Class3_Sub28_Sub19.anInt3775 = GraphicDefinition.incomingBuffer.readUnsignedByte();
                  Class3_Sub15.aBoolean2433 = GraphicDefinition.incomingBuffer.readUnsignedByte() == 1;
                  Class121.aBoolean1641 = 1 == GraphicDefinition.incomingBuffer.readUnsignedByte();
                  Class3_Sub28_Sub10_Sub1.aBoolean4063 = ~GraphicDefinition.incomingBuffer.readUnsignedByte() == -2;
                  Class3_Sub13_Sub14.aBoolean3166 = 1 == GraphicDefinition.incomingBuffer.readUnsignedByte();
                  Canvas_Sub2.aBoolean29 = GraphicDefinition.incomingBuffer.readUnsignedByte() == 1;
                  ClickMask.localIndex = GraphicDefinition.incomingBuffer.readUnsignedShort();
                  Class3_Sub13_Sub29.disableGEBoxes = GraphicDefinition.incomingBuffer.readUnsignedByte() == 1;
                  Client.isMember = ~GraphicDefinition.incomingBuffer.readUnsignedByte() == -2;
                  Class113.method1702((byte)-124, Client.isMember);
                  JS5Manager.method845(Client.isMember, 255);
                  if(!Class3_Sub28_Sub19.aBoolean3779) {
                     if((!Class3_Sub15.aBoolean2433 || Class3_Sub28_Sub10_Sub1.aBoolean4063) && !Class3_Sub13_Sub29.disableGEBoxes) {
                        try {
                           Class27.aClass94_516.method1577(-1857, Class38.aClass87_665.anApplet1219);
                        } catch (Throwable var5) {
                           ;
                        }
                     } else {
                        try {
                           Class97.aClass94_1374.method1577(-1857, Class38.aClass87_665.anApplet1219);
                        } catch (Throwable var6) {
                           ;
                        }
                     }
                  }

                  RSString.incomingOpcode = GraphicDefinition.incomingBuffer.getOpcode(0);
                  class325.incomingPacketLength = GraphicDefinition.incomingBuffer.readUnsignedShort();
                  Class3_Sub13_Sub25.loginStage = 9;
               }

               if(-10 == ~Class3_Sub13_Sub25.loginStage) {
                  if(~Class3_Sub15.aClass89_2429.availableBytes(-18358) > ~class325.incomingPacketLength) {
                     return;
                  }

                  GraphicDefinition.incomingBuffer.pos = 0;
                  Class3_Sub15.aClass89_2429.readBytes(0, class325.incomingPacketLength, -18455, GraphicDefinition.incomingBuffer.buffer);
                  Class158.anInt2005 = 2;
                  Class3_Sub13_Sub25.loginStage = 0;
                  Class142.method2061(true);
                  Class3_Sub28_Sub7.anInt3606 = -1;
                  Class39.updateSceneGraph(0, false);
                  RSString.incomingOpcode = -1;
                  return;
               }

            } catch (IOException var7) {
               if(null != Class3_Sub15.aClass89_2429) {
                  Class3_Sub15.aClass89_2429.close(14821);
                  Class3_Sub15.aClass89_2429 = null;
               }

               if(Class166.anInt2079 >= 1) {
                  Class3_Sub13_Sub25.loginStage = 0;
                  Class158.anInt2005 = -4;
               } else {
                  Class3_Sub13_Sub25.loginStage = 1;
                  Class50.anInt820 = 0;
                  ++Class166.anInt2079;
                  if(~Class162.anInt2036 == ~Class140_Sub6.anInt2894) {
                     Class140_Sub6.anInt2894 = WorldListCountry.anInt506;
                  } else {
                     Class140_Sub6.anInt2894 = Class162.anInt2036;
                  }
               }
            }

         }
      } catch (RuntimeException var8) {
         throw Class44.method1067(var8, "ri.A(" + var0 + ')');
      }
   }

   static final int method1753(int var0, int var1) {
      var1 = var1 * (var0 & 127) >> 7;
      if(var1 < 2) {
         var1 = 2;
      } else if(var1 > 126) {
         var1 = 126;
      }

      return (var0 & '\uff80') + var1;
   }

   public static void method1754(int var0) {
      try {
         identity_kit_config_data_530 = null;
         identity_kit_config_data_osrs = null;
         anIntArray1679 = null;
         if(var0 >= -49) {
            handleLogin((byte)102);
         }

         anIntArray1681 = null;
      } catch (RuntimeException var2) {
         throw Class44.method1067(var2, "ri.B(" + var0 + ')');
      }
   }

}
