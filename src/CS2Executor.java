import java.io.UnsupportedEncodingException;
import java.util.Date;

public class CS2Executor {

	static void execute_script_osrs(ScriptEvent scriptEvent, int size, int widget_id) {
		if (widget_id > 2000) {
			widget_id = widget_id >> 16;
		}
		Object[] params = scriptEvent.params;
		int script_id = ((Integer) params[0]).intValue();
		Script script = Script.get_script(script_id, true);
		int var18;
		if (script == null) {
			System.out.println("Script data is null.");
		}
		if (script != null) {
			CS2Info.intStackSize = 0;
			CS2Info.stringStackSize = 0;
			var18 = -1;
			int[] instructions = script.instructions;
			int[] intOperands = script.intValues;
			byte var8 = -1;
			CS2Info.subScriptPos = 0;
			// CS2Info.field1078 = false;

			try {
				int var11;
				try {
					CS2Info.scriptLocalInts = new int[script.localIntCount];
					int var9 = 0;
					CS2Info.scriptLocalStrings = new RSString[script.localStringCount];
					int var10 = 0;

					int var12;
					RSString var19;
					for (var11 = 1; var11 < params.length; ++var11) {
						if (params[var11] instanceof Integer) {
							var12 = ((Integer) params[var11]).intValue();
							if (var12 == -2147483647) {
								var12 = scriptEvent.mouseX;
							}

							if (var12 == -2147483646) {
								var12 = scriptEvent.mouseY;
							}

							if (var12 == -2147483645) {
								var12 = scriptEvent.source != null ? scriptEvent.source.hash : -1;
							}

							if (var12 == -2147483644) {
								var12 = scriptEvent.op;
							}

							if (var12 == -2147483643) {
								var12 = scriptEvent.source != null ? scriptEvent.source.index : -1;
							}

							if (var12 == -2147483642) {
								var12 = scriptEvent.target != null ? scriptEvent.target.hash : -1;
							}

							if (var12 == -2147483641) {
								var12 = scriptEvent.target != null ? scriptEvent.target.index : -1;
							}

							if (var12 == -2147483640) {
								var12 = scriptEvent.typedKeyCode;
							}

							if (var12 == -2147483639) {
								var12 = scriptEvent.typedKeyChar;
							}

							CS2Info.scriptLocalInts[var9++] = var12;
						} else if (params[var11] instanceof String) {
							var19 = (RSString) params[var11];
							if (var19.equals("event_opbase")) {
								var19 = scriptEvent.opbase;
							}

							CS2Info.scriptLocalStrings[var10++] = var19;
						}
					}

					var11 = 0;
					CS2Info.field1127 = scriptEvent.field604;

					while (true) {
						++var11;
						if (var11 > size) {
							throw new RuntimeException();
						}

						++var18;
						int opcode = instructions[var18];
						System.out.println("[CS2-OSRS] " + "[widget = " + widget_id + "] Executing opcode " + opcode);
						int var21;
						if (opcode < 100) {
							if (opcode == 0) {
								CS2Info.intStack[++CS2Info.intStackSize - 1] = 1;
								//CS2Info.intStack[++CS2Info.intStackSize - 1] = intOperands[var18];
							} else if (opcode == 1) {
								var12 = intOperands[var18];
								CS2Info.intStack[++CS2Info.intStackSize - 1] = Varp.clientVarps[var12];
							} else if (opcode == 2) {
								var12 = intOperands[var18];
								Varp.clientVarps[var12] = CS2Info.intStack[--CS2Info.intStackSize];
								Varp.processVarpClientCode(var12, 0);
							} else if (opcode == 3) {
								CS2Info.field1118[++CS2Info.stringStackSize - 1] = script.stringValues[var18];
							} else if (opcode == 6) {
								var18 += intOperands[var18];
							} else if (opcode == 7) {
								CS2Info.intStackSize -= 2;
								if (CS2Info.intStack[CS2Info.intStackSize] != CS2Info.intStack[CS2Info.intStackSize
										+ 1]) {
									var18 += intOperands[var18];
								}
							} else if (opcode == 8) {
								CS2Info.intStackSize -= 2;
								if (CS2Info.intStack[CS2Info.intStackSize] == CS2Info.intStack[CS2Info.intStackSize
										+ 1]) {
									var18 += intOperands[var18];
								}
							} else if (opcode == 9) {
								CS2Info.intStackSize -= 2;
								if (CS2Info.intStack[CS2Info.intStackSize] < CS2Info.intStack[CS2Info.intStackSize
										+ 1]) {
									var18 += intOperands[var18];
								}
							} else if (opcode == 10) {
								CS2Info.intStackSize -= 2;
								if (CS2Info.intStack[CS2Info.intStackSize] > CS2Info.intStack[CS2Info.intStackSize
										+ 1]) {
									var18 += intOperands[var18];
								}
							} else if (opcode == 21) {
								if (CS2Info.subScriptPos == 0) {
									return;
								}

								ScriptState script_state = CS2Info.scriptStack[--CS2Info.subScriptPos];
								script = script_state.invokedFromScript;
								instructions = script.instructions;
								intOperands = script.intValues;
								var18 = script_state.invokedFromPc;
								CS2Info.scriptLocalInts = script_state.savedLocalInts;
								CS2Info.scriptLocalStrings = script_state.savedLocalStrings;
							} else if (opcode == 25) {
								var12 = intOperands[var18];
								CS2Info.intStack[++CS2Info.intStackSize - 1] = Varbit.getVarbit(var12);
							} else if (opcode == 27) {
								var12 = intOperands[var18];
								Varbit.setVarbit(var12, CS2Info.intStack[--CS2Info.intStackSize]);
							} else if (opcode == 31) {
								CS2Info.intStackSize -= 2;
								if (CS2Info.intStack[CS2Info.intStackSize] <= CS2Info.intStack[CS2Info.intStackSize
										+ 1]) {
									var18 += intOperands[var18];
								}
							} else if (opcode == 32) {
								CS2Info.intStackSize -= 2;
								if (CS2Info.intStack[CS2Info.intStackSize] >= CS2Info.intStack[CS2Info.intStackSize
										+ 1]) {
									var18 += intOperands[var18];
								}
							} else if (opcode == 33) {
								CS2Info.intStack[++CS2Info.intStackSize
										- 1] = CS2Info.scriptLocalInts[intOperands[var18]];
							} else if (opcode == 34) {
								CS2Info.scriptLocalInts[intOperands[var18]] = CS2Info.intStack[--CS2Info.intStackSize];
							} else if (opcode == 35) {
								CS2Info.field1118[++CS2Info.stringStackSize
										- 1] = CS2Info.scriptLocalStrings[intOperands[var18]];
							} else if (opcode == 36) {
								CS2Info.scriptLocalStrings[intOperands[var18]] = CS2Info.field1118[--CS2Info.stringStackSize];
							} else if (opcode == 37) {
								var12 = intOperands[var18];
								CS2Info.stringStackSize -= var12;
								RSString var13 = class208.method3900(CS2Info.field1118, CS2Info.stringStackSize, var12,
										(short) 313);
								CS2Info.field1118[++CS2Info.stringStackSize - 1] = var13;
							} else if (opcode == 38) {
								--CS2Info.intStackSize;
							} else if (opcode == 39) {
								--CS2Info.stringStackSize;
							} else {
								int var16;
								if (opcode == 40) {
									var12 = intOperands[var18];
									Script var31 = Script.get_script(var12, true);
									int[] var14 = new int[var31.localIntCount];
									RSString[] var15 = new RSString[var31.localStringCount];

									for (var16 = 0; var16 < var31.intArgumentCount; ++var16) {
										var14[var16] = CS2Info.intStack[var16
												+ (CS2Info.intStackSize - var31.intArgumentCount)];
									}

									for (var16 = 0; var16 < var31.stringArgumentCount; ++var16) {
										var15[var16] = CS2Info.field1118[var16
												+ (CS2Info.stringStackSize - var31.stringArgumentCount)];
									}

									CS2Info.intStackSize -= var31.intArgumentCount;
									CS2Info.stringStackSize -= var31.stringArgumentCount;
									ScriptState var20 = new ScriptState();
									var20.invokedFromScript = script;
									var20.invokedFromPc = var18;
									var20.savedLocalInts = CS2Info.scriptLocalInts;
									var20.savedLocalStrings = CS2Info.scriptLocalStrings;
									CS2Info.scriptStack[++CS2Info.subScriptPos - 1] = var20;
									script = var31;
									instructions = var31.instructions;
									intOperands = var31.intValues;
									var18 = -1;
									CS2Info.scriptLocalInts = var14;
									CS2Info.scriptLocalStrings = var15;
								} else if (opcode == 42) {
									CS2Info.intStack[CS2Info.intStackSize++] = Varbit.anIntArray1277[intOperands[var18]];
								} else if (opcode == 43) {
									int j4 = intOperands[var18];
									Varbit.anIntArray1277[j4] = CS2Info.intStack[--CS2Info.intStackSize];
									PacketParser.method825((byte) 92, j4);
								} else if (opcode == 44) {
									var12 = intOperands[var18] >> 16;
									var21 = intOperands[var18] & '\uffff';
									int var22 = CS2Info.intStack[--CS2Info.intStackSize];
									if (var22 < 0 || var22 > 5000) {
										throw new RuntimeException();
									}

									CS2Info.field1114[var12] = var22;
									byte var23 = -1;
									if (var21 == 105) {
										var23 = 0;
									}

									for (var16 = 0; var16 < var22; ++var16) {
										CS2Info.field1122[var12][var16] = var23;
									}
								} else if (opcode == 45) {
									var12 = intOperands[var18];
									var21 = CS2Info.intStack[--CS2Info.intStackSize];
									if (var21 < 0 || var21 >= CS2Info.field1114[var12]) {
										throw new RuntimeException();
									}

									CS2Info.intStack[++CS2Info.intStackSize - 1] = CS2Info.field1122[var12][var21];
								} else if (opcode == 46) {
									var12 = intOperands[var18];
									CS2Info.intStackSize -= 2;
									var21 = CS2Info.intStack[CS2Info.intStackSize];
									if (var21 < 0 || var21 >= CS2Info.field1114[var12]) {
										throw new RuntimeException();
									}

									CS2Info.field1122[var12][var21] = CS2Info.intStack[CS2Info.intStackSize + 1];
								} else if (opcode == 47) {
									RSString class94_1 = Class132.aClass94Array1739[intOperands[var18]];
									if (null == class94_1)
										class94_1 = Class140_Sub7.aClass94_2928;
									CS2Info.field1118[CS2Info.stringStackSize++] = class94_1;
								} else if (opcode == 48) {
									int j5 = intOperands[18];
									Class132.aClass94Array1739[j5] = CS2Info.field1118[--CS2Info.stringStackSize];
									Class49.method1126(-94, j5);
								} else if (opcode == 49) {
									var19 = Class3_Sub4.createRSString("intOperands[var18]: " + intOperands[var18],
											(byte) -124);
									CS2Info.field1118[++CS2Info.stringStackSize - 1] = var19;
								} else if (opcode == 50) {
									--CS2Info.stringStackSize; // TODO
									// class160.field1983.method2272(intOperands[var18],
									// CS2Info.field1118[--CS2Info.stringStackSize], 1504120549);
								} else {
									if (opcode != 60) {
										throw new IllegalStateException();
									}

									/*
									 * class325 var35 = script.field1304[intOperands[var18]]; class188 var32 =
									 * (class188)var35.method5968((long)CS2Info.intStack[--CS2Info.intStackSize]);
									 * if(var32 != null) { var18 += var32.field2114; }
									 */
									--CS2Info.intStackSize; // TODO
								}
							}
						} else {
							boolean var34;
							if (script.intValues[var18] == 1) {
								var34 = true;
							} else {
								var34 = false;
							}

							var21 = process_op(opcode, script, var34);
							switch (var21) {
							case 0:
								return;
							case 1:
							default:
								System.out.println("[CS2-OSRS] Missing CS2 opcode " + opcode);
								break;
							case 2:
								System.err.println("[CS2-OSRS] Error processing CS2 opcode " + opcode);
								break;
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("[CS2-OSRS] Error decoding CS2-script.");
				}
			} finally {
				/*
				 * if(CS2Info.field1078) { CS2Info.field1126 = true;
				 * class227.method4160(1891530232); CS2Info.field1126 = false; CS2Info.field1078
				 * = false; }
				 */

			}
		}
	}

	static int process_op_under_3300(int opcode, Script script, boolean flag) {
		if (opcode == 3200) {
			CS2Info.intStackSize -= 3;
			Class3_Sub13_Sub6.method199(CS2Info.intStack[CS2Info.intStackSize],
					CS2Info.intStack[CS2Info.intStackSize - -1], CS2Info.intStack[CS2Info.intStackSize + 2], -799);
			return 1;
		} else if (opcode == 3201) {
			Class86.method1427(true, CS2Info.intStack[--CS2Info.intStackSize]);
			return 1;
		} else if (opcode == 3202) {
			CS2Info.intStackSize -= 2;
			Class167.method2266(CS2Info.intStack[1 + CS2Info.intStackSize], CS2Info.intStack[CS2Info.intStackSize],
					(byte) -1);
			return 1;
		} else {
			return 2;
		}
	}

	static int process_op_under_2700(int opcode, Script script, boolean flag) {
		Widget var4 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
		if (opcode == 2600) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2646;
			return 1;
		} else if (opcode == 2601) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2589;
			return 1;
		} else if (opcode == 2602) {
			CS2Info.field1118[++CS2Info.stringStackSize - 1] = var4.text;
			return 1;
		} else if (opcode == 2603) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.scrollWidth;
			return 1;
		} else if (opcode == 2604) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.scrollHeight;
			return 1;
		} else if (opcode == 2605) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.modelZoom;
			return 1;
		} else if (opcode == 2606) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.rotationX;
			return 1;
		} else if (opcode == 2607) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.rotationZ;
			return 1;
		} else if (opcode == 2608) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.rotationY;
			return 1;
		} else if (opcode == 2609) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.opacity;
			return 1;
		} else if (opcode == 2610) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2914;
			return 1;
		} else if (opcode == 2611) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.textColor;
			return 1;
		} else if (opcode == 2612) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.spriteId;
			return 1;
		} else if (opcode == 2613) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = /* var4.field2651.vmethod6149((byte)21) */1;
			return 1;
		} else if (opcode == 2614) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2625 ? 1 : 0;
			return 1;
		} else {
			return 2;
		}
	}
	
	static int process_op_under_1600(int opcode, Script script, boolean flag) {
	      Widget var4 = flag?Class20.field115:CS2Info.field1130;
	      if(opcode == 1500) {
	         CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2580;
	         return 1;
	      } else if(opcode == 1501) {
	         CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2653;
	         return 1;
	      } else if(opcode == 1502) {
	         CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2582;
	         return 1;
	      } else if(opcode == 1503) {
	         CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.field2642;
	         return 1;
	      } else if(opcode == 1504) {
	         CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.isHidden?1:0;
	         return 1;
	      } else if(opcode == 1505) {
	         CS2Info.intStack[++CS2Info.intStackSize - 1] = var4.parentId;
	         return 1;
	      } else {
	         return 2;
	      }
	   }
	
	static int process_op_under_1400(int opcode, Script script, boolean flag) {
		boolean var4 = true;
		Widget var5;
		if (opcode >= 2000) {
			opcode -= 1000;
			var5 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
			var4 = false;
		} else {
			var5 = flag ? Class20.field115 : CS2Info.field1130;
		}

		int var12;
		if (opcode == 1300) {
			var12 = CS2Info.intStack[--CS2Info.intStackSize] - 1;
			if (var12 >= 0 && var12 <= 9) {
				var5.method857(var12, CS2Info.field1118[--CS2Info.stringStackSize]);
				return 1;
			} else {
				--CS2Info.stringStackSize;
				return 1;
			}
		} else {
			int var7;
			if (opcode == 1301) {
				CS2Info.intStackSize -= 2;
				var12 = CS2Info.intStack[CS2Info.intStackSize];
				var7 = CS2Info.intStack[CS2Info.intStackSize + 1];
				var5.field2648 = Class19.method224(var12, var7, (byte) 0);
				return 1;
			} else if (opcode == 1302) {
				var5.dragRenderBehavior = CS2Info.intStack[--CS2Info.intStackSize] == 1;
				return 1;
			} else if (opcode == 1303) {
				var5.dragDeadZone = CS2Info.intStack[--CS2Info.intStackSize];
				return 1;
			} else if (opcode == 1304) {
				var5.dragDeadTime = CS2Info.intStack[--CS2Info.intStackSize];
				return 1;
			} else if (opcode == 1305) {
				var5.name = CS2Info.field1118[--CS2Info.stringStackSize];
				return 1;
			} else if (opcode == 1306) {
				var5.targetVerb = CS2Info.field1118[--CS2Info.stringStackSize];
				return 1;
			} else if (opcode == 1307) {
				var5.actions = null;
				return 1;
			} else if (opcode == 1308) {
				boolean field2658 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
				return 1;
			} else if (opcode == 1309) {
				--CS2Info.intStackSize;
				return 1;
			} else {
				int var8;
				byte[] var10;
				if (opcode == 1350) {
					byte[] var9 = null;
					var10 = null;
					if (var4) {
						CS2Info.intStackSize -= 10;

						for (var8 = 0; var8 < 10 && CS2Info.intStack[var8 + CS2Info.intStackSize] >= 0; var8 += 2) {
							;
						}

						if (var8 > 0) {
							var9 = new byte[var8 / 2];
							var10 = new byte[var8 / 2];

							for (var8 -= 2; var8 >= 0; var8 -= 2) {
								var9[var8 / 2] = (byte) CS2Info.intStack[var8 + CS2Info.intStackSize];
								var10[var8 / 2] = (byte) CS2Info.intStack[var8 + CS2Info.intStackSize + 1];
							}
						}
					} else {
						CS2Info.intStackSize -= 2;
						var9 = new byte[] { (byte) CS2Info.intStack[CS2Info.intStackSize] };
						var10 = new byte[] { (byte) CS2Info.intStack[CS2Info.intStackSize + 1] };
					}

					var8 = CS2Info.intStack[--CS2Info.intStackSize] - 1;
					if (var8 >= 0 && var8 <= 9) {
						//class301.method5356(var5, var8, var9, var10, -481571835);
						return 1;
					} else {
						throw new RuntimeException();
					}
				} else {
					byte var6;
					if (opcode == 1351) {
						CS2Info.intStackSize -= 2;
						var6 = 10;
						var10 = new byte[] { (byte) CS2Info.intStack[CS2Info.intStackSize] };
						byte[] var11 = new byte[] { (byte) CS2Info.intStack[CS2Info.intStackSize + 1] };
						//class301.method5356(var5, var6, var10, var11, -481571835);
						return 1;
					} else if (opcode == 1352) {
						CS2Info.intStackSize -= 3;
						var12 = CS2Info.intStack[CS2Info.intStackSize] - 1;
						var7 = CS2Info.intStack[CS2Info.intStackSize + 1];
						var8 = CS2Info.intStack[CS2Info.intStackSize + 2];
						if (var12 >= 0 && var12 <= 9) {
							//class63.method1163(var5, var12, var7, var8, -951911792);
							return 1;
						} else {
							throw new RuntimeException();
						}
					} else if (opcode == 1353) {
						var6 = 10;
						var7 = CS2Info.intStack[--CS2Info.intStackSize];
						var8 = CS2Info.intStack[--CS2Info.intStackSize];
						//class63.method1163(var5, var6, var7, var8, -951911792);
						return 1;
					} else if (opcode == 1354) {
						--CS2Info.intStackSize;
						var12 = CS2Info.intStack[CS2Info.intStackSize] - 1;
						if (var12 >= 0 && var12 <= 9) {
							//class195.method3679(var5, var12, (short) 10820);
							return 1;
						} else {
							throw new RuntimeException();
						}
					} else if (opcode == 1355) {
						var6 = 10;
						//class195.method3679(var5, var6, (short) -23676);
						return 1;
					} else {
						return 2;
					}
				}
			}
		}
	}
	
	static int process_op_under_1500(int opcode, Script script, boolean flag) {
		Widget var4;
		if (opcode >= 2000) {
			opcode -= 1000;
			var4 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
		} else {
			var4 = flag ? Class20.field115 : CS2Info.field1130;
		}

		RSString var5 = CS2Info.field1118[--CS2Info.stringStackSize];
		int[] var6 = null;
		if (var5.length(33) > 0 && var5.charAt(var5.length(33) - 1, (byte)0) == 89) {
			int var7 = CS2Info.intStack[--CS2Info.intStackSize];
			if (var7 > 0) {
				for (var6 = new int[var7]; var7-- > 0; var6[var7] = CS2Info.intStack[--CS2Info.intStackSize]) {
					;
				}
			}

			var5 = var5.substring(var5.length(33) - 1);
		}

		Object[] var9 = new Object[var5.length(33) + 1];

		int var8;
		for (var8 = var9.length - 1; var8 >= 1; --var8) {
			if (var5.charAt(var8 - 1, (byte) 0) == 115) {
				var9[var8] = CS2Info.field1118[--CS2Info.stringStackSize];
			} else {
				var9[var8] = new Integer(CS2Info.intStack[--CS2Info.intStackSize]);
			}
		}

		var8 = CS2Info.intStack[--CS2Info.intStackSize];
		if (var8 != -1) {
			var9[0] = new Integer(var8);
		} else {
			var9 = null;
		}

		if (opcode == 1400) {
			var4.onClickListener = var9;
		} else if (opcode == 1401) {
			var4.onHoldListener = var9;
		} else if (opcode == 1402) {
			var4.onReleaseListener = var9;
		} else if (opcode == 1403) {
			var4.onMouseOverListener = var9;
		} else if (opcode == 1404) {
			var4.onMouseLeaveListener = var9;
		} else if (opcode == 1405) {
			var4.onDragListener = var9;
		} else if (opcode == 1406) {
			var4.onTargetLeaveListener = var9;
		} else if (opcode == 1407) {
			var4.varTransmitTriggers = var6;
			var4.onVarTransmitListener = var9;
		} else if (opcode == 1408) {
			var4.onTimerListener = var9;
		} else if (opcode == 1409) {
			var4.onOpListener = var9;
		} else if (opcode == 1410) {
			var4.onDragCompleteListener = var9;
		} else if (opcode == 1411) {
			var4.onClickRepeatListener = var9;
		} else if (opcode == 1412) {
			var4.onMouseRepeatListener = var9;
		} else if (opcode == 1414) {
			var4.invTransmitTriggers = var6;
			var4.onInvTransmitListener = var9;
		} else if (opcode == 1415) {
			var4.statTransmitTriggers = var6;
			var4.onStatTransmitListener = var9;
		} else if (opcode == 1416) {
			var4.onTargetEnterListener = var9;
		} else if (opcode == 1417) {
			var4.onScrollWheelListener = var9;
		} else if (opcode == 1418) {
			var4.anObjectArray256 = var9;
		} else if (opcode == 1419) {
			var4.anObjectArray220 = var9;
		} else if (opcode == 1420) {
			var4.anObjectArray156 = var9;
		} else if (opcode == 1421) {
			var4.anObjectArray313 = var9;
		} else if (opcode == 1422) {
			var4.anObjectArray315 = var9;
		} else if (opcode == 1423) {
			var4.anObjectArray206 = var9;
		} else if (opcode == 1424) {
			var4.anObjectArray176 = var9;
		} else if (opcode == 1425) {
			var4.anObjectArray268 = var9;
		} else if (opcode == 1426) {
			var4.anObjectArray217 = var9;
		} else {
			if (opcode != 1427) {
				return 2;
			}

			var4.anObjectArray235 = var9;
		}

		//var4.field2670 = true;
		return 1;
	}
	
	static int process_op_under_1300(int opcode, Script script, boolean flag) {
		Widget var4;
		if (opcode >= 2000) {
			opcode -= 1000;
			var4 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
		} else {
			var4 = flag ? Class20.field115 : CS2Info.field1130;
		}

		Class68.method909(var4);
		if (opcode != 1200 && opcode != 1205 && opcode != 1212) {
			if (opcode == 1201) {
				var4.modelType = 2;
				var4.modelId = CS2Info.intStack[--CS2Info.intStackSize];
				return 1;
			} else if (opcode == 1202) {
				var4.modelType = 3;
				var4.modelId = Client.field3717.field646.method4071(2000496598);
				return 1;
			} else {
				return 2;
			}
		} else {
			CS2Info.intStackSize -= 2;
			int var5 = CS2Info.intStack[CS2Info.intStackSize];
			int var6 = CS2Info.intStack[CS2Info.intStackSize + 1];
			//var4.field2704 = var5;
			//var4.field2694 = var6;
			//class264 var7 = class30.method373(var5, (byte) 53);
			//var4.rotationX = var7.field3411;
			//var4.rotationY = var7.field3429;
			//var4.rotationZ = var7.field3413;
			//var4.anInt258 = var7.field3414;
			//var4.anInt264 = var7.field3445;
			//var4.modelZoom = var7.field3410;
			if (opcode == 1205) {
				
				var4.aBoolean227 = false;
			} else if (opcode == 1212) {
				var4.aBoolean227 = true;
			} else {
				int field2626 = 2;
			}

			if (var4.modelHeightOverride > 0) {
				var4.modelZoom = var4.modelZoom * 32 / var4.modelHeightOverride;
			} else if (var4.originalWidth > 0) {
				var4.modelZoom = var4.modelZoom * 32 / var4.originalWidth;
			}

			return 1;
		}
	}

	static int process_op_under_1200(int opcode, Script script, boolean flag) {
		int var5 = -1;
		Widget var4;
		if (opcode >= 2000) {
			opcode -= 1000;
			var5 = CS2Info.intStack[--CS2Info.intStackSize];
			var4 = Widget.get_widget(var5);
		} else {
			var4 = flag ? Class20.field115 : CS2Info.field1130;
		}

		if (opcode == 1100) {
			CS2Info.intStackSize -= 2;
			var4.field2646 = CS2Info.intStack[CS2Info.intStackSize];
			if (var4.field2646 > var4.scrollWidth - var4.field2582) {
				var4.field2646 = var4.scrollWidth - var4.field2582;
			}

			if (var4.field2646 < 0) {
				var4.field2646 = 0;
			}

			var4.field2589 = CS2Info.intStack[CS2Info.intStackSize + 1];
			if (var4.field2589 > var4.scrollHeight - var4.field2642) {
				var4.field2589 = var4.scrollHeight - var4.field2642;
			}

			if (var4.field2589 < 0) {
				var4.field2589 = 0;
			}

			Class68.method909(var4);
			return 1;
		} else if (opcode == 1101) {
			var4.textColor = CS2Info.intStack[--CS2Info.intStackSize];
			Class68.method909(var4);
			System.out.println("used 1101 lolface");
			return 1;
		} else if (opcode == 1102) {
			var4.filled = CS2Info.intStack[--CS2Info.intStackSize] == 1;
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1103) {
			var4.opacity = CS2Info.intStack[--CS2Info.intStackSize];
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1104) {
			var4.lineWidth = CS2Info.intStack[--CS2Info.intStackSize];
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1105) {
			var4.spriteId = CS2Info.intStack[--CS2Info.intStackSize];
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1106) {
			var4.textureId = CS2Info.intStack[--CS2Info.intStackSize];
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1107) {
			var4.spriteTiling = CS2Info.intStack[--CS2Info.intStackSize] == 1;
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1108) {
			var4.modelType = 1;
			var4.modelId = CS2Info.intStack[--CS2Info.intStackSize];
			Class68.method909(var4);
			return 1;
		} else if (opcode == 1109) {
			CS2Info.intStackSize -= 6;
			var4.field2914 = CS2Info.intStack[CS2Info.intStackSize];
			var4.anInt264 = CS2Info.intStack[CS2Info.intStackSize + 1];
			var4.rotationX = CS2Info.intStack[CS2Info.intStackSize + 2];
			var4.rotationY = CS2Info.intStack[CS2Info.intStackSize + 3];
			var4.rotationZ = CS2Info.intStack[CS2Info.intStackSize + 4];
			var4.modelZoom = CS2Info.intStack[CS2Info.intStackSize + 5];
			Class68.method909(var4);
			return 1;
		} else {
			int var9;
			if (opcode == 1110) {
				var9 = CS2Info.intStack[--CS2Info.intStackSize];
				if (var9 != var4.animation) {
					var4.animation = var9;
					var4.anInt283 = 0;
					var4.anInt267 = 0;
					Class68.method909(var4);
				}

				return 1;
			} else if (opcode == 1111) {
				var4.orthogonal = CS2Info.intStack[--CS2Info.intStackSize] == 1;
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1112) {
				RSString var8 = CS2Info.field1118[--CS2Info.stringStackSize];
				if (!var8.equals(var4.text)) {
					var4.text = var8;
					Class68.method909(var4);
				}

				return 1;
			} else if (opcode == 1113) {
				var4.fontId = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1114) {
				CS2Info.intStackSize -= 3;
				var4.xTextAlignment = CS2Info.intStack[CS2Info.intStackSize];
				var4.yTextAlignment = CS2Info.intStack[CS2Info.intStackSize + 1];
				var4.lineHeight = CS2Info.intStack[CS2Info.intStackSize + 2];
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1115) {
				var4.textShadowed = CS2Info.intStack[--CS2Info.intStackSize] == 1;
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1116) {
				var4.borderType = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1117) {
				var4.shadowColor = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1118) {
				var4.flippedVertically = CS2Info.intStack[--CS2Info.intStackSize] == 1;
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1119) {
				var4.flippedHorizontally = CS2Info.intStack[--CS2Info.intStackSize] == 1;
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1120) {
				CS2Info.intStackSize -= 2;
				var4.scrollWidth = CS2Info.intStack[CS2Info.intStackSize];
				var4.scrollHeight = CS2Info.intStack[CS2Info.intStackSize + 1];
				Class68.method909(var4);
				if (var5 != -1 && var4.type == 0) {
					class63.method1161(var4, false, -116);
				}

				return 1;
			} else if (opcode == 1121) {
				method968(var4.hash, var4.index, -1252458216);
				Widget field882 = var4;
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1122) {
				var4.alternateSpriteId = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1123) {
				var4.modelZoom = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(var4);
				if (var4.index == -1)
					Class3_Sub13_Sub19.method265((byte) -42, var4.hash);
				return 1;
			} else if (opcode == 1124) {
				var4.field2599 = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(var4);
				return 1;
			} else if (opcode == 1125) {
				var9 = CS2Info.intStack[--CS2Info.intStackSize];
				/*
				 * class332 var7 = (class332)class16.method159(class208.method3899(-133902824),
				 * var9, -1609956543); if(var7 != null) { var4.field2651 = var7;
				 * Class68.method909(345207759, var4); }
				 */

				return 1;
			} else {
				boolean var6;
				if (opcode == 1126) {
					var6 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					var4.field2601 = var6;
					return 1;
				} else if (opcode == 1127) {
					var6 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					var4.field2625 = var6;
					return 1;
				} else {
					return 2;
				}
			}
		}
	}

	private static void method968(int hash, int index, int i) {
		// TODO Auto-generated method stub

	}

	static int process_op_under_3400(int opcode, Script script, boolean flag) {
		if (opcode == 3300) {
			CS2Info.intStack[++CS2Info.intStackSize - 1] = Class44.anInt719;
			return 1;
		} else {
			int var4;
			int var5;
			if (opcode == 3301) {
				CS2Info.intStackSize -= 2;
				var4 = CS2Info.intStack[CS2Info.intStackSize];
				var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
				CS2Info.intStack[++CS2Info.intStackSize - 1] = Widget.method861(var4, 1010005518, var5);
				return 1;
			} else if (opcode == 3302) {
				CS2Info.intStackSize -= 2;
				var4 = CS2Info.intStack[CS2Info.intStackSize];
				var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
				CS2Info.intStack[++CS2Info.intStackSize - 1] = class196.method872(1872697875, var4, var5);
				return 1;
			} else if (opcode == 3303) {
				CS2Info.intStackSize -= 2;
				int j47 = CS2Info.intStack[CS2Info.intStackSize - -1];
				int k8 = CS2Info.intStack[CS2Info.intStackSize];
				CS2Info.intStack[CS2Info.intStackSize++] = Class167.method2268(k8, j47);
				return 1;
			} else if (opcode == 3304) {
				int l8 = CS2Info.intStack[--CS2Info.intStackSize];
				CS2Info.intStack[CS2Info.intStackSize++] = class231.method4193(l8, -127).anInt3647;
				return 1;
			} else if (opcode == 3305) {
				var4 = CS2Info.intStack[--CS2Info.intStackSize];
				CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field686[var4];
				return 1;
			} else if (opcode == 3306) {
				var4 = CS2Info.intStack[--CS2Info.intStackSize];
				CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field752[var4];
				return 1;
			} else if (opcode == 3307) {
				var4 = CS2Info.intStack[--CS2Info.intStackSize];
				CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field821[var4];
				return 1;
			} else {
				int var6;
				if (opcode == 3308) {
					var4 = Client.localPlane;
					var5 = (Client.field3717.field983 >> 7) + Client.field590 * 731242929;
					var6 = (Client.field3717.field973 >> 7) + Client.field1152 * 1825982697;
					CS2Info.intStack[++CS2Info.intStackSize - 1] = (var5 << 14) + var6 + (var4 << 28);
					return 1;
				} else if (opcode == 3309) {
					var4 = CS2Info.intStack[--CS2Info.intStackSize];
					CS2Info.intStack[++CS2Info.intStackSize - 1] = var4 >> 14 & 16383;
					return 1;
				} else if (opcode == 3310) {
					var4 = CS2Info.intStack[--CS2Info.intStackSize];
					CS2Info.intStack[++CS2Info.intStackSize - 1] = var4 >> 28;
					return 1;
				} else if (opcode == 3311) {
					var4 = CS2Info.intStack[--CS2Info.intStackSize];
					CS2Info.intStack[++CS2Info.intStackSize - 1] = var4 & 16383;
					return 1;
				} else if (opcode == 3312) {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.isMember ? 1 : 0;
					return 1;
				} else if (opcode == 3313) {
					CS2Info.intStackSize -= 2;
					var4 = CS2Info.intStack[CS2Info.intStackSize] + '\u8000';
					var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Widget.method861(var4, 1010005518, var5);
					return 1;
				} else if (opcode == 3314) {
					CS2Info.intStackSize -= 2;
					var4 = CS2Info.intStack[CS2Info.intStackSize] + '\u8000';
					var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
					CS2Info.intStack[++CS2Info.intStackSize - 1] = class196.method872(1872697875, var4, var5);
					return 1;
				} else if (opcode == 3315) {
					CS2Info.intStackSize -= 2;
					var4 = CS2Info.intStack[CS2Info.intStackSize] + '\u8000';
					var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Class167.method2268(var4, var5);
					return 1;
				} else if (opcode == 3316) {
					if (Client.rights >= 2) {
						CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.rights;
					} else {
						CS2Info.intStack[++CS2Info.intStackSize - 1] = 0;
					}

					return 1;
				} else if (opcode == 3317) {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field917;
					return 1;
				} else if (opcode == 3318) {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field680;
					return 1;
				} else if (opcode == 3321) {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field847;
					return 1;
				} else if (opcode == 3322) {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = Client.field848;
					return 1;
				} else if (opcode == 3323) {
					if (Class3_Sub28_Sub19.anInt3775 >= 5 && Class3_Sub28_Sub19.anInt3775 <= 9) {
						CS2Info.intStack[CS2Info.intStackSize++] = 1;
					} else {
						CS2Info.intStack[CS2Info.intStackSize++] = 0;
					}

					return 1;
				} else if (opcode == 3324) {
					if (Class3_Sub28_Sub19.anInt3775 >= 5 && Class3_Sub28_Sub19.anInt3775 <= 9) {
						CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub19.anInt3775;
					} else {
						CS2Info.intStack[CS2Info.intStackSize++] = 0;
					}
					return 1;
				} else if (opcode == 3325) {
					CS2Info.intStackSize -= 4;
					var4 = CS2Info.intStack[CS2Info.intStackSize];
					var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
					var6 = CS2Info.intStack[CS2Info.intStackSize + 2];
					int var7 = CS2Info.intStack[CS2Info.intStackSize + 3];
					var4 += var5 << 14;
					var4 += var6 << 28;
					var4 += var7;
					CS2Info.intStack[++CS2Info.intStackSize - 1] = var4;
					return 1;
				} else {
					return 2;
				}
			}
		}
	}

	static int process_op_under_1100(int opcode, Script script, boolean flag) {
		int var4 = -1;
		Widget var5;
		if (opcode >= 2000) {
			opcode -= 1000;
			var4 = CS2Info.intStack[--CS2Info.intStackSize];
			var5 = Widget.get_widget(var4);
		} else {
			var5 = flag ? Class20.field115 : CS2Info.field1130;
		}

		if (opcode == 1000) {
			CS2Info.intStackSize -= 4;
			var5.originalX = CS2Info.intStack[CS2Info.intStackSize];
			var5.originalY = CS2Info.intStack[CS2Info.intStackSize + 1];
			var5.xPositionMode = (byte) CS2Info.intStack[CS2Info.intStackSize + 2];
			var5.yPositionMode = (byte) CS2Info.intStack[CS2Info.intStackSize + 3];
			Class68.method909(var5);
			Class3_Sub13_Sub12.method225(14, var5);
			if (var5.index == -1)
				Class168.method2280(2714, var5.hash);

			return 1;
		} else if (opcode == 1001) {
			CS2Info.intStackSize -= 4;
			var5.originalWidth = CS2Info.intStack[CS2Info.intStackSize];
			var5.originalHeight = CS2Info.intStack[CS2Info.intStackSize + 1];
			var5.modelHeightOverride = CS2Info.intStack[CS2Info.intStackSize + 2];
			var5.field2683 = CS2Info.intStack[CS2Info.intStackSize + 3];
			Class68.method909(var5);
			Class3_Sub13_Sub12.method225(14, var5);
			if (var5.type == 0) {
				class63.method1161(var5, false, 32);
			}
			return 1;
		} else if (opcode == 1003) {
			boolean var6 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
			if (var6 != var5.isHidden) {
				var5.isHidden = var6;
				Class68.method909(var5);
			}

			return 1;
		} else if (opcode == 1005) {
			boolean noClickThrough = CS2Info.intStack[--CS2Info.intStackSize] == 1;
			try {
				var5.noClickThrough = noClickThrough;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 1;
		} else if (opcode == 1006) {
			var5.noScrollThrough = CS2Info.intStack[--CS2Info.intStackSize] == 1;
			return 1;
		} else {
			return 2;
		}
	}

	static int process_op_under_1000(int opcode, Script script, boolean flag) {
		int var4;
		int var5;
		if (opcode == 100) {
			CS2Info.intStackSize -= 3;
			var4 = CS2Info.intStack[CS2Info.intStackSize];
			var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
			int var6 = CS2Info.intStack[CS2Info.intStackSize + 2];
			if (var5 == 0) {
				throw new RuntimeException();
			} else {
				Widget var7 = Widget.get_widget(var4);
				if (var7.children == null) {
					var7.children = new Widget[var6 + 1];
				}

				if (var7.children.length <= var6) {
					Widget[] var8 = new Widget[var6 + 1];

					for (int var9 = 0; var9 < var7.children.length; ++var9) {
						var8[var9] = var7.children[var9];
					}

					var7.children = var8;
				}

				Widget var13 = new Widget();
				var13.type = var5;
				var13.parentId = var13.hash = var7.hash;
				var13.index = var6;
				var13.isIf3 = true;
				var7.children[var6] = var13;
				if (flag) {
					Class20.field115 = var13;
				} else {
					CS2Info.field1130 = var13;
				}

				Class68.method909(var7);
				return 1;
			}
		} else {
			Widget var10;
			if (opcode == 101) {
				var10 = flag ? Class20.field115 : CS2Info.field1130;
				Widget var11 = Widget.get_widget(var10.hash);
				var11.children[var10.index] = null;
				Class68.method909(var11);
				return 1;
			} else if (opcode == 102) {
				var10 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
				var10.children = null;
				Class68.method909(var10);
				return 1;
			} else if (opcode == 200) {
				CS2Info.intStackSize -= 2;
				var4 = CS2Info.intStack[CS2Info.intStackSize];
				var5 = CS2Info.intStack[CS2Info.intStackSize + 1];
				Widget var12 = Class19.method224(var4, var5, (byte) 0);
				if (var12 == null || var5 == -1) {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = 0;
				} else {
					CS2Info.intStack[++CS2Info.intStackSize - 1] = 1;
					if (flag) {
						Class20.field115 = var12;
					} else {
						CS2Info.field1130 = var12;
					}
				}

				return 1;
			} else {
				if (opcode == 201) {
					var10 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
					if (var10 == null) {
						CS2Info.intStack[++CS2Info.intStackSize - 1] = 0;
					} else {
						CS2Info.intStack[++CS2Info.intStackSize - 1] = 1;
						if (flag) {
							Class20.field115 = var10;
						} else {
							CS2Info.field1130 = var10;
						}
					}

					return 1;
				} else {
					return 2;
				}
			}
		}
	}

	/*static int process_op(int opcode, Script script, boolean flag) {
		if (opcode < 3400) {
			return process_op_under_1000(opcode, script, flag);
		} else if (opcode < 3300) {
			return process_op_under_3300(opcode, script, flag);
		} else if (opcode < 2700) {
			return process_op_under_2700(opcode, script, flag);

		} else if (opcode < 2200) {
			return process_op_under_1200(opcode, script, flag);
		} else if (opcode < 2100) {
			return process_op_under_1100(opcode, script, flag);

		} else if (opcode < 1600) {
			return process_op_under_1600(opcode, script, flag);
		} else if (opcode < 1200) {
			return process_op_under_1200(opcode, script, flag);

		} else if (opcode < 1100) {
			return process_op_under_1100(opcode, script, flag);
		} else if (opcode < 1000) {
			return process_op_under_1000(opcode, script, flag);
		}
		return 1;
	}*/
	


	static int process_op(int opcode, Script script, boolean flag) {
		return opcode < 1000 ? process_op_under_1000(opcode, script, flag)
				: (opcode < 1100 ? process_op_under_1100(opcode, script, flag)
						: (opcode < 1200 ? process_op_under_1200(opcode, script, flag)
								: (opcode < 1300 ? process_op_under_1300(opcode, script, flag)
										: (opcode < 1400 ? process_op_under_1400(opcode, script, flag)
												: (opcode < 1500 ? process_op_under_1500(opcode, script, flag)
														: (opcode < 1600 ? process_op_under_1600(opcode, script, flag)
																: (opcode < 1700 ? process_op_under_1700(opcode, script,
																		flag)
																		: (opcode < 1800 ? process_op_under_1800(opcode,
																				script, flag)
																				: (opcode < 1900
																						? process_op_under_1900(opcode,
																								script, flag)
																						: (opcode < 2000
																								? process_op_under_2000(
																										opcode, script,
																										flag)
																								: (opcode < 2100
																										? process_op_under_1100(
																												opcode,
																												script,
																												flag)
																										: (opcode < 2200
																												? process_op_under_1200(
																														opcode,
																														script,
																														flag)
																												: (opcode < 2300
																														? process_op_under_1300(
																																opcode,
																																script,
																																flag)
																														: (opcode < 2400
																																? process_op_under_1400(
																																		opcode,
																																		script,
																																		flag)
																																: (opcode < 2500
																																		? process_op_under_1500(
																																				opcode,
																																				script,
																																				flag)
																																		: (opcode < 2600
																																				? process_op_under_2600(
																																						opcode,
																																						script,
																																						flag)
																																				: (opcode < 2700
																																						? process_op_under_2700(
																																								opcode,
																																								script,
																																								flag)
																																						: (opcode < 2800
																																								? process_op_under_2800(
																																										opcode,
																																										script,
																																										flag)
																																								: (opcode < 2900
																																										? process_op_under_2900(
																																												opcode,
																																												script,
																																												flag)
																																										: (opcode < 3000
																																												? process_op_under_2000(
																																														opcode,
																																														script,
																																														flag)
																																												: (opcode < 3200
																																														? process_op_under_3200(
																																																opcode,
																																																script,
																																																flag)
																																														: (opcode < 3300
																																																? process_op_under_3300(
																																																		opcode,
																																																		script,
																																																		flag)
																																																: (opcode < 3400
																																																		? process_op_under_3400(
																																																				opcode,
																																																				script,
																																																				flag)
																																																		: (opcode < 3500
																																																				? process_op_under_3500(
																																																						opcode,
																																																						script,
																																																						flag)
																																																				: (opcode < 3700
																																																						? process_op_under_3700(
																																																								opcode,
																																																								script,
																																																								flag)
																																																						: (opcode < 4000
																																																								? process_op_under_4000(
																																																										opcode,
																																																										script,
																																																										flag)
																																																								: (opcode < 4100
																																																										? process_op_under_4100(
																																																												opcode,
																																																												script,
																																																												flag)
																																																										: (opcode < 4200
																																																												? process_op_under_4200(
																																																														opcode,
																																																														script,
																																																														flag)
																																																												: (opcode < 4300
																																																														? process_op_under_4300(
																																																																opcode,
																																																																script,
																																																																flag)
																																																														: (opcode < 5100
																																																																? process_op_under_5100(
																																																																		opcode,
																																																																		script,
																																																																		flag)
																																																																: (opcode < 5400
																																																																		? process_op_under_5400(
																																																																				opcode,
																																																																				script,
																																																																				flag)
																																																																		: (opcode < 5600
																																																																				? process_op_under_5600(
																																																																								opcode,
																																																																								script,
																																																																								flag)
																																																																				: (opcode < 5700
																																																																						? process_op_under_5700(
																																																																								opcode,
																																																																								script,
																																																																								flag)
																																																																						: (opcode < 6300
																																																																								? process_op_under_6300(
																																																																										opcode,
																																																																										script,
																																																																										flag)
																																																																								: (opcode < 6600
																																																																										? process_op_under_6600(
																																																																												opcode,
																																																																												script,
																																																																												flag)
																																																																										: (opcode < 6700
																																																																												? process_op_under_6700(
																																																																														opcode,
																																																																														script,
																																																																														flag)
																																																																												: 2))))))))))))))))))))))))))))))))))));
	}

	private static int process_op_under_6700(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_6600(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_6300(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_5700(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_5600(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_5400(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_5100(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_4300(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_4200(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_4100(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_4000(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_3700(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_3500(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_3200(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_2900(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_2800(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_2600(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_2000(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_1900(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_1800(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	private static int process_op_under_1700(int opcode, Script script, boolean flag) {
		// TODO Auto-generated method stub
		return 2;
	}

	static final void execute_script(ScriptEvent script_event, int size, int widget_id) {
		Object params[] = script_event.params;
		int script_id = ((Integer) params[0]).intValue();
		Script script = Script.get_script(script_id, false);
		if (script == null) {
			return;
		}
		CS2Info.subScriptPos = 0;
		CS2Info.stringStackSize = 0;
		CS2Info.intStackSize = 0;
		int i1 = -1;
		int intOperands[] = script.intValues;
		int instructions[] = script.instructions;
		CS2Info.scriptLocalInts = new int[script.localIntCount];
		int k1 = 0;
		CS2Info.scriptLocalStrings = new RSString[script.localStringCount];
		int l1 = 0;
		for (int i2 = 1; params.length > i2; i2++) {
			if (params[i2] instanceof Integer) {
				int k2 = ((Integer) params[i2]).intValue();
				if (k2 == -2147483647)
					k2 = script_event.mouseX;
				if (0x80000002 == k2)
					k2 = script_event.mouseY;
				if (k2 == -2147483645)
					k2 = null == script_event.source ? -1 : script_event.source.hash;
				if (0x80000004 == k2)
					k2 = script_event.op;
				if (k2 == -2147483643)
					k2 = null == script_event.source ? -1 : script_event.source.index;
				if (k2 == -2147483642)
					k2 = null == script_event.target ? -1 : script_event.target.hash;
				if (k2 == -2147483641)
					k2 = script_event.target != null ? script_event.target.index : -1;
				if (k2 == -2147483640)
					k2 = script_event.typedKeyCode;
				if (k2 == -2147483639)
					k2 = script_event.typedKeyChar;
				CS2Info.scriptLocalInts[k1++] = k2;
				continue;
			}
			if (!(params[i2] instanceof RSString))
				continue;
			RSString class94 = (RSString) params[i2];
			if (class94.equals(Widget.aClass94_209))
				class94 = script_event.opbase;
			CS2Info.scriptLocalStrings[l1++] = class94;
		}
		int j2 = 0;
		label0: do {
			j2++;
			if (size < j2)
				throw new RuntimeException("slow");
			int opcode = instructions[++i1];
			int inter = (widget_id >> 16);

			if (inter != 0 && inter != 137 && inter != 750 && inter != 548 && inter != 751 && inter != 748) {
				System.out.println("[CS2-530] " + " [widget = " + inter + "] Executing opcode " + opcode);
			}
			if (opcode < 100) {
				if (opcode == 0) {
					CS2Info.intStack[CS2Info.intStackSize++] = intOperands[i1];
					continue;
				}
				if (opcode == 1) {
					int l2 = intOperands[i1];
					CS2Info.intStack[CS2Info.intStackSize++] = Varp.clientVarps[l2];
					continue;
				}
				if (opcode == 2) {
					int i3 = intOperands[i1];
					Varp.processVarpClientCode(i3, CS2Info.intStack[--CS2Info.intStackSize]);
					continue;
				}
				if (opcode == 3) {
					CS2Info.field1118[CS2Info.stringStackSize++] = script.stringValues[i1];
					continue;
				}
				if (opcode == 6) {
					i1 += intOperands[i1];
					continue;
				}
				if (7 == opcode) {
					CS2Info.intStackSize -= 2;
					if (CS2Info.intStack[CS2Info.intStackSize] != CS2Info.intStack[1 + CS2Info.intStackSize])
						i1 += intOperands[i1];
					continue;
				}
				if (opcode == 8) {
					CS2Info.intStackSize -= 2;
					if (CS2Info.intStack[CS2Info.intStackSize] == CS2Info.intStack[CS2Info.intStackSize + 1])
						i1 += intOperands[i1];
					continue;
				}
				if (9 == opcode) {
					CS2Info.intStackSize -= 2;
					if (CS2Info.intStack[1 + CS2Info.intStackSize] > CS2Info.intStack[CS2Info.intStackSize])
						i1 += intOperands[i1];
					continue;
				}
				if (opcode == 10) {
					CS2Info.intStackSize -= 2;
					if (CS2Info.intStack[CS2Info.intStackSize - -1] < CS2Info.intStack[CS2Info.intStackSize])
						i1 += intOperands[i1];
					continue;
				}
				if (opcode == 21) {
					if (CS2Info.subScriptPos == 0)
						return;
					ScriptState script_state = CS2Info.scriptStack[--CS2Info.subScriptPos];
					script = script_state.invokedFromScript;
					CS2Info.scriptLocalInts = script_state.savedLocalInts;
					instructions = script.instructions;
					i1 = script_state.invokedFromPc;
					CS2Info.scriptLocalStrings = script_state.savedLocalStrings;
					intOperands = script.intValues;
					continue;
				}
				if (opcode == 25) {
					int j3 = intOperands[i1];
					CS2Info.intStack[CS2Info.intStackSize++] = Varbit.getVarbit(j3);
					continue;
				}
				if (opcode == 27) {
					int k3 = intOperands[i1];
					Varbit.setVarbit(k3, CS2Info.intStack[--CS2Info.intStackSize]);
					continue;
				}
				if (31 == opcode) {
					CS2Info.intStackSize -= 2;
					if (CS2Info.intStack[CS2Info.intStackSize] <= CS2Info.intStack[1 + CS2Info.intStackSize])
						i1 += intOperands[i1];
					continue;
				}
				if (opcode == 32) {
					CS2Info.intStackSize -= 2;
					if (CS2Info.intStack[1 + CS2Info.intStackSize] <= CS2Info.intStack[CS2Info.intStackSize])
						i1 += intOperands[i1];
					continue;
				}
				if (33 == opcode) {
					CS2Info.intStack[CS2Info.intStackSize++] = CS2Info.scriptLocalInts[intOperands[i1]];
					continue;
				}
				if (opcode == 34) {
					CS2Info.scriptLocalInts[intOperands[i1]] = CS2Info.intStack[--CS2Info.intStackSize];
					continue;
				}
				if (opcode == 35) {
					CS2Info.field1118[CS2Info.stringStackSize++] = CS2Info.scriptLocalStrings[intOperands[i1]];
					continue;
				}
				if (opcode == 36) {
					CS2Info.scriptLocalStrings[intOperands[i1]] = CS2Info.field1118[--CS2Info.stringStackSize];
					continue;
				}
				if (opcode == 37) {
					int l3 = intOperands[i1];
					CS2Info.stringStackSize -= l3;
					RSString class94_2 = class208.method3900(CS2Info.field1118, CS2Info.stringStackSize, l3, 2774);
					CS2Info.field1118[CS2Info.stringStackSize++] = class94_2;
					continue;
				}
				if (38 == opcode) {
					CS2Info.intStackSize--;
					continue;
				}
				if (opcode == 39) {
					CS2Info.stringStackSize--;
					continue;
				}
				if (opcode == 40) {
					int i4 = intOperands[i1];
					Script class3_sub28_sub15_1 = Script.get_script(i4, false);
					int ai2[] = new int[class3_sub28_sub15_1.localIntCount];
					RSString aclass94[] = new RSString[class3_sub28_sub15_1.localStringCount];
					for (int l75 = 0; class3_sub28_sub15_1.intArgumentCount > l75; l75++)
						ai2[l75] = CS2Info.intStack[l75 + (CS2Info.intStackSize - class3_sub28_sub15_1.intArgumentCount)];

					for (int i76 = 0; class3_sub28_sub15_1.stringArgumentCount > i76; i76++)
						aclass94[i76] = CS2Info.field1118[i76 + -class3_sub28_sub15_1.stringArgumentCount
								+ CS2Info.stringStackSize];

					CS2Info.intStackSize -= class3_sub28_sub15_1.intArgumentCount;
					CS2Info.stringStackSize -= class3_sub28_sub15_1.stringArgumentCount;
					ScriptState class54_1 = new ScriptState();
					class54_1.savedLocalStrings = CS2Info.scriptLocalStrings;
					class54_1.savedLocalInts = CS2Info.scriptLocalInts;
					class54_1.invokedFromPc = i1;
					class54_1.invokedFromScript = script;
					if (CS2Info.scriptStack.length <= CS2Info.subScriptPos)
						throw new RuntimeException();
					script = class3_sub28_sub15_1;
					i1 = -1;
					CS2Info.scriptStack[CS2Info.subScriptPos++] = class54_1;
					CS2Info.scriptLocalInts = ai2;
					intOperands = script.intValues;
					instructions = script.instructions;
					CS2Info.scriptLocalStrings = aclass94;
					continue;
				}
				if (42 == opcode) {
					CS2Info.intStack[CS2Info.intStackSize++] = Varbit.anIntArray1277[intOperands[i1]];
					continue;
				}
				if (opcode == 43) {
					int j4 = intOperands[i1];
					Varbit.anIntArray1277[j4] = CS2Info.intStack[--CS2Info.intStackSize];
					PacketParser.method825((byte) 92, j4);
					continue;
				}
				if (44 == opcode) {
					int k4 = intOperands[i1] >> 16;
					int l43 = CS2Info.intStack[--CS2Info.intStackSize];
					int k5 = 0xffff & intOperands[i1];
					if (l43 < 0 || 5000 < l43)
						throw new RuntimeException();
					CS2Info.field1114[k4] = l43;
					byte byte2 = -1;
					if (k5 == 105)
						byte2 = 0;
					int j76 = 0;
					while (j76 < l43) {
						CS2Info.field1122[k4][j76] = byte2;
						j76++;
					}
					continue;
				}
				if (opcode == 45) {
					int l4 = intOperands[i1];
					int l5 = CS2Info.intStack[--CS2Info.intStackSize];
					if (0 > l5 || CS2Info.field1114[l4] <= l5)
						throw new RuntimeException();
					CS2Info.intStack[CS2Info.intStackSize++] = CS2Info.field1122[l4][l5];
					continue;
				}
				if (opcode == 46) {
					int i5 = intOperands[i1];
					CS2Info.intStackSize -= 2;
					int i6 = CS2Info.intStack[CS2Info.intStackSize];
					if (i6 < 0 || CS2Info.field1114[i5] <= i6)
						throw new RuntimeException();
					CS2Info.field1122[i5][i6] = CS2Info.intStack[1 + CS2Info.intStackSize];
					continue;
				}
				if (47 == opcode) {
					RSString class94_1 = Class132.aClass94Array1739[intOperands[i1]];
					if (null == class94_1)
						class94_1 = Class140_Sub7.aClass94_2928;
					CS2Info.field1118[CS2Info.stringStackSize++] = class94_1;
					continue;
				}
				if (opcode == 48) {
					int j5 = intOperands[i1];
					Class132.aClass94Array1739[j5] = CS2Info.field1118[--CS2Info.stringStackSize];
					Class49.method1126(-94, j5);
					continue;
				}
				if (opcode == 51) {
					class325 class130 = script.field1304[intOperands[i1]];
					class188 class3_sub18 = (class188) class130.method1780(CS2Info.intStack[--CS2Info.intStackSize], 0);
					if (null != class3_sub18)
						i1 += class3_sub18.anInt2467;
					continue;
				}
			}
			boolean flag;
			if (1 != intOperands[i1])
				flag = false;
			else
				flag = true;
			if (opcode < 300) {
				if (opcode == 100) {
					CS2Info.intStackSize -= 3;
					int j6 = CS2Info.intStack[CS2Info.intStackSize];
					int i44 = CS2Info.intStack[1 + CS2Info.intStackSize];
					int k66 = CS2Info.intStack[2 + CS2Info.intStackSize];
					if (i44 == 0)
						throw new RuntimeException();
					Widget class11_21 = Widget.get_widget(j6);
					if (null == class11_21.children)
						class11_21.children = new Widget[k66 + 1];
					if (k66 >= class11_21.children.length) {
						Widget aclass11[] = new Widget[k66 + 1];
						for (int k81 = 0; class11_21.children.length > k81; k81++)
							aclass11[k81] = class11_21.children[k81];

						class11_21.children = aclass11;
					}
					if (0 < k66 && class11_21.children[-1 + k66] == null)
						throw new RuntimeException("Gap at:" + (-1 + k66));
					Widget class11_23 = new Widget();
					class11_23.isIf3 = true;
					class11_23.index = k66;
					class11_23.parentId = class11_23.hash = class11_21.hash;
					class11_23.type = i44;
					class11_21.children[k66] = class11_23;
					if (flag)
						Class20.field115 = class11_23;
					else
						CS2Info.field1130 = class11_23;
					Class68.method909(class11_21);
					continue;
				}
				if (opcode == 101) {
					Widget class11 = flag ? Class20.field115 : CS2Info.field1130;
					if (class11.index == -1)
						if (!flag)
							throw new RuntimeException("Tried to cc_delete static active-component!");
						else
							throw new RuntimeException("Tried to .cc_delete static .active-component!");
					Widget class11_17 = Widget.get_widget(class11.hash);
					class11_17.children[class11.index] = null;
					Class68.method909(class11_17);
					continue;
				}
				if (opcode == 102) {
					Widget class11_1 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
					class11_1.children = null;
					Class68.method909(class11_1);
					continue;
				}
				if (opcode == 200) {
					CS2Info.intStackSize -= 2;
					int var1 = CS2Info.intStack[CS2Info.intStackSize];
					int var2 = CS2Info.intStack[CS2Info.intStackSize - -1];
					Widget class11_19 = Class19.method224(var1, var2, (byte) -19);
					if (null == class11_19 || var2 == -1) {
						CS2Info.intStack[CS2Info.intStackSize++] = 0;
					} else {
						CS2Info.intStack[CS2Info.intStackSize++] = 1;
						if (!flag)
							CS2Info.field1130 = class11_19;
						else
							Class20.field115 = class11_19;
					}
					continue;
				}
				if (opcode != 201)
					break;
				int l6 = CS2Info.intStack[--CS2Info.intStackSize];
				Widget class11_18 = Widget.get_widget(l6);
				if (null == class11_18) {
					CS2Info.intStack[CS2Info.intStackSize++] = 0;
				} else {
					CS2Info.intStack[CS2Info.intStackSize++] = 1;
					if (flag)
						Class20.field115 = class11_18;
					else
						CS2Info.field1130 = class11_18;
				}
				continue;
			}
			if (500 <= opcode) {
				if (1000 <= opcode && opcode < 1100 || 2000 <= opcode && opcode < 2100) {
					Widget class11_2;
					if (opcode < 2000) {
						class11_2 = flag ? Class20.field115 : CS2Info.field1130;
					} else {
						class11_2 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
						opcode -= 1000;
					}
					if (opcode == 1000) {
						CS2Info.intStackSize -= 4;
						class11_2.originalX = CS2Info.intStack[CS2Info.intStackSize];
						class11_2.originalY = CS2Info.intStack[CS2Info.intStackSize + 1];
						int l66 = CS2Info.intStack[3 + CS2Info.intStackSize];
						if (l66 < 0)
							l66 = 0;
						else if (l66 > 5)
							l66 = 5;
						int k44 = CS2Info.intStack[CS2Info.intStackSize + 2];
						if (k44 >= 0) {
							if (k44 > 5)
								k44 = 5;
						} else {
							k44 = 0;
						}
						class11_2.yPositionMode = (byte) l66;
						class11_2.xPositionMode = (byte) k44;
						Class68.method909(class11_2);
						Class3_Sub13_Sub12.method225(14, class11_2);
						if (class11_2.index == -1)
							Class168.method2280(2714, class11_2.hash);
						continue;
					}
					if (opcode == 1001) {
						CS2Info.intStackSize -= 4;
						class11_2.originalWidth = CS2Info.intStack[CS2Info.intStackSize];
						class11_2.originalHeight = CS2Info.intStack[1 + CS2Info.intStackSize];
						class11_2.modelHeightOverride = 0;
						class11_2.field2683 = 0;
						int l44 = CS2Info.intStack[CS2Info.intStackSize + 2];
						int i67 = CS2Info.intStack[3 + CS2Info.intStackSize];
						if (i67 >= 0) {
							if (i67 > 4)
								i67 = 4;
						} else {
							i67 = 0;
						}
						class11_2.heightMode = (byte) i67;
						if (l44 < 0)
							l44 = 0;
						else if (l44 > 4)
							l44 = 4;
						class11_2.widthMode = (byte) l44;
						Class68.method909(class11_2);
						Class3_Sub13_Sub12.method225(14, class11_2);
						if (class11_2.type == 0)
							class63.method1161(class11_2, false, 32);
						continue;
					}
					if (opcode == 1003) {
						boolean flag3 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
						if (flag3 == (!class11_2.isHidden)) {
							class11_2.isHidden = flag3;
							Class68.method909(class11_2);
						}
						if (-1 == class11_2.index)
							Class3_Sub28_Sub7_Sub1.method569(-82, class11_2.hash);
						continue;
					}
					if (opcode == 1004) {
						CS2Info.intStackSize -= 2;
						class11_2.anInt216 = CS2Info.intStack[CS2Info.intStackSize];
						class11_2.anInt160 = CS2Info.intStack[CS2Info.intStackSize - -1];
						Class68.method909(class11_2);
						Class3_Sub13_Sub12.method225(14, class11_2);
						if (class11_2.type == 0)
							class63.method1161(class11_2, false, -127);
						continue;
					}
					if (opcode != 1005)
						break;
					class11_2.noClickThrough = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					continue;
				}
				if ((opcode < 1100 || 1200 <= opcode) && (opcode < 2100 || 2200 <= opcode)) {
					if ((opcode < 1200 || 1300 <= opcode) && (2200 > opcode || opcode >= 2300)) {
						if (opcode >= 1300 && opcode < 1400 || opcode >= 2300 && opcode < 2400) {
							Widget class11_3;
							if (2000 <= opcode) {
								class11_3 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
								opcode -= 1000;
							} else {
								class11_3 = flag ? Class20.field115 : CS2Info.field1130;
							}
							if (opcode == 1300) {
								int i45 = CS2Info.intStack[--CS2Info.intStackSize] + -1;
								if (0 > i45 || i45 > 9)
									CS2Info.stringStackSize--;
								else
									class11_3.method857(i45, CS2Info.field1118[--CS2Info.stringStackSize]);
								continue;
							}
							if (opcode == 1301) {
								CS2Info.intStackSize -= 2;
								int j67 = CS2Info.intStack[1 + CS2Info.intStackSize];
								int j45 = CS2Info.intStack[CS2Info.intStackSize];
								class11_3.field2648 = Class19.method224(j45, j67, (byte) -19);
								continue;
							}
							if (opcode == 1302) {
								class11_3.dragRenderBehavior = CS2Info.intStack[--CS2Info.intStackSize] == 1;
								continue;
							}
							if (opcode == 1303) {
								class11_3.dragDeadZone = CS2Info.intStack[--CS2Info.intStackSize];
								continue;
							}
							if (opcode == 1304) {
								class11_3.dragDeadTime = CS2Info.intStack[--CS2Info.intStackSize];
								continue;
							}
							if (1305 == opcode) {
								class11_3.name = CS2Info.field1118[--CS2Info.stringStackSize];
								continue;
							}
							if (opcode == 1306) {
								class11_3.targetVerb = CS2Info.field1118[--CS2Info.stringStackSize];
								continue;
							}
							if (opcode == 1307) {
								class11_3.actions = null;
								continue;
							}
							if (opcode == 1308) {
								class11_3.anInt238 = CS2Info.intStack[--CS2Info.intStackSize];
								class11_3.anInt266 = CS2Info.intStack[--CS2Info.intStackSize];
								continue;
							}
							if (1309 != opcode)
								break;
							int k45 = CS2Info.intStack[--CS2Info.intStackSize];
							int k67 = CS2Info.intStack[--CS2Info.intStackSize];
							if (k67 >= 1 && k67 <= 10)
								class11_3.method854(k67 + -1, k45, (byte) 43);
							continue;
						}
						if ((opcode < 1400 || opcode >= 1500) && (2400 > opcode || opcode >= 2500)) {
							if (1600 > opcode) {
								Widget class11_4 = flag ? Class20.field115 : CS2Info.field1130;
								if (opcode == 1500) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_4.field2580;
									continue;
								}
								if (opcode == 1501) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_4.field2653;
									continue;
								}
								if (opcode == 1502) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_4.field2582;
									continue;
								}
								if (opcode == 1503) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_4.field2642;
									continue;
								}
								if (opcode == 1504) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_4.isHidden ? 1 : 0;
									continue;
								}
								if (opcode != 1505)
									break;
								CS2Info.intStack[CS2Info.intStackSize++] = class11_4.parentId;
								continue;
							}
							if (opcode < 1700) {
								Widget class11_5 = flag ? Class20.field115 : CS2Info.field1130;
								if (opcode == 1600) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.field2646;
									continue;
								}
								if (opcode == 1601) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.field2589;
									continue;
								}
								if (opcode == 1602) {
									CS2Info.field1118[CS2Info.stringStackSize++] = class11_5.text;
									continue;
								}
								if (opcode == 1603) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.scrollWidth;
									continue;
								}
								if (opcode == 1604) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.scrollHeight;
									continue;
								}
								if (opcode == 1605) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.modelZoom;
									continue;
								}
								if (opcode == 1606) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.rotationX;
									continue;
								}
								if (1607 == opcode) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.rotationY;
									continue;
								}
								if (opcode == 1608) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.rotationZ;
									continue;
								}
								if (opcode == 1609) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.opacity;
									continue;
								}
								if (1610 == opcode) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.field2914;
									continue;
								}
								if (opcode == 1611) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_5.anInt264;
									continue;
								}
								if (opcode != 1612)
									break;
								CS2Info.intStack[CS2Info.intStackSize++] = class11_5.spriteId;
								continue;
							}
							if (opcode >= 1800) {
								if (opcode < 1900) {
									Widget class11_6 = flag ? Class20.field115 : CS2Info.field1130;
									if (1800 == opcode) {
										CS2Info.intStack[CS2Info.intStackSize++] = Client.method44(class11_6)
												.method101(-95);
										continue;
									}
									if (1801 == opcode) {
										int l45 = CS2Info.intStack[--CS2Info.intStackSize];
										l45--;
										if (null == class11_6.actions || class11_6.actions.length <= l45
												|| null == class11_6.actions[l45])
											CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
										else
											CS2Info.field1118[CS2Info.stringStackSize++] = class11_6.actions[l45];
										continue;
									}
									if (opcode != 1802)
										break;
									if (null != class11_6.name)
										CS2Info.field1118[CS2Info.stringStackSize++] = class11_6.name;
									else
										CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
									continue;
								}
								if (2600 > opcode) {
									Widget class11_7 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
									if (opcode == 2500) {
										CS2Info.intStack[CS2Info.intStackSize++] = class11_7.field2580;
										continue;
									}
									if (opcode == 2501) {
										CS2Info.intStack[CS2Info.intStackSize++] = class11_7.field2653;
										continue;
									}
									if (2502 == opcode) {
										CS2Info.intStack[CS2Info.intStackSize++] = class11_7.field2582;
										continue;
									}
									if (opcode == 2503) {
										CS2Info.intStack[CS2Info.intStackSize++] = class11_7.field2642;
										continue;
									}
									if (2504 == opcode) {
										CS2Info.intStack[CS2Info.intStackSize++] = class11_7.isHidden ? 1 : 0;
										continue;
									}
									if (opcode != 2505)
										break;
									CS2Info.intStack[CS2Info.intStackSize++] = class11_7.parentId;
									continue;
								}
								if (opcode >= 2700) {
									if (2800 <= opcode) {
										if (opcode >= 2900) {
											if (opcode < 3200) {
												if (opcode == 3100) {
													RSString class94_3 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class3_Sub30_Sub1.method805(Class3_Sub9.aClass94_2331, 0, class94_3,
															-1);
													continue;
												}
												if (opcode == 3101) {
													CS2Info.intStackSize -= 2;
													Class3_Sub28_Sub14.method628(0,
															CS2Info.intStack[CS2Info.intStackSize - -1],
															CS2Info.intStack[CS2Info.intStackSize], Client.field3717);
													continue;
												}
												if (opcode == 3103) {
													Class3_Sub13_Sub19.method264((byte) 87);
													continue;
												}
												if (opcode == 3104) {
													Class100.anInt1405++;
													RSString class94_4 = CS2Info.field1118[--CS2Info.stringStackSize];
													int i46 = 0;
													if (class94_4.method1543(82))
														i46 = class94_4.method1552((byte) -104);
													Class3_Sub13_Sub1.outgoingBuffer.putOpcode(23);
													Class3_Sub13_Sub1.outgoingBuffer.putInt(-124, i46);
													continue;
												}
												if (opcode == 3105) {
													Class3_Sub22.anInt2500++;
													RSString class94_5 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class3_Sub13_Sub1.outgoingBuffer.putOpcode(244);
													Class3_Sub13_Sub1.outgoingBuffer.putLong(class94_5.toLong(-115),
															0x868e5910);
													continue;
												}
												if (opcode == 3106) {
													Class7.anInt2165++;
													RSString class94_6 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class3_Sub13_Sub1.outgoingBuffer.putOpcode(65);
													Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -17,
															1 + class94_6.length(-84));
													Class3_Sub13_Sub1.outgoingBuffer.putString(0, class94_6);
													continue;
												}
												if (opcode == 3107) {
													int i7 = CS2Info.intStack[--CS2Info.intStackSize];
													RSString class94_44 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class166.method2258(i7, 0, class94_44);
													continue;
												}
												if (opcode == 3108) {
													CS2Info.intStackSize -= 3;
													int j46 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int j7 = CS2Info.intStack[CS2Info.intStackSize];
													int l67 = CS2Info.intStack[2 + CS2Info.intStackSize];
													Widget class11_22 = Widget.get_widget(l67);
													Class3_Sub28_Sub6.a(j46, j7, 115, class11_22);
													continue;
												}
												if (opcode == 3109) {
													CS2Info.intStackSize -= 2;
													int k7 = CS2Info.intStack[CS2Info.intStackSize];
													Widget class11_20 = flag ? Class20.field115 : CS2Info.field1130;
													int k46 = CS2Info.intStack[1 + CS2Info.intStackSize];
													Class3_Sub28_Sub6.a(k46, k7, 79, class11_20);
													continue;
												}
												if (opcode != 3110)
													break;
												Class3_Sub13_Sub16.anInt3199++;
												int l7 = CS2Info.intStack[--CS2Info.intStackSize];
												Class3_Sub13_Sub1.outgoingBuffer.putOpcode(111);
												Class3_Sub13_Sub1.outgoingBuffer.putShort(l7);
												continue;
											}
											if (opcode < 3300) {
												if (opcode == 3200) {
													CS2Info.intStackSize -= 3;
													Class3_Sub13_Sub6.method199(CS2Info.intStack[CS2Info.intStackSize],
															CS2Info.intStack[CS2Info.intStackSize - -1],
															CS2Info.intStack[CS2Info.intStackSize + 2], -799);
													continue;
												}
												if (opcode == 3201) {
													Class86.method1427(true, CS2Info.intStack[--CS2Info.intStackSize]);
													continue;
												}
												if (opcode != 3202)
													break;
												CS2Info.intStackSize -= 2;
												Class167.method2266(CS2Info.intStack[1 + CS2Info.intStackSize],
														CS2Info.intStack[CS2Info.intStackSize], (byte) -1);
												continue;
											}
											if (opcode < 3400) {
												if (opcode == 3300) {
													CS2Info.intStack[CS2Info.intStackSize++] = Class44.anInt719;
													continue;
												}
												if (opcode == 3301) {
													CS2Info.intStackSize -= 2;
													int i8 = CS2Info.intStack[CS2Info.intStackSize];
													int l46 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Widget.method861(i8, 89,
															l46);
													continue;
												}
												if (opcode == 3302) {
													CS2Info.intStackSize -= 2;
													int i47 = CS2Info.intStack[CS2Info.intStackSize + 1];
													int j8 = CS2Info.intStack[CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = class196.method872(-1,
															j8, i47);
													continue;
												}
												if (3303 == opcode) {
													CS2Info.intStackSize -= 2;
													int j47 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int k8 = CS2Info.intStack[CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class167.method2268(k8,
															j47);
													continue;
												}
												if (3304 == opcode) {
													int l8 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = class231.method4193(l8,
															-127).anInt3647;
													continue;
												}
												if (opcode == 3305) {
													int i9 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field686[i9];
													continue;
												}
												if (opcode == 3306) {
													int j9 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field752[j9];
													continue;
												}
												if (3307 == opcode) {
													int k9 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field821[k9];
													continue;
												}
												if (opcode == 3308) {
													int l9 = Client.localPlane;
													int k47 = Client.field590 + (Client.field3717.field983 >> 7);
													int i68 = (Client.field3717.field973 >> 7) - -Client.field1152;
													CS2Info.intStack[CS2Info.intStackSize++] = (l9 << 28)
															- (-(k47 << 14) - i68);
													continue;
												}
												if (opcode == 3309) {
													int i10 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Script.method633(16383,
															i10 >> 14);
													continue;
												}
												if (3310 == opcode) {
													int j10 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = j10 >> 28;
													continue;
												}
												if (opcode == 3311) {
													int k10 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Script.method633(k10,
															16383);
													continue;
												}
												if (opcode == 3312) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.isMember ? 1 : 0;
													continue;
												}
												if (3313 == opcode) {
													CS2Info.intStackSize -= 2;
													int l10 = 32768 + CS2Info.intStack[CS2Info.intStackSize];
													int l47 = CS2Info.intStack[CS2Info.intStackSize - -1];
													CS2Info.intStack[CS2Info.intStackSize++] = Widget.method861(l10,
															118, l47);
													continue;
												}
												if (3314 == opcode) {
													CS2Info.intStackSize -= 2;
													int i11 = CS2Info.intStack[CS2Info.intStackSize] - -32768;
													int i48 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = class196.method872(-1,
															i11, i48);
													continue;
												}
												if (3315 == opcode) {
													CS2Info.intStackSize -= 2;
													int j11 = 32768 + CS2Info.intStack[CS2Info.intStackSize];
													int j48 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class167.method2268(j11,
															j48);
													continue;
												}
												if (opcode == 3316) {
													if (Client.rights < 2)
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Client.rights;
													continue;
												}
												if (opcode == 3317) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field917;
													continue;
												}
												if (3318 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field680;
													continue;
												}
												if (3321 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field847;
													continue;
												}
												if (opcode == 3322) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field848;
													continue;
												}
												if (3323 == opcode) {
													if (Class3_Sub28_Sub19.anInt3775 >= 5
															&& Class3_Sub28_Sub19.anInt3775 <= 9) {
														CS2Info.intStack[CS2Info.intStackSize++] = 1;
													} else {
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													}
													continue;
												}
												if (opcode == 3324) {
													if (Class3_Sub28_Sub19.anInt3775 < 5
															|| Class3_Sub28_Sub19.anInt3775 > 9)
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub19.anInt3775;
													continue;
												}
												if (3325 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub29.disableGEBoxes
															? 1
															: 0;
													continue;
												}
												if (3326 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field3717.anInt3960;
													continue;
												}
												if (3327 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Client.field3717.field646.aBoolean864
															? 1
															: 0;
													continue;
												}
												if (3328 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = !Class3_Sub15.aBoolean2433
															|| Class121.aBoolean1641 ? 0 : 1;
													continue;
												}
												if (3329 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub14.aBoolean3166
															? 1
															: 0;
													continue;
												}
												if (opcode == 3330) {
													int k11 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = WorldListEntry
															.method1079(k11, (byte) -80);
													continue;
												}
												if (opcode == 3331) {
													CS2Info.intStackSize -= 2;
													int k48 = CS2Info.intStack[1 + CS2Info.intStackSize];
													int l11 = CS2Info.intStack[CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class106
															.method1643(10131, false, l11, k48);
													continue;
												}
												if (3332 == opcode) {
													CS2Info.intStackSize -= 2;
													int i12 = CS2Info.intStack[CS2Info.intStackSize];
													int l48 = CS2Info.intStack[CS2Info.intStackSize + 1];
													CS2Info.intStack[CS2Info.intStackSize++] = Class106
															.method1643(10131, true, i12, l48);
													continue;
												}
												if (3333 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Class7.anInt2161;
													continue;
												}
												if (3335 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub20.language;
													continue;
												}
												if (opcode == 3336) {
													CS2Info.intStackSize -= 4;
													int i49 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int j12 = CS2Info.intStack[CS2Info.intStackSize];
													j12 += i49 << 14;
													int k76 = CS2Info.intStack[3 + CS2Info.intStackSize];
													int j68 = CS2Info.intStack[2 + CS2Info.intStackSize];
													j12 += j68 << 28;
													j12 += k76;
													CS2Info.intStack[CS2Info.intStackSize++] = j12;
													continue;
												}
												if (opcode != 3337)
													break;
												CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub26.anInt2554;
												continue;
											}
											if (opcode < 3500) {
												if (opcode == 3400) {
													CS2Info.intStackSize -= 2;
													int k12 = CS2Info.intStack[CS2Info.intStackSize];
													int j49 = CS2Info.intStack[1 + CS2Info.intStackSize];
													Class3_Sub28_Sub13 class3_sub28_sub13_1 = Class3_Sub13_Sub36
															.method342(k12, true);
													if (class3_sub28_sub13_1.anInt3658 != 115)
														;
													CS2Info.field1118[CS2Info.stringStackSize++] = class3_sub28_sub13_1
															.method616(j49, (byte) 121);
													continue;
												}
												if (3408 == opcode) {
													CS2Info.intStackSize -= 4;
													int l12 = CS2Info.intStack[CS2Info.intStackSize];
													int k49 = CS2Info.intStack[1 + CS2Info.intStackSize];
													int l76 = CS2Info.intStack[3 + CS2Info.intStackSize];
													int k68 = CS2Info.intStack[CS2Info.intStackSize - -2];
													Class3_Sub28_Sub13 class3_sub28_sub13_4 = Class3_Sub13_Sub36
															.method342(k68, true);
													if (class3_sub28_sub13_4.anInt3662 != l12
															|| k49 != class3_sub28_sub13_4.anInt3658)
														throw new RuntimeException("C3408-1");
													if (k49 != 115)
														CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub13_4
																.method620(0, l76);
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = class3_sub28_sub13_4
																.method616(l76, (byte) -25);
													continue;
												}
												if (opcode == 3409) {
													CS2Info.intStackSize -= 3;
													int l49 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int l68 = CS2Info.intStack[CS2Info.intStackSize + 2];
													int i13 = CS2Info.intStack[CS2Info.intStackSize];
													if (l49 == -1)
														throw new RuntimeException("C3409-2");
													Class3_Sub28_Sub13 class3_sub28_sub13_3 = Class3_Sub13_Sub36
															.method342(l49, true);
													if (i13 != class3_sub28_sub13_3.anInt3658)
														throw new RuntimeException("C3409-1");
													CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub13_3
															.method621(-8143, l68) ? 1 : 0;
													continue;
												}
												if (opcode == 3410) {
													int j13 = CS2Info.intStack[--CS2Info.intStackSize];
													RSString class94_45 = CS2Info.field1118[--CS2Info.stringStackSize];
													if (j13 == -1)
														throw new RuntimeException("C3410-2");
													Class3_Sub28_Sub13 class3_sub28_sub13_2 = Class3_Sub13_Sub36
															.method342(j13, true);
													if (class3_sub28_sub13_2.anInt3658 != 115)
														throw new RuntimeException("C3410-1");
													CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub13_2
															.method617(class94_45, 8729) ? 1 : 0;
													continue;
												}
												if (opcode != 3411)
													break;
												int k13 = CS2Info.intStack[--CS2Info.intStackSize];
												Class3_Sub28_Sub13 class3_sub28_sub13 = Class3_Sub13_Sub36
														.method342(k13, true);
												CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub13.aClass130_3663
														.method1781(79);
												continue;
											}
											if (3700 > opcode) {
												if (3600 == opcode) {
													if (Class96.anInt1357 == 0)
														CS2Info.intStack[CS2Info.intStackSize++] = -2;
													else if (Class96.anInt1357 != 1)
														CS2Info.intStack[CS2Info.intStackSize++] = JS5Manager.anInt104;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = -1;
													continue;
												}
												if (3601 == opcode) {
													int l13 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Class96.anInt1357 != 2 || JS5Manager.anInt104 <= l13)
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class70.aClass94Array1046[l13];
													continue;
												}
												if (opcode == 3602) {
													int i14 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Class96.anInt1357 != 2 || i14 >= JS5Manager.anInt104)
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Class55.anIntArray882[i14];
													continue;
												}
												if (opcode == 3603) {
													int j14 = CS2Info.intStack[--CS2Info.intStackSize];
													if (2 == Class96.anInt1357 && JS5Manager.anInt104 > j14)
														CS2Info.intStack[CS2Info.intStackSize++] = Class57.anIntArray904[j14];
													else
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													continue;
												}
												if (3604 == opcode) {
													int i50 = CS2Info.intStack[--CS2Info.intStackSize];
													RSString class94_7 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class100.method1605(255, class94_7, i50);
													continue;
												}
												if (opcode == 3605) {
													RSString class94_8 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class163_Sub3.method2229(class94_8.toLong(-120), (byte) -91);
													continue;
												}
												if (opcode == 3606) {
													RSString class94_9 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class3_Sub13_Sub27.method297(class94_9.toLong(-114), 1);
													continue;
												}
												if (opcode == 3607) {
													RSString class94_10 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class81.method1399(32, class94_10.toLong(-116));
													continue;
												}
												if (opcode == 3608) {
													RSString class94_11 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class3_Sub13_Sub10.method212(class94_11.toLong(-115), 0);
													continue;
												}
												if (opcode == 3609) {
													RSString class94_12 = CS2Info.field1118[--CS2Info.stringStackSize];
													if (class94_12.method1558(Class3_Sub9.aClass94_2323, 0)
															|| class94_12.method1558(Class3_Sub13_Sub16.aClass94_3190,
																	0))
														class94_12 = class94_12.method1556(7, (byte) -74);
													CS2Info.intStack[CS2Info.intStackSize++] = ScriptState
															.method1176(class94_12, (byte) -82) ? 1 : 0;
													continue;
												}
												if (opcode == 3610) {
													int k14 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Class96.anInt1357 == 2 && JS5Manager.anInt104 > k14)
														CS2Info.field1118[CS2Info.stringStackSize++] = Node.aClass94Array2566[k14];
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													continue;
												}
												if (opcode == 3611) {
													if (Widget.aClass94_251 != null)
														CS2Info.field1118[CS2Info.stringStackSize++] = Widget.aClass94_251
																.method1545((byte) -50);
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													continue;
												}
												if (opcode == 3612) {
													if (null != Widget.aClass94_251)
														CS2Info.intStack[CS2Info.intStackSize++] = Node.clanSize;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													continue;
												}
												if (opcode == 3613) {
													int l14 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Widget.aClass94_251 == null || l14 >= Node.clanSize)
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Script.aClass3_Sub19Array3694[l14].aClass94_2476
																.method1545((byte) -50);
													continue;
												}
												if (opcode == 3614) {
													int i15 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Widget.aClass94_251 == null || i15 >= Node.clanSize)
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Script.aClass3_Sub19Array3694[i15].anInt2478;
													continue;
												}
												if (3615 == opcode) {
													int j15 = CS2Info.intStack[--CS2Info.intStackSize];
													if (null == Widget.aClass94_251 || j15 >= Node.clanSize)
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Script.aClass3_Sub19Array3694[j15].aByte2472;
													continue;
												}
												if (3616 == opcode) {
													CS2Info.intStack[CS2Info.intStackSize++] = Player.aByte3953;
													continue;
												}
												if (opcode == 3617) {
													RSString class94_13 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class106.method1642(3803, class94_13);
													continue;
												}
												if (opcode == 3618) {
													CS2Info.intStack[CS2Info.intStackSize++] = Class91.aByte1308;
													continue;
												}
												if (opcode == 3619) {
													RSString class94_14 = CS2Info.field1118[--CS2Info.stringStackSize];
													Class3_Sub22.method400(class94_14.toLong(-107), 0);
													continue;
												}
												if (opcode == 3620) {
													Class77.method1368(-90);
													continue;
												}
												if (opcode == 3621) {
													if (Class96.anInt1357 == 0)
														CS2Info.intStack[CS2Info.intStackSize++] = -1;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub5.anInt3591;
													continue;
												}
												if (3622 == opcode) {
													int k15 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Class96.anInt1357 == 0 || Class3_Sub28_Sub5.anInt3591 <= k15)
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class41
																.method1052(-29664, Class114.aLongArray1574[k15])
																.method1545((byte) -50);
													continue;
												}
												if (3623 == opcode) {
													RSString class94_15 = CS2Info.field1118[--CS2Info.stringStackSize];
													if (class94_15.method1558(Class3_Sub9.aClass94_2323, 0)
															|| class94_15.method1558(Class3_Sub13_Sub16.aClass94_3190,
																	0))
														class94_15 = class94_15.method1556(7, (byte) -74);
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub24_Sub3
															.method467(class94_15, 0) ? 1 : 0;
													continue;
												}
												if (opcode == 3624) {
													int l15 = CS2Info.intStack[--CS2Info.intStackSize];
													if (null != Script.aClass3_Sub19Array3694 && l15 < Node.clanSize
															&& Script.aClass3_Sub19Array3694[l15].aClass94_2476
																	.method1531(-118, Client.field3717.displayName))
														CS2Info.intStack[CS2Info.intStackSize++] = 1;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													continue;
												}
												if (opcode == 3625) {
													if (Class161.aClass94_2035 == null)
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class161.aClass94_2035
																.method1545((byte) -50);
													continue;
												}
												if (3626 == opcode) {
													int i16 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Widget.aClass94_251 == null || i16 >= Node.clanSize)
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Script.aClass3_Sub19Array3694[i16].aClass94_2473;
													continue;
												}
												if (opcode == 3627) {
													int j16 = CS2Info.intStack[--CS2Info.intStackSize];
													if (Class96.anInt1357 != 2 || 0 > j16 || JS5Manager.anInt104 <= j16)
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Class3.aBooleanArray73[j16]
																? 1
																: 0;
													continue;
												}
												if (opcode == 3628) {
													RSString class94_16 = CS2Info.field1118[--CS2Info.stringStackSize];
													if (class94_16.method1558(Class3_Sub9.aClass94_2323, 0)
															|| class94_16.method1558(Class3_Sub13_Sub16.aClass94_3190,
																	0))
														class94_16 = class94_16.method1556(7, (byte) -74);
													CS2Info.intStack[CS2Info.intStackSize++] = PacketParser
															.method826(class94_16, -1);
													continue;
												}
												if (opcode != 3629)
													break;
												CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub31.countryId;
												continue;
											}
											if (opcode < 4000) {
												if (opcode == 3903) {
													int k16 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub33.aClass133Array3393[k16]
															.method1805((byte) -33);
													continue;
												}
												if (opcode == 3904) {
													int l16 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub33.aClass133Array3393[l16].anInt1752;
													continue;
												}
												if (opcode == 3905) {
													int i17 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub33.aClass133Array3393[i17].anInt1757;
													continue;
												}
												if (opcode == 3906) {
													int j17 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub33.aClass133Array3393[j17].anInt1747;
													continue;
												}
												if (opcode == 3907) {
													int k17 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub33.aClass133Array3393[k17].anInt1746;
													continue;
												}
												if (3908 == opcode) {
													int l17 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub33.aClass133Array3393[l17].anInt1750;
													continue;
												}
												if (3910 == opcode) {
													int i18 = CS2Info.intStack[--CS2Info.intStackSize];
													int j50 = Class3_Sub13_Sub33.aClass133Array3393[i18]
															.method1804(false);
													CS2Info.intStack[CS2Info.intStackSize++] = j50 == 0 ? 1 : 0;
													continue;
												}
												if (3911 == opcode) {
													int j18 = CS2Info.intStack[--CS2Info.intStackSize];
													int k50 = Class3_Sub13_Sub33.aClass133Array3393[j18]
															.method1804(false);
													CS2Info.intStack[CS2Info.intStackSize++] = k50 != 2 ? 0 : 1;
													continue;
												}
												if (opcode == 3912) {
													int k18 = CS2Info.intStack[--CS2Info.intStackSize];
													int l50 = Class3_Sub13_Sub33.aClass133Array3393[k18]
															.method1804(false);
													CS2Info.intStack[CS2Info.intStackSize++] = l50 == 5 ? 1 : 0;
													continue;
												}
												if (opcode != 3913)
													break;
												int l18 = CS2Info.intStack[--CS2Info.intStackSize];
												int i51 = Class3_Sub13_Sub33.aClass133Array3393[l18].method1804(false);
												CS2Info.intStack[CS2Info.intStackSize++] = 1 == i51 ? 1 : 0;
												continue;
											}
											if (opcode < 4100) {
												if (opcode == 4000) {
													CS2Info.intStackSize -= 2;
													int i19 = CS2Info.intStack[CS2Info.intStackSize];
													int j51 = CS2Info.intStack[CS2Info.intStackSize - -1];
													CS2Info.intStack[CS2Info.intStackSize++] = j51 + i19;
													continue;
												}
												if (opcode == 4001) {
													CS2Info.intStackSize -= 2;
													int j19 = CS2Info.intStack[CS2Info.intStackSize];
													int k51 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = -k51 + j19;
													continue;
												}
												if (4002 == opcode) {
													CS2Info.intStackSize -= 2;
													int k19 = CS2Info.intStack[CS2Info.intStackSize];
													int l51 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = l51 * k19;
													continue;
												}
												if (4003 == opcode) {
													CS2Info.intStackSize -= 2;
													int l19 = CS2Info.intStack[CS2Info.intStackSize];
													int i52 = CS2Info.intStack[CS2Info.intStackSize - -1];
													CS2Info.intStack[CS2Info.intStackSize++] = l19 / i52;
													continue;
												}
												if (opcode == 4004) {
													int i20 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = (int) ((double) i20
															* Math.random());
													continue;
												}
												if (4005 == opcode) {
													int j20 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = (int) (Math.random()
															* (double) (1 + j20));
													continue;
												}
												if (4006 == opcode) {
													CS2Info.intStackSize -= 5;
													int k20 = CS2Info.intStack[CS2Info.intStackSize];
													int j52 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int i77 = CS2Info.intStack[CS2Info.intStackSize - -3];
													int i69 = CS2Info.intStack[2 + CS2Info.intStackSize];
													int j79 = CS2Info.intStack[CS2Info.intStackSize + 4];
													CS2Info.intStack[CS2Info.intStackSize++] = ((-k20 + j52)
															* (j79 + -i69)) / (-i69 + i77) + k20;
													continue;
												}
												if (opcode == 4007) {
													CS2Info.intStackSize -= 2;
													long l20 = CS2Info.intStack[CS2Info.intStackSize];
													long l69 = CS2Info.intStack[CS2Info.intStackSize + 1];
													CS2Info.intStack[CS2Info.intStackSize++] = (int) ((l20 * l69) / 100L
															+ l20);
													continue;
												}
												if (opcode == 4008) {
													CS2Info.intStackSize -= 2;
													int i21 = CS2Info.intStack[CS2Info.intStackSize];
													int k52 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub29
															.method308(i21, 1 << k52);
													continue;
												}
												if (4009 == opcode) {
													CS2Info.intStackSize -= 2;
													int j21 = CS2Info.intStack[CS2Info.intStackSize];
													int l52 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Script
															.method633(-1 - (1 << l52), j21);
													continue;
												}
												if (opcode == 4010) {
													CS2Info.intStackSize -= 2;
													int k21 = CS2Info.intStack[CS2Info.intStackSize];
													int i53 = CS2Info.intStack[CS2Info.intStackSize - -1];
													CS2Info.intStack[CS2Info.intStackSize++] = Script.method633(k21,
															1 << i53) != 0 ? 1 : 0;
													continue;
												}
												if (opcode == 4011) {
													CS2Info.intStackSize -= 2;
													int j53 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int l21 = CS2Info.intStack[CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = l21 % j53;
													continue;
												}
												if (opcode == 4012) {
													CS2Info.intStackSize -= 2;
													int k53 = CS2Info.intStack[CS2Info.intStackSize + 1];
													int i22 = CS2Info.intStack[CS2Info.intStackSize];
													if (0 != i22)
														CS2Info.intStack[CS2Info.intStackSize++] = (int) Math.pow(i22,
																k53);
													else
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													continue;
												}
												if (opcode == 4013) {
													CS2Info.intStackSize -= 2;
													int l53 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int j22 = CS2Info.intStack[CS2Info.intStackSize];
													if (j22 == 0) {
														CS2Info.intStack[CS2Info.intStackSize++] = 0;
													} else {
														if (l53 == 0)
															CS2Info.intStack[CS2Info.intStackSize++] = 0x7fffffff;
														else
															CS2Info.intStack[CS2Info.intStackSize++] = (int) Math
																	.pow(j22, 1.0D / (double) l53);
													}
													continue;
												}
												if (opcode == 4014) {
													CS2Info.intStackSize -= 2;
													int i54 = CS2Info.intStack[CS2Info.intStackSize + 1];
													int k22 = CS2Info.intStack[CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = Script.method633(i54,
															k22);
													continue;
												}
												if (opcode == 4015) {
													CS2Info.intStackSize -= 2;
													int l22 = CS2Info.intStack[CS2Info.intStackSize];
													int j54 = CS2Info.intStack[CS2Info.intStackSize + 1];
													CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub29
															.method308(l22, j54);
													continue;
												}
												if (opcode == 4016) {
													CS2Info.intStackSize -= 2;
													int i23 = CS2Info.intStack[CS2Info.intStackSize];
													int k54 = CS2Info.intStack[1 + CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = k54 <= i23 ? k54 : i23;
													continue;
												}
												if (opcode == 4017) {
													CS2Info.intStackSize -= 2;
													int l54 = CS2Info.intStack[1 + CS2Info.intStackSize];
													int j23 = CS2Info.intStack[CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = j23 > l54 ? j23 : l54;
													continue;
												}
												if (opcode != 4018)
													break;
												CS2Info.intStackSize -= 3;
												long l23 = CS2Info.intStack[CS2Info.intStackSize];
												long l70 = CS2Info.intStack[CS2Info.intStackSize + 1];
												long l79 = CS2Info.intStack[2 + CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = (int) ((l23 * l79) / l70);
												continue;
											}
											if (4200 <= opcode) {
												if (opcode >= 4300) {
													if (opcode < 4400) {
														if (4300 != opcode)
															break;
														CS2Info.intStackSize -= 2;
														int k23 = CS2Info.intStack[CS2Info.intStackSize];
														int i55 = CS2Info.intStack[1 + CS2Info.intStackSize];
														Class3_Sub28_Sub9 class3_sub28_sub9 = Class61.method1210(64,
																i55);
														if (!class3_sub28_sub9.method585(0))
															CS2Info.intStack[CS2Info.intStackSize++] = Node
																	.method522(k23, 27112).method1475(i55, -26460,
																			class3_sub28_sub9.anInt3614);
														else
															CS2Info.field1118[CS2Info.stringStackSize++] = Node
																	.method522(k23, 27112).method1477(i55,
																			class3_sub28_sub9.aClass94_3619, true);
														continue;
													}
													if (opcode >= 4500) {
														if (opcode >= 4600) {
															if (opcode < 5100) {
																if (opcode == 5000) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub8.anInt3101;
																	continue;
																}
																if (opcode == 5001) {
																	Class123.anInt1657++;
																	CS2Info.intStackSize -= 3;
																	Class3_Sub13_Sub8.anInt3101 = CS2Info.intStack[CS2Info.intStackSize];
																	Class24.anInt467 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	Class45.anInt734 = CS2Info.intStack[2
																			+ CS2Info.intStackSize];
																	Class3_Sub13_Sub1.outgoingBuffer.putOpcode(157);
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -8,
																			Class3_Sub13_Sub8.anInt3101);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putByte((byte) -126, Class24.anInt467);
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -82,
																			Class45.anInt734);
																	continue;
																}
																if (opcode == 5002) {
																	RSString class94_17 = CS2Info.field1118[--CS2Info.stringStackSize];
																	CS2Info.intStackSize -= 2;
																	int j55 = CS2Info.intStack[CS2Info.intStackSize];
																	Class154.anInt1956++;
																	int j69 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	Class3_Sub13_Sub1.outgoingBuffer.putOpcode(99);
																	Class3_Sub13_Sub1.outgoingBuffer.putLong(
																			class94_17.toLong(-110), 0x868e5910);
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -33,
																			j55 - 1);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putByte((byte) -104, j69);
																	continue;
																}
																if (opcode == 5003) {
																	RSString class94_46 = null;
																	int i24 = CS2Info.intStack[--CS2Info.intStackSize];
																	if (i24 < 100)
																		class94_46 = Class3_Sub29.aClass94Array2580[i24];
																	if (class94_46 == null)
																		class94_46 = Class3_Sub9.aClass94_2331;
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_46;
																	continue;
																}
																if (opcode == 5004) {
																	int j24 = CS2Info.intStack[--CS2Info.intStackSize];
																	int k55 = -1;
																	if (j24 < 100
																			&& null != Class3_Sub29.aClass94Array2580[j24])
																		k55 = Class3_Sub13_Sub6.anIntArray3082[j24];
																	CS2Info.intStack[CS2Info.intStackSize++] = k55;
																	continue;
																}
																if (opcode == 5005) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class24.anInt467;
																	continue;
																}
																if (opcode == 5008) {
																	RSString class94_18 = CS2Info.field1118[--CS2Info.stringStackSize];
																	if (class94_18.method1558(Class9.aClass94_132, 0))
																		Class73.method1308(class94_18, false);
																	else if (Client.rights != 0
																			|| (!Class3_Sub15.aBoolean2433
																					|| Class121.aBoolean1641)
																					&& !Class3_Sub13_Sub14.aBoolean3166) {
																		RSString class94_47 = class94_18
																				.method1534(-98);
																		Class162.anInt2037++;
																		byte byte3 = 0;
																		if (class94_47.method1558(
																				Class3_Sub28_Sub2.aClass94_3548, 0)) {
																			byte3 = 0;
																			class94_18 = class94_18.method1556(
																					Class3_Sub28_Sub2.aClass94_3548
																							.length(-54),
																					(byte) -74);
																		} else if (class94_47.method1558(
																				Class3_Sub20.aClass94_2490, 0)) {
																					class94_18 = class94_18
																							.method1556(
																									Class3_Sub20.aClass94_2490
																											.length(-102),
																									(byte) -74);
																					byte3 = 1;
																				} else {
																			if (class94_47.method1558(
																					ItemDefinition.aClass94_806, 0)) {
																				class94_18 = class94_18.method1556(
																						ItemDefinition.aClass94_806
																								.length(-115),
																						(byte) -74);
																				byte3 = 2;
																			} else {
																				if (class94_47.method1558(
																						Client.aClass94_2197, 0)) {
																					byte3 = 3;
																					class94_18 = class94_18.method1556(
																							Client.aClass94_2197.length(
																									-108),
																							(byte) -74);
																				} else if (class94_47.method1558(
																						Class50.aClass94_833, 0)) {
																					class94_18 = class94_18.method1556(
																							Class50.aClass94_833.length(
																									-62),
																							(byte) -74);
																					byte3 = 4;
																				} else if (class94_47.method1558(
																						ClickMask.aClass94_2214, 0)) {
																					class94_18 = class94_18.method1556(
																							ClickMask.aClass94_2214
																									.length(-46),
																							(byte) -74);
																					byte3 = 5;
																				} else if (class94_47.method1558(
																						Class3_Sub13_Sub20.aClass94_3246,
																						0)) {
																					byte3 = 6;
																					class94_18 = class94_18.method1556(
																							Class3_Sub13_Sub20.aClass94_3246
																									.length(-63),
																							(byte) -74);
																				} else {
																					if (class94_47.method1558(
																							Canvas_Sub2.aClass94_32,
																							0)) {
																						byte3 = 7;
																						class94_18 = class94_18
																								.method1556(
																										Canvas_Sub2.aClass94_32
																												.length(-83),
																										(byte) -74);
																					} else {
																						if (class94_47.method1558(
																								CS2Info.aClass94_3981,
																								0)) {
																							class94_18 = class94_18
																									.method1556(
																											CS2Info.aClass94_3981
																													.length(-92),
																											(byte) -74);
																							byte3 = 8;
																						} else if (class94_47
																								.method1558(
																										Class2.aClass94_60,
																										0)) {
																							byte3 = 9;
																							class94_18 = class94_18
																									.method1556(
																											Class2.aClass94_60
																													.length(-34),
																											(byte) -74);
																						} else {
																							if (class94_47.method1558(
																									Class154.aClass94_1961,
																									0)) {
																								byte3 = 10;
																								class94_18 = class94_18
																										.method1556(
																												Class154.aClass94_1961
																														.length(-126),
																												(byte) -74);
																							} else if (class94_47
																									.method1558(
																											Varp.aClass94_2992,
																											0)) {
																								class94_18 = class94_18
																										.method1556(
																												Varp.aClass94_2992
																														.length(-50),
																												(byte) -74);
																								byte3 = 11;
																							} else {
																								if (0 != Class3_Sub20.language)
																									if (class94_47
																											.method1558(
																													Class3_Sub28_Sub2.aClass94_3544,
																													0)) {
																										byte3 = 0;
																										class94_18 = class94_18
																												.method1556(
																														Class3_Sub28_Sub2.aClass94_3544
																																.length(-116),
																														(byte) -74);
																									} else if (class94_47
																											.method1558(
																													Class3_Sub20.aClass94_2481,
																													0)) {
																										class94_18 = class94_18
																												.method1556(
																														Class3_Sub20.aClass94_2481
																																.length(-80),
																														(byte) -74);
																										byte3 = 1;
																									} else if (class94_47
																											.method1558(
																													ItemDefinition.aClass94_809,
																													0)) {
																										class94_18 = class94_18
																												.method1556(
																														ItemDefinition.aClass94_809
																																.length(-90),
																														(byte) -74);
																										byte3 = 2;
																									} else if (class94_47
																											.method1558(
																													Client.aClass94_2196,
																													0)) {
																										class94_18 = class94_18
																												.method1556(
																														Client.aClass94_2196
																																.length(-34),
																														(byte) -74);
																										byte3 = 3;
																									} else {
																										if (class94_47
																												.method1558(
																														Class50.aClass94_825,
																														0)) {
																											class94_18 = class94_18
																													.method1556(
																															Class50.aClass94_825
																																	.length(-52),
																															(byte) -74);
																											byte3 = 4;
																										} else {
																											if (class94_47
																													.method1558(
																															ClickMask.aClass94_2210,
																															0)) {
																												byte3 = 5;
																												class94_18 = class94_18
																														.method1556(
																																ClickMask.aClass94_2210
																																		.length(-90),
																																(byte) -74);
																											} else if (class94_47
																													.method1558(
																															Class3_Sub13_Sub20.aClass94_3249,
																															0)) {
																												class94_18 = class94_18
																														.method1556(
																																Class3_Sub13_Sub20.aClass94_3249
																																		.length(-100),
																																(byte) -74);
																												byte3 = 6;
																											} else if (class94_47
																													.method1558(
																															Canvas_Sub2.aClass94_36,
																															0)) {
																												byte3 = 7;
																												class94_18 = class94_18
																														.method1556(
																																Canvas_Sub2.aClass94_36
																																		.length(-30),
																																(byte) -74);
																											} else if (class94_47
																													.method1558(
																															CS2Info.aClass94_3988,
																															0)) {
																												byte3 = 8;
																												class94_18 = class94_18
																														.method1556(
																																CS2Info.aClass94_3988
																																		.length(-101),
																																(byte) -74);
																											} else if (class94_47
																													.method1558(
																															Class2.aClass94_62,
																															0)) {
																												byte3 = 9;
																												class94_18 = class94_18
																														.method1556(
																																Class2.aClass94_62
																																		.length(-55),
																																(byte) -74);
																											} else if (class94_47
																													.method1558(
																															Class154.aClass94_1962,
																															0)) {
																												class94_18 = class94_18
																														.method1556(
																																Class154.aClass94_1962
																																		.length(-115),
																																(byte) -74);
																												byte3 = 10;
																											} else if (class94_47
																													.method1558(
																															Varp.aClass94_2991,
																															0)) {
																												class94_18 = class94_18
																														.method1556(
																																Varp.aClass94_2991
																																		.length(-84),
																																(byte) -74);
																												byte3 = 11;
																											}
																										}
																									}
																							}
																						}
																					}
																				}
																			}
																		}
																		byte byte4 = 0;
																		class94_47 = class94_18.method1534(-98);
																		if (class94_47.method1558(
																				Class140_Sub3.aClass94_2740, 0)) {
																			class94_18 = class94_18
																					.method1556(
																							Class140_Sub3.aClass94_2740
																									.length(-105),
																							(byte) -74);
																			byte4 = 1;
																		} else if (class94_47.method1558(
																				Class3_Sub13_Sub26.aClass94_3325, 0)) {
																					byte4 = 2;
																					class94_18 = class94_18.method1556(
																							Class3_Sub13_Sub26.aClass94_3325
																									.length(-117),
																							(byte) -74);
																				} else {
																			if (class94_47.method1558(
																					Class128.aClass94_1689, 0)) {
																				class94_18 = class94_18
																						.method1556(
																								Class128.aClass94_1689
																										.length(-37),
																								(byte) -74);
																				byte4 = 3;
																			} else {
																				if (class94_47.method1558(
																						Class27.aClass94_528, 0)) {
																					byte4 = 4;
																					class94_18 = class94_18.method1556(
																							Class27.aClass94_528.length(
																									-37),
																							(byte) -74);
																				} else {
																					if (class94_47.method1558(
																							GraphicDefinition.aClass94_547,
																							0)) {
																						byte4 = 5;
																						class94_18 = class94_18
																								.method1556(
																										GraphicDefinition.aClass94_547
																												.length(-17),
																										(byte) -74);
																					} else if (0 != Class3_Sub20.language)
																						if (class94_47.method1558(
																								Class140_Sub3.aClass94_2751,
																								0)) {
																							class94_18 = class94_18
																									.method1556(
																											Class140_Sub3.aClass94_2751
																													.length(-74),
																											(byte) -74);
																							byte4 = 1;
																						} else {
																							if (class94_47.method1558(
																									Class3_Sub13_Sub26.aClass94_3333,
																									0)) {
																								byte4 = 2;
																								class94_18 = class94_18
																										.method1556(
																												Class3_Sub13_Sub26.aClass94_3333
																														.length(-106),
																												(byte) -74);
																							} else {
																								if (class94_47
																										.method1558(
																												Class128.aClass94_1688,
																												0)) {
																									byte4 = 3;
																									class94_18 = class94_18
																											.method1556(
																													Class128.aClass94_1688
																															.length(-45),
																													(byte) -74);
																								} else {
																									if (class94_47
																											.method1558(
																													Class27.aClass94_523,
																													0)) {
																										byte4 = 4;
																										class94_18 = class94_18
																												.method1556(
																														Class27.aClass94_523
																																.length(-92),
																														(byte) -74);
																									} else {
																										if (class94_47
																												.method1558(
																														GraphicDefinition.aClass94_551,
																														0)) {
																											class94_18 = class94_18
																													.method1556(
																															GraphicDefinition.aClass94_551
																																	.length(-124),
																															(byte) -74);
																											byte4 = 5;
																										}
																									}
																								}
																							}
																						}
																				}
																			}
																		}
																		Class3_Sub13_Sub1.outgoingBuffer.putOpcode(237);
																		Class3_Sub13_Sub1.outgoingBuffer
																				.putByte((byte) -11, 0);
																		int k79 = Class3_Sub13_Sub1.outgoingBuffer.pos;
																		Class3_Sub13_Sub1.outgoingBuffer
																				.putByte((byte) -34, byte3);
																		Class3_Sub13_Sub1.outgoingBuffer
																				.putByte((byte) -117, byte4);
																		Class85.method1423(false,
																				Class3_Sub13_Sub1.outgoingBuffer,
																				class94_18);
																		Class3_Sub13_Sub1.outgoingBuffer.method769(
																				(byte) -127,
																				-k79 + Class3_Sub13_Sub1.outgoingBuffer.pos);
																	}
																	continue;
																}
																if (opcode == 5009) {
																	CS2Info.stringStackSize -= 2;
																	RSString class94_48 = CS2Info.field1118[CS2Info.stringStackSize
																			+ 1];
																	RSString class94_19 = CS2Info.field1118[CS2Info.stringStackSize];
																	if (Client.rights != 0
																			|| (!Class3_Sub15.aBoolean2433
																					|| Class121.aBoolean1641)
																					&& !Class3_Sub13_Sub14.aBoolean3166) {
																		Class3_Sub13_Sub1.outgoingBuffer.putOpcode(201);
																		Class15.anInt348++;
																		Class3_Sub13_Sub1.outgoingBuffer
																				.putByte((byte) -121, 0);
																		int k69 = Class3_Sub13_Sub1.outgoingBuffer.pos;
																		Class3_Sub13_Sub1.outgoingBuffer.putLong(
																				class94_19.toLong(-128), 0x868e5910);
																		Class85.method1423(false,
																				Class3_Sub13_Sub1.outgoingBuffer,
																				class94_48);
																		Class3_Sub13_Sub1.outgoingBuffer.method769(
																				(byte) -127,
																				Class3_Sub13_Sub1.outgoingBuffer.pos
																						- k69);
																	}
																	continue;
																}
																if (opcode == 5010) {
																	int k24 = CS2Info.intStack[--CS2Info.intStackSize];
																	RSString class94_49 = null;
																	if (k24 < 100)
																		class94_49 = Class3_Sub13_Sub19.aClass94Array3226[k24];
																	if (null == class94_49)
																		class94_49 = Class3_Sub9.aClass94_2331;
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_49;
																	continue;
																}
																if (opcode == 5011) {
																	int l24 = CS2Info.intStack[--CS2Info.intStackSize];
																	RSString class94_50 = null;
																	if (l24 < 100)
																		class94_50 = Class163_Sub3.aClass94Array3003[l24];
																	if (class94_50 == null)
																		class94_50 = Class3_Sub9.aClass94_2331;
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_50;
																	continue;
																}
																if (opcode == 5012) {
																	int i25 = CS2Info.intStack[--CS2Info.intStackSize];
																	int l55 = -1;
																	if (i25 < 100)
																		l55 = Class140.anIntArray1835[i25];
																	CS2Info.intStack[CS2Info.intStackSize++] = l55;
																	continue;
																}
																if (opcode == 5015) {
																	RSString class94_20;
																	if (Client.field3717 == null
																			|| null == Client.field3717.displayName)
																		class94_20 = Class3_Sub28_Sub14.username;
																	else
																		class94_20 = Client.field3717.method1980(0);
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_20;
																	continue;
																}
																if (opcode == 5016) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class45.anInt734;
																	continue;
																}
																if (opcode == 5017) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub9.anInt3114;
																	continue;
																}
																if (5050 == opcode) {
																	int j25 = CS2Info.intStack[--CS2Info.intStackSize];
																	CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub13_Sub35
																			.method336(j25, (byte) -54).aClass94_3538;
																	continue;
																}
																if (opcode == 5051) {
																	int k25 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub28_Sub1 class3_sub28_sub1 = Class3_Sub13_Sub35
																			.method336(k25, (byte) -54);
																	if (class3_sub28_sub1.anIntArray3534 != null)
																		CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub1.anIntArray3534.length;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	continue;
																}
																if (opcode == 5052) {
																	CS2Info.intStackSize -= 2;
																	int l25 = CS2Info.intStack[CS2Info.intStackSize];
																	int i56 = CS2Info.intStack[CS2Info.intStackSize
																			- -1];
																	Class3_Sub28_Sub1 class3_sub28_sub1_2 = Class3_Sub13_Sub35
																			.method336(l25, (byte) -54);
																	int j77 = class3_sub28_sub1_2.anIntArray3534[i56];
																	CS2Info.intStack[CS2Info.intStackSize++] = j77;
																	continue;
																}
																if (opcode == 5053) {
																	int i26 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub28_Sub1 class3_sub28_sub1_1 = Class3_Sub13_Sub35
																			.method336(i26, (byte) -54);
																	if (class3_sub28_sub1_1.anIntArray3540 != null)
																		CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub1_1.anIntArray3540.length;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	continue;
																}
																if (opcode == 5054) {
																	CS2Info.intStackSize -= 2;
																	int j56 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	int j26 = CS2Info.intStack[CS2Info.intStackSize];
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub35
																			.method336(j26,
																					(byte) -54).anIntArray3540[j56];
																	continue;
																}
																if (opcode == 5055) {
																	int k26 = CS2Info.intStack[--CS2Info.intStackSize];
																	CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub29
																			.method733(0xbc614e, k26).method554(-1);
																	continue;
																}
																if (opcode == 5056) {
																	int l26 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub28_Sub4 class3_sub28_sub4 = Class3_Sub29
																			.method733(0xbc614e, l26);
																	if (null != class3_sub28_sub4.anIntArray3567)
																		CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub4.anIntArray3567.length;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	continue;
																}
																if (opcode == 5057) {
																	CS2Info.intStackSize -= 2;
																	int k56 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	int i27 = CS2Info.intStack[CS2Info.intStackSize];
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub29
																			.method733(0xbc614e,
																					i27).anIntArray3567[k56];
																	continue;
																}
																if (opcode == 5058) {
																	Class70.aClass10_1056 = new Class10();
																	Class70.aClass10_1056.anInt149 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class70.aClass10_1056.aClass3_Sub28_Sub4_151 = Class3_Sub29
																			.method733(0xbc614e,
																					Class70.aClass10_1056.anInt149);
																	Class70.aClass10_1056.anIntArray153 = new int[Class70.aClass10_1056.aClass3_Sub28_Sub4_151
																			.method552(true)];
																	continue;
																}
																if (5059 == opcode) {
																	Class3_Sub28_Sub8.anInt3613++;
																	Class3_Sub13_Sub1.outgoingBuffer.putOpcode(167);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putByte((byte) -105, 0);
																	int j27 = Class3_Sub13_Sub1.outgoingBuffer.pos;
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -61,
																			0);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putShort(Class70.aClass10_1056.anInt149);
																	Class70.aClass10_1056.aClass3_Sub28_Sub4_151
																			.method545(Class3_Sub13_Sub1.outgoingBuffer,
																					Class70.aClass10_1056.anIntArray153,
																					false);
																	Class3_Sub13_Sub1.outgoingBuffer.method769(
																			(byte) -126,
																			-j27 + Class3_Sub13_Sub1.outgoingBuffer.pos);
																	continue;
																}
																if (5060 == opcode) {
																	Class148.anInt1906++;
																	RSString class94_21 = CS2Info.field1118[--CS2Info.stringStackSize];
																	Class3_Sub13_Sub1.outgoingBuffer.putOpcode(178);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putByte((byte) -108, 0);
																	int l56 = Class3_Sub13_Sub1.outgoingBuffer.pos;
																	Class3_Sub13_Sub1.outgoingBuffer.putLong(
																			class94_21.toLong(-124), 0x868e5910);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putShort(Class70.aClass10_1056.anInt149);
																	Class70.aClass10_1056.aClass3_Sub28_Sub4_151
																			.method545(Class3_Sub13_Sub1.outgoingBuffer,
																					Class70.aClass10_1056.anIntArray153,
																					false);
																	Class3_Sub13_Sub1.outgoingBuffer.method769(
																			(byte) 108,
																			Class3_Sub13_Sub1.outgoingBuffer.pos
																					+ -l56);
																	continue;
																}
																if (opcode == 5061) {
																	Class3_Sub13_Sub1.outgoingBuffer.putOpcode(167);
																	Class3_Sub28_Sub8.anInt3613++;
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -62,
																			0);
																	int k27 = Class3_Sub13_Sub1.outgoingBuffer.pos;
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -88,
																			1);
																	Class3_Sub13_Sub1.outgoingBuffer
																			.putShort(Class70.aClass10_1056.anInt149);
																	Class70.aClass10_1056.aClass3_Sub28_Sub4_151
																			.method545(Class3_Sub13_Sub1.outgoingBuffer,
																					Class70.aClass10_1056.anIntArray153,
																					false);
																	Class3_Sub13_Sub1.outgoingBuffer.method769(
																			(byte) -126,
																			-k27 + Class3_Sub13_Sub1.outgoingBuffer.pos);
																	continue;
																}
																if (opcode == 5062) {
																	CS2Info.intStackSize -= 2;
																	int i57 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	int l27 = CS2Info.intStack[CS2Info.intStackSize];
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub35
																			.method336(l27,
																					(byte) -54).anIntArray3535[i57];
																	continue;
																}
																if (opcode == 5063) {
																	CS2Info.intStackSize -= 2;
																	int j57 = CS2Info.intStack[CS2Info.intStackSize
																			- -1];
																	int i28 = CS2Info.intStack[CS2Info.intStackSize];
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub35
																			.method336(i28,
																					(byte) -54).anIntArray3533[j57];
																	continue;
																}
																if (5064 == opcode) {
																	CS2Info.intStackSize -= 2;
																	int k57 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	int j28 = CS2Info.intStack[CS2Info.intStackSize];
																	if (k57 != -1)
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub35
																				.method336(j28, (byte) -54)
																				.method529((byte) 50, k57);
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = -1;
																	continue;
																}
																if (opcode == 5065) {
																	CS2Info.intStackSize -= 2;
																	int k28 = CS2Info.intStack[CS2Info.intStackSize];
																	int l57 = CS2Info.intStack[CS2Info.intStackSize
																			+ 1];
																	if (l57 != -1)
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub35
																				.method336(k28, (byte) -54)
																				.method526(l57, 0);
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = -1;
																	continue;
																}
																if (opcode == 5066) {
																	int l28 = CS2Info.intStack[--CS2Info.intStackSize];
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub29
																			.method733(0xbc614e, l28).method552(true);
																	continue;
																}
																if (opcode == 5067) {
																	CS2Info.intStackSize -= 2;
																	int i58 = CS2Info.intStack[CS2Info.intStackSize
																			+ 1];
																	int i29 = CS2Info.intStack[CS2Info.intStackSize];
																	int i70 = Class3_Sub29.method733(0xbc614e, i29)
																			.method550(49, i58);
																	CS2Info.intStack[CS2Info.intStackSize++] = i70;
																	continue;
																}
																if (5068 == opcode) {
																	CS2Info.intStackSize -= 2;
																	int j29 = CS2Info.intStack[CS2Info.intStackSize];
																	int j58 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	Class70.aClass10_1056.anIntArray153[j29] = j58;
																	continue;
																}
																if (opcode == 5069) {
																	CS2Info.intStackSize -= 2;
																	int k29 = CS2Info.intStack[CS2Info.intStackSize];
																	int k58 = CS2Info.intStack[CS2Info.intStackSize
																			+ 1];
																	Class70.aClass10_1056.anIntArray153[k29] = k58;
																	continue;
																}
																if (opcode == 5070) {
																	CS2Info.intStackSize -= 3;
																	int l29 = CS2Info.intStack[CS2Info.intStackSize];
																	int j70 = CS2Info.intStack[CS2Info.intStackSize
																			- -2];
																	int l58 = CS2Info.intStack[CS2Info.intStackSize
																			+ 1];
																	Class3_Sub28_Sub4 class3_sub28_sub4_1 = Class3_Sub29
																			.method733(0xbc614e, l29);
																	if (0 != class3_sub28_sub4_1.method550(73, l58))
																		throw new RuntimeException("bad command");
																	CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub4_1
																			.method549(-117, j70, l58);
																	continue;
																}
																if (opcode == 5071) {
																	RSString class94_22 = CS2Info.field1118[--CS2Info.stringStackSize];
																	boolean flag4 = 1 == CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub28_Sub3.method541((byte) 123, flag4,
																			class94_22);
																	CS2Info.intStack[CS2Info.intStackSize++] = Class62.anInt952;
																	continue;
																}
																if (5072 == opcode) {
																	if (Class99.aShortArray1398 == null
																			|| Class62.anInt952 <= Class140_Sub4.anInt2756)
																		CS2Info.intStack[CS2Info.intStackSize++] = -1;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = Script
																				.method633(
																						Class99.aShortArray1398[Class140_Sub4.anInt2756++],
																						65535);
																	continue;
																}
																if (opcode != 5073)
																	break;
																Class140_Sub4.anInt2756 = 0;
																continue;
															}
															if (5200 > opcode) {
																if (5100 == opcode) {
																	if (!ObjectDefinition.aBooleanArray1490[86])
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 1;
																	continue;
																}
																if (5101 == opcode) {
																	if (ObjectDefinition.aBooleanArray1490[82])
																		CS2Info.intStack[CS2Info.intStackSize++] = 1;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	continue;
																}
																if (5102 != opcode)
																	break;
																if (ObjectDefinition.aBooleanArray1490[81])
																	CS2Info.intStack[CS2Info.intStackSize++] = 1;
																else
																	CS2Info.intStack[CS2Info.intStackSize++] = 0;
																continue;
															}
															if (opcode < 5300) {
																if (opcode == 5200) {
																	Varbit.method1479(
																			CS2Info.intStack[--CS2Info.intStackSize],
																			(byte) 56);
																	continue;
																}
																if (5201 == opcode) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub8
																			.method571(-109);
																	continue;
																}
																if (opcode == 5202) {
																	Class3_Sub24_Sub4.method503((byte) -53,
																			CS2Info.intStack[--CS2Info.intStackSize]);
																	continue;
																}
																if (5203 == opcode) {
																	Class3.method84(
																			CS2Info.field1118[--CS2Info.stringStackSize],
																			-801);
																	continue;
																}
																if (5204 == opcode) {
																	CS2Info.field1118[CS2Info.stringStackSize
																			- 1] = Applet_Sub1.method27(
																					CS2Info.field1118[CS2Info.stringStackSize
																							- 1],
																					true);
																	continue;
																}
																if (5205 == opcode) {
																	Class3_Sub10.method138(
																			CS2Info.field1118[--CS2Info.stringStackSize],
																			0);
																	continue;
																}
																if (opcode == 5206) {
																	int i30 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub28_Sub3 class3_sub28_sub3_4 = NodeList
																			.method884(0x3fff & i30 >> 0x36628f6e,
																					(byte) 111, 0x3fff & i30);
																	if (class3_sub28_sub3_4 != null)
																		CS2Info.field1118[CS2Info.stringStackSize++] = class3_sub28_sub3_4.aClass94_3561;
																	else
																		CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																	continue;
																}
																if (opcode == 5207) {
																	Class3_Sub28_Sub3 class3_sub28_sub3 = Class3_Sub15
																			.method371(2,
																					CS2Info.field1118[--CS2Info.stringStackSize]);
																	if (null != class3_sub28_sub3
																			&& class3_sub28_sub3.aClass94_3554 != null)
																		CS2Info.field1118[CS2Info.stringStackSize++] = class3_sub28_sub3.aClass94_3554;
																	else
																		CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																	continue;
																}
																if (5208 == opcode) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class49.anInt817;
																	CS2Info.intStack[CS2Info.intStackSize++] = Class17.anInt410;
																	continue;
																}
																if (5209 == opcode) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub21.anInt3256
																			+ Class3_Sub28_Sub1.anInt3536;
																	CS2Info.intStack[CS2Info.intStackSize++] = Class2.anInt65
																			+ -Class3_Sub4.anInt2251
																			+ (-1 + Class108.anInt1460);
																	continue;
																}
																if (opcode == 5210) {
																	Class3_Sub28_Sub3 class3_sub28_sub3_1 = Node
																			.method520((byte) -82);
																	if (class3_sub28_sub3_1 == null) {
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	} else {
																		CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub3_1.anInt3558
																				* 64;
																		CS2Info.intStack[CS2Info.intStackSize++] = 64
																				* class3_sub28_sub3_1.anInt3556;
																	}
																	continue;
																}
																if (opcode == 5211) {
																	Class3_Sub28_Sub3 class3_sub28_sub3_2 = Node
																			.method520((byte) -121);
																	if (class3_sub28_sub3_2 == null) {
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	} else {
																		CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub3_2.anInt3559
																				- class3_sub28_sub3_2.anInt3555;
																		CS2Info.intStack[CS2Info.intStackSize++] = -class3_sub28_sub3_2.anInt3562
																				+ class3_sub28_sub3_2.anInt3549;
																	}
																	continue;
																}
																if (opcode == 5212) {
																	int j30 = class208.method1258((byte) -53);
																	int k70 = 0;
																	RSString class94_51;
																	if (j30 == -1) {
																		class94_51 = Class3_Sub9.aClass94_2331;
																	} else {
																		class94_51 = Class119.aClass131_1624.aClass94Array1721[j30];
																		k70 = Class119.aClass131_1624.method1791(j30,
																				8);
																	}
																	class94_51 = class94_51.method1560(
																			Class140_Sub4.aClass94_2765, true,
																			Class7.aClass94_2168);
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_51;
																	CS2Info.intStack[CS2Info.intStackSize++] = k70;
																	continue;
																}
																if (opcode == 5213) {
																	int i71 = 0;
																	int k30 = Class3_Sub13_Sub17.method251(-1);
																	RSString class94_52;
																	if (k30 == -1) {
																		class94_52 = Class3_Sub9.aClass94_2331;
																	} else {
																		class94_52 = Class119.aClass131_1624.aClass94Array1721[k30];
																		i71 = Class119.aClass131_1624.method1791(k30,
																				8);
																	}
																	class94_52 = class94_52.method1560(
																			Class140_Sub4.aClass94_2765, true,
																			Class7.aClass94_2168);
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_52;
																	CS2Info.intStack[CS2Info.intStackSize++] = i71;
																	continue;
																}
																if (opcode == 5214) {
																	int l30 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub28_Sub7.method565((byte) 86,
																			0x3fff & l30 >> 0xa1b3276e, 0x3fff & l30);
																	continue;
																}
																if (opcode == 5215) {
																	int i31 = CS2Info.intStack[--CS2Info.intStackSize];
																	RSString class94_53 = CS2Info.field1118[--CS2Info.stringStackSize];
																	boolean flag10 = false;
																	NodeList class13 = Class109.method1664(
																			0x3fff & i31 >> 0xa4e408ae, 0x3fff & i31,
																			(byte) -118);
																	Class3_Sub28_Sub3 class3_sub28_sub3_5 = (Class3_Sub28_Sub3) class13
																			.method876((byte) 116);
																	do {
																		if (class3_sub28_sub3_5 == null)
																			break;
																		if (class3_sub28_sub3_5.aClass94_3561
																				.method1531(124, class94_53)) {
																			flag10 = true;
																			break;
																		}
																		class3_sub28_sub3_5 = (Class3_Sub28_Sub3) class13
																				.method878(125);
																	} while (true);
																	if (!flag10)
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 1;
																	continue;
																}
																if (opcode == 5216) {
																	int j31 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class3_Sub13_Sub36.method344(j31, 4);
																	continue;
																}
																if (opcode == 5217) {
																	int k31 = CS2Info.intStack[--CS2Info.intStackSize];
																	if (!Class3_Sub10.method140(k31, 20))
																		CS2Info.intStack[CS2Info.intStackSize++] = 0;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = 1;
																	continue;
																}
																if (opcode == 5218) {
																	Class3_Sub28_Sub3 class3_sub28_sub3_3 = Node
																			.method520((byte) -124);
																	if (null != class3_sub28_sub3_3)
																		CS2Info.intStack[CS2Info.intStackSize++] = class3_sub28_sub3_3.anInt3563;
																	else
																		CS2Info.intStack[CS2Info.intStackSize++] = -1;
																	continue;
																}
																if (opcode == 5219) {
																	Class21.method915(
																			CS2Info.field1118[--CS2Info.stringStackSize],
																			-1);
																	continue;
																}
																if (opcode != 5220)
																	break;
																CS2Info.intStack[CS2Info.intStackSize++] = Class140_Sub3.anInt2737 != 100
																		? 0
																		: 1;
																continue;
															}
															if (opcode < 5400) {
																if (opcode == 5300) {
																	CS2Info.intStackSize -= 2;
																	int i59 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	int l31 = CS2Info.intStack[CS2Info.intStackSize];
																	Class140.method1862(false, 3, -8914, l31, i59);
																	CS2Info.intStack[CS2Info.intStackSize++] = null != Class3_Sub13_Sub10.aFrame3121
																			? 1
																			: 0;
																	continue;
																}
																if (opcode == 5301) {
																	if (null != Class3_Sub13_Sub10.aFrame3121)
																		Class140.method1862(false, Node.anInt2577,
																				-8914, -1, -1);
																	continue;
																}
																if (5302 == opcode) {
																	Class106 aclass106[] = Class3.method88((byte) 28);
																	CS2Info.intStack[CS2Info.intStackSize++] = aclass106.length;
																	continue;
																}
																if (5303 == opcode) {
																	int i32 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class106 aclass106_1[] = Class3.method88((byte) 28);
																	CS2Info.intStack[CS2Info.intStackSize++] = aclass106_1[i32].anInt1447;
																	CS2Info.intStack[CS2Info.intStackSize++] = aclass106_1[i32].anInt1449;
																	continue;
																}
																if (opcode == 5305) {
																	int j59 = Class3_Sub13_Sub5.anInt3071;
																	int j32 = Class3_Sub13.anInt2378;
																	int j71 = -1;
																	Class106 aclass106_2[] = Class3.method88((byte) 28);
																	int i80 = 0;
																	do {
																		if (aclass106_2.length <= i80)
																			break;
																		Class106 class106 = aclass106_2[i80];
																		if (j32 == class106.anInt1447
																				&& class106.anInt1449 == j59) {
																			j71 = i80;
																			break;
																		}
																		i80++;
																	} while (true);
																	CS2Info.intStack[CS2Info.intStackSize++] = j71;
																	continue;
																}
																if (opcode == 5306) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class83
																			.method1411(0);
																	continue;
																}
																if (opcode == 5307) {
																	int k32 = CS2Info.intStack[--CS2Info.intStackSize];
																	if (k32 < 0 || k32 > 2)
																		k32 = 0;
																	Class140.method1862(false, k32, -8914, -1, -1);
																	continue;
																}
																if (5308 == opcode) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Node.anInt2577;
																	continue;
																}
																if (5309 != opcode)
																	break;
																int l32 = CS2Info.intStack[--CS2Info.intStackSize];
																if (l32 < 0 || l32 > 2)
																	l32 = 0;
																Node.anInt2577 = l32;
																Class119.method1730(Class38.aClass87_665, (byte) 14);
																continue;
															}
															if (5500 > opcode) {
																if (opcode == 5400) {
																	CS2Info.stringStackSize -= 2;
																	RSString class94_23 = CS2Info.field1118[CS2Info.stringStackSize];
																	RSString class94_54 = CS2Info.field1118[CS2Info.stringStackSize
																			- -1];
																	int k71 = CS2Info.intStack[--CS2Info.intStackSize];
																	Class142.anInt1853++;
																	Class3_Sub13_Sub1.outgoingBuffer.putOpcode(117);
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -91,
																			Class3_Sub13_Sub33.method326((byte) 39,
																					class94_23)
																					- (-Class3_Sub13_Sub33.method326(
																							(byte) 102, class94_54)
																							+ -1));
																	Class3_Sub13_Sub1.outgoingBuffer.putString(0,
																			class94_23);
																	Class3_Sub13_Sub1.outgoingBuffer.putString(0,
																			class94_54);
																	Class3_Sub13_Sub1.outgoingBuffer.putByte((byte) -79,
																			k71);
																	continue;
																}
																if (opcode == 5401) {
																	CS2Info.intStackSize -= 2;
																	Class3_Sub13_Sub38.aShortArray3455[CS2Info.intStack[CS2Info.intStackSize]] = (short) Class56
																			.method1186(0,
																					CS2Info.intStack[CS2Info.intStackSize
																							+ 1]);
																	Applet_Sub1.method28(true);
																	Buffer.method746((byte) -29);
																	Class167.method2265(0);
																	WorldListEntry.method1076(88);
																	Class47.method1093(false);
																	continue;
																}
																if (opcode == 5405) {
																	CS2Info.intStackSize -= 2;
																	int i33 = CS2Info.intStack[CS2Info.intStackSize];
																	int k59 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	if (i33 >= 0 && i33 < 2)
																		Class58.anIntArrayArrayArray911[i33] = new int[k59 << 0x27e6541][4];
																	continue;
																}
																if (opcode == 5406) {
																	CS2Info.intStackSize -= 7;
																	int j33 = CS2Info.intStack[CS2Info.intStackSize];
																	int l59 = CS2Info.intStack[1
																			+ CS2Info.intStackSize] << 0xee994ea1;
																	int k77 = CS2Info.intStack[CS2Info.intStackSize
																			- -3];
																	int l71 = CS2Info.intStack[2
																			+ CS2Info.intStackSize];
																	int j80 = CS2Info.intStack[4
																			+ CS2Info.intStackSize];
																	int j82 = CS2Info.intStack[6
																			+ CS2Info.intStackSize];
																	int l81 = CS2Info.intStack[5
																			+ CS2Info.intStackSize];
																	if (j33 >= 0 && j33 < 2
																			&& null != Class58.anIntArrayArrayArray911[j33]
																			&& l59 >= 0
																			&& Class58.anIntArrayArrayArray911[j33].length > l59) {
																		Class58.anIntArrayArrayArray911[j33][l59] = (new int[] {
																				(Script.method633(0xfffc3b9,
																						l71) >> 0x5f43122e) * 128,
																				k77, 128 * Script.method633(l71, 16383),
																				j82 });
																		Class58.anIntArrayArrayArray911[j33][l59
																				+ 1] = (new int[] {
																						128 * (Script.method633(j80,
																								0xfffed27) >> 0xe3e5364e),
																						l81, 128 * Script.method633(j80,
																								16383) });
																	}
																	continue;
																}
																if (opcode == 5407) {
																	int k33 = Class58.anIntArrayArrayArray911[CS2Info.intStack[--CS2Info.intStackSize]].length >> 0x2b932e01;
																	CS2Info.intStack[CS2Info.intStackSize++] = k33;
																	continue;
																}
																if (opcode == 5411) {
																	if (Class3_Sub13_Sub10.aFrame3121 != null)
																		Class140.method1862(false, Node.anInt2577,
																				-8914, -1, -1);
																	if (null == Class3_Sub13_Sub7.aFrame3092)
																		Class99.method1596(Widget.method856(true),
																				(byte) 126, false);
																	else
																		System.exit(0);
																	continue;
																}
																if (opcode == 5419) {
																	RSString class94_24 = Class3_Sub9.aClass94_2331;
																	if (null != Class136.aClass64_1778) {
																		class94_24 = Class108.method1653(
																				Class136.aClass64_1778.anInt979, 0);
																		if (Class136.aClass64_1778.anObject974 != null) {
																			byte abyte0[] = null;
																			try {
																				abyte0 = ((String) Class136.aClass64_1778.anObject974)
																						.getBytes("ISO-8859-1");
																			} catch (UnsupportedEncodingException e) {
																				// TODO Auto-generated catch block
																				e.printStackTrace();
																			}
																			class94_24 = Class3_Sub13_Sub3.method178(
																					abyte0, -4114, abyte0.length, 0);
																		}
																	}
																	CS2Info.field1118[CS2Info.stringStackSize++] = class94_24;
																	continue;
																}
																if (opcode == 5420) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Signlink.anInt1214 != 3
																			? 0
																			: 1;
																	continue;
																}
																if (opcode == 5421) {
																	if (null != Class3_Sub13_Sub10.aFrame3121)
																		Class140.method1862(false, Node.anInt2577,
																				-8914, -1, -1);
																	boolean flag5 = 1 == CS2Info.intStack[--CS2Info.intStackSize];
																	RSString class94_25 = CS2Info.field1118[--CS2Info.stringStackSize];
																	RSString class94_64 = RenderAnimationDefinition
																			.method903(new RSString[] {
																					Widget.method856(true),
																					class94_25 }, (byte) -71);
																	if (null == Class3_Sub13_Sub7.aFrame3092 && (!flag5
																			|| Signlink.anInt1214 == 3
																			|| !Signlink.osName.startsWith("win")
																			|| Class106.hasInternetExplorer6)) {
																		Class99.method1596(class94_64, (byte) 127,
																				flag5);
																	} else {
																		RSString.aBoolean2154 = flag5;
																		Class3_Sub13_Sub24.aClass94_3295 = class94_64;
																		try {
																			Class15.aClass64_351 = Class38.aClass87_665
																					.method1452(
																							new String(class94_64
																									.method1568(0),
																									"ISO-8859-1"),
																							true);
																		} catch (UnsupportedEncodingException e) {
																			// TODO Auto-generated catch block
																			e.printStackTrace();
																		}
																	}
																	continue;
																}
																if (5422 == opcode) {
																	int i72 = CS2Info.intStack[--CS2Info.intStackSize];
																	CS2Info.stringStackSize -= 2;
																	RSString class94_55 = CS2Info.field1118[1
																			+ CS2Info.stringStackSize];
																	RSString class94_26 = CS2Info.field1118[CS2Info.stringStackSize];
																	if (class94_26.length(-127) > 0) {
																		if (null == Class3_Sub30_Sub1.aClass94Array3802)
																			Class3_Sub30_Sub1.aClass94Array3802 = new RSString[Class3_Sub13_Sub18.anIntArray3218[Class158.anInt2014]];
																		Class3_Sub30_Sub1.aClass94Array3802[i72] = class94_26;
																	}
																	if (class94_55.length(-118) > 0) {
																		if (OutputStream_Sub1.aClass94Array45 == null)
																			OutputStream_Sub1.aClass94Array45 = new RSString[Class3_Sub13_Sub18.anIntArray3218[Class158.anInt2014]];
																		OutputStream_Sub1.aClass94Array45[i72] = class94_55;
																	}
																	continue;
																}
																if (opcode == 5423) {
																	CS2Info.field1118[--CS2Info.stringStackSize]
																			.method1549(false);
																	continue;
																}
																if (5424 == opcode) {
																	CS2Info.intStackSize -= 11;
																	Class3_Sub28_Sub6.anInt3600 = CS2Info.intStack[CS2Info.intStackSize];
																	Class62.anInt963 = CS2Info.intStack[CS2Info.intStackSize
																			- -1];
																	Class149.anInt1926 = CS2Info.intStack[CS2Info.intStackSize
																			+ 2];
																	Class136.anInt1771 = CS2Info.intStack[3
																			+ CS2Info.intStackSize];
																	WorldListCountry.anInt502 = CS2Info.intStack[4
																			+ CS2Info.intStackSize];
																	Class99.anInt1400 = CS2Info.intStack[5
																			+ CS2Info.intStackSize];
																	Class46.anInt739 = CS2Info.intStack[6
																			+ CS2Info.intStackSize];
																	Class79.anInt1126 = CS2Info.intStack[7
																			+ CS2Info.intStackSize];
																	Class140_Sub7.anInt2937 = CS2Info.intStack[8
																			+ CS2Info.intStackSize];
																	Class3_Sub13_Sub28.anInt3351 = CS2Info.intStack[CS2Info.intStackSize
																			+ 9];
																	Class154.anInt1957 = CS2Info.intStack[10
																			+ CS2Info.intStackSize];
																	JS5Group.index8.method2144(0,
																			WorldListCountry.anInt502);
																	JS5Group.index8.method2144(0, Class99.anInt1400);
																	JS5Group.index8.method2144(0, Class46.anInt739);
																	JS5Group.index8.method2144(0, Class79.anInt1126);
																	JS5Group.index8.method2144(0,
																			Class140_Sub7.anInt2937);
																	JS5Index.aBoolean1951 = true;
																	continue;
																}
																if (opcode == 5425) {
																	Class3_Sub13.method165(-7878);
																	JS5Index.aBoolean1951 = false;
																	continue;
																}
																if (opcode == 5426) {
																	Class161.anInt2027 = CS2Info.intStack[--CS2Info.intStackSize];
																	continue;
																}
																if (opcode != 5427)
																	break;
																CS2Info.intStackSize -= 2;
																Class99.anInt1403 = CS2Info.intStack[CS2Info.intStackSize];
																Class131.anInt1719 = CS2Info.intStack[CS2Info.intStackSize
																		+ 1];
																continue;
															}
															if (5600 > opcode) {
																if (5500 == opcode) {
																	CS2Info.intStackSize -= 4;
																	int l33 = CS2Info.intStack[CS2Info.intStackSize];
																	int l77 = CS2Info.intStack[CS2Info.intStackSize
																			- -3];
																	int j72 = CS2Info.intStack[CS2Info.intStackSize
																			- -2];
																	int i60 = CS2Info.intStack[CS2Info.intStackSize
																			+ 1];
																	Class3_Sub20.method390(false, j72, i60, l77,
																			(byte) -128,
																			-Client.field1152 + (0x3fff & l33),
																			((0xffffe30 & l33) >> 0x372f8c2e)
																					- Client.field590);
																	continue;
																}
																if (opcode == 5501) {
																	CS2Info.intStackSize -= 4;
																	int j60 = CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	int i34 = CS2Info.intStack[CS2Info.intStackSize];
																	int i78 = CS2Info.intStack[CS2Info.intStackSize
																			- -3];
																	int k72 = CS2Info.intStack[CS2Info.intStackSize
																			+ 2];
																	Class164_Sub1.method2238(j60,
																			(0x3fff & i34) - Client.field1152, k72,
																			-Client.field590
																					+ ((0xffff221 & i34) >> 0xcd90732e),
																			(byte) -21, i78);
																	continue;
																}
																if (opcode == 5502) {
																	CS2Info.intStackSize -= 6;
																	int j34 = CS2Info.intStack[CS2Info.intStackSize];
																	if (j34 >= 2)
																		throw new RuntimeException();
																	Varbit.anInt1252 = j34;
																	int k60 = CS2Info.intStack[CS2Info.intStackSize
																			- -1];
																	if (1 + k60 >= Class58.anIntArrayArrayArray911[Varbit.anInt1252].length >> 0x84afc601)
																		throw new RuntimeException();
																	Class73.anInt1081 = k60;
																	Class163_Sub2_Sub1.anInt4020 = 0;
																	Class134.anInt1759 = CS2Info.intStack[CS2Info.intStackSize
																			+ 2];
																	Class3_Sub13.anInt2383 = CS2Info.intStack[CS2Info.intStackSize
																			+ 3];
																	int l72 = CS2Info.intStack[CS2Info.intStackSize
																			+ 4];
																	if (2 <= l72)
																		throw new RuntimeException();
																	Class3_Sub7.anInt2293 = l72;
																	int j78 = CS2Info.intStack[5
																			+ CS2Info.intStackSize];
																	if (Class58.anIntArrayArrayArray911[Class3_Sub7.anInt2293].length >> 0x4d73ee21 <= 1
																			+ j78)
																		throw new RuntimeException();
																	Class39.anInt670 = j78;
																	Class133.anInt1753 = 3;
																	continue;
																}
																if (opcode == 5503) {
																	Class3_Sub28_Sub5.method560(-21556);
																	continue;
																}
																if (5504 == opcode) {
																	CS2Info.intStackSize -= 2;
																	Class3_Sub9.anInt2309 = CS2Info.intStack[CS2Info.intStackSize];
																	GraphicDefinition.anInt531 = CS2Info.intStack[CS2Info.intStackSize
																			+ 1];
																	if (Class133.anInt1753 == 2) {
																		Class3_Sub13_Sub25.anInt3315 = GraphicDefinition.anInt531;
																		Class139.anInt1823 = Class3_Sub9.anInt2309;
																	}
																	Class47.method1098((byte) -74);
																	continue;
																}
																if (opcode == 5505) {
																	CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub9.anInt2309;
																	continue;
																}
																if (5506 != opcode)
																	break;
																CS2Info.intStack[CS2Info.intStackSize++] = GraphicDefinition.anInt531;
																continue;
															}
															if (opcode >= 5700) {
																if (6100 > opcode) {
																	if (opcode == 6001) {
																		int k34 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (k34 < 1)
																			k34 = 1;
																		if (k34 > 4)
																			k34 = 4;
																		Class3_Sub28_Sub10.anInt3625 = k34;
																		if (!Class138.aBoolean1807
																				|| !Class106.aBoolean1441) {
																			if (Class3_Sub28_Sub10.anInt3625 == 1)
																				Class51.method1137(0.9F);
																			if (Class3_Sub28_Sub10.anInt3625 == 2)
																				Class51.method1137(0.8F);
																			if (3 == Class3_Sub28_Sub10.anInt3625)
																				Class51.method1137(0.7F);
																			if (Class3_Sub28_Sub10.anInt3625 == 4)
																				Class51.method1137(0.6F);
																		}
																		if (Class138.aBoolean1807) {
																			Class3_Sub13_Sub14.method236((byte) 64);
																			if (!Class106.aBoolean1441)
																				Class84.method1417(104);
																		}
																		Buffer.method746((byte) -29);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6002) {
																		Class25.method957(96,
																				1 == CS2Info.intStack[--CS2Info.intStackSize]);
																		Class3_Sub10.method139(66);
																		Class84.method1417(101);
																		Buffer.method792(0x8c1111);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6003) {
																		Class3_Sub28_Sub7.aBoolean3604 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Buffer.method792(0x8c1111);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6005) {
																		Class148.aBoolean1905 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Class84.method1417(112);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6006) {
																		Class25.aBoolean488 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		((Class102) Class51.anInterface2_838)
																				.method1616(!Class25.aBoolean488,
																						-17830);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6007) {
																		Widget.aBoolean236 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6008) {
																		WorldListEntry.aBoolean2623 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6009) {
																		Class3_Sub13_Sub22.aBoolean3275 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6010) {
																		Class140_Sub6.aBoolean2910 = 1 == CS2Info.intStack[--CS2Info.intStackSize];
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6011) {
																		int l34 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (l34 < 0 || l34 > 2)
																			l34 = 0;
																		Class80.anInt1137 = l34;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (6012 == opcode) {
																		if (Class138.aBoolean1807)
																			Class3_Sub28_Sub4.method551(0, 0, 0);
																		Class106.aBoolean1441 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		if (Class138.aBoolean1807
																				&& Class106.aBoolean1441) {
																			Class51.method1137(0.7F);
																		} else {
																			if (Class3_Sub28_Sub10.anInt3625 == 1)
																				Class51.method1137(0.9F);
																			if (Class3_Sub28_Sub10.anInt3625 == 2)
																				Class51.method1137(0.8F);
																			if (Class3_Sub28_Sub10.anInt3625 == 3)
																				Class51.method1137(0.7F);
																			if (Class3_Sub28_Sub10.anInt3625 == 4)
																				Class51.method1137(0.6F);
																		}
																		Class84.method1417(108);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6014) {
																		Class128.aBoolean1685 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		if (Class138.aBoolean1807)
																			Class84.method1417(109);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6015) {
																		Class38.aBoolean661 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		if (Class138.aBoolean1807)
																			Class3_Sub13_Sub14.method236((byte) 64);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (6016 == opcode) {
																		int i35 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (Class138.aBoolean1807)
																			Class3_Sub28_Sub5.aBoolean3593 = true;
																		if (0 > i35 || i35 > 2)
																			i35 = 0;
																		Class3_Sub28_Sub14.anInt3671 = i35;
																		continue;
																	}
																	if (opcode == 6017) {
																		Class3_Sub13_Sub15.aBoolean3184 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Applet_Sub1.method34(-32589);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6018) {
																		int j35 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (j35 < 0)
																			j35 = 0;
																		if (j35 > 127)
																			j35 = 127;
																		ScriptEvent.anInt2453 = j35;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6019) {
																		int k35 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (k35 < 0)
																			k35 = 0;
																		if (k35 > 255)
																			k35 = 255;
																		if (Class9.anInt120 != k35) {
																			if (Class9.anInt120 == 0
																					&& Class129.anInt1691 != -1) {
																				Class70.method1285(JS5Group.index6,
																						false, Class129.anInt1691, 0,
																						false, k35);
																				Class83.aBoolean1158 = false;
																			} else if (k35 == 0) {
																				Class140.method1870(false);
																				Class83.aBoolean1158 = false;
																			} else {
																				Class3_Sub29.method736(k35, 115);
																			}
																			Class9.anInt120 = k35;
																		}
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6020) {
																		int l35 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (l35 < 0)
																			l35 = 0;
																		if (127 < l35)
																			l35 = 127;
																		Class14.anInt340 = l35;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		continue;
																	}
																	if (opcode == 6021) {
																		Class73.aBoolean1084 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																		Buffer.method792(0x8c1111);
																		continue;
																	}
																	if (opcode == 6023) {
																		int i36 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (0 > i36)
																			i36 = 0;
																		if (i36 > 2)
																			i36 = 2;
																		boolean flag6 = false;
																		if (96 > Class3_Sub24_Sub3.anInt3492) {
																			flag6 = true;
																			i36 = 0;
																		}
																		Class127_Sub1.method1758(i36);
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		Class140_Sub2.aBoolean2705 = false;
																		CS2Info.intStack[CS2Info.intStackSize++] = flag6
																				? 0
																				: 1;
																		continue;
																	}
																	if (opcode == 6024) {
																		int j36 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (j36 < 0 || 2 < j36)
																			j36 = 0;
																		Class3_Sub28_Sub9.anInt3622 = j36;
																		Class119.method1730(Class38.aClass87_665,
																				(byte) 14);
																		continue;
																	}
																	if (opcode != 6028)
																		break;
																	Class163_Sub3.aBoolean3004 = CS2Info.intStack[--CS2Info.intStackSize] != 0;
																	Class119.method1730(Class38.aClass87_665,
																			(byte) 14);
																	continue;
																}
																if (opcode < 6200) {
																	if (opcode == 6101) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub10.anInt3625;
																		continue;
																	}
																	if (opcode == 6102) {
																		CS2Info.intStack[CS2Info.intStackSize++] = CS2Info
																				.method1986(109) ? 1 : 0;
																		continue;
																	}
																	if (opcode == 6103) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub7.aBoolean3604
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6105) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class148.aBoolean1905
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6106) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class25.aBoolean488
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6107) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Widget.aBoolean236
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6108) {
																		CS2Info.intStack[CS2Info.intStackSize++] = WorldListEntry.aBoolean2623
																				? 1
																				: 0;
																		continue;
																	}
																	if (6109 == opcode) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub22.aBoolean3275
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6110) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class140_Sub6.aBoolean2910
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6111) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class80.anInt1137;
																		continue;
																	}
																	if (6112 == opcode) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class106.aBoolean1441
																				? 1
																				: 0;
																		continue;
																	}
																	if (6114 == opcode) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class128.aBoolean1685
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6115) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class38.aBoolean661
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6116) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub14.anInt3671;
																		continue;
																	}
																	if (6117 == opcode) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub15.aBoolean3184
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode == 6118) {
																		CS2Info.intStack[CS2Info.intStackSize++] = ScriptEvent.anInt2453;
																		continue;
																	}
																	if (6119 == opcode) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class9.anInt120;
																		continue;
																	}
																	if (opcode == 6120) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class14.anInt340;
																		continue;
																	}
																	if (opcode == 6121) {
																		if (Class138.aBoolean1807)
																			CS2Info.intStack[CS2Info.intStackSize++] = Class138.aBoolean1809
																					? 1
																					: 0;
																		else
																			CS2Info.intStack[CS2Info.intStackSize++] = 0;
																		continue;
																	}
																	if (opcode == 6123) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class127_Sub1
																				.method1757();
																		continue;
																	}
																	if (opcode == 6124) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub9.anInt3622;
																		continue;
																	}
																	if (opcode != 6128)
																		break;
																	CS2Info.intStack[CS2Info.intStackSize++] = Class163_Sub3.aBoolean3004
																			? 1
																			: 0;
																	continue;
																}
																if (opcode >= 6300) {
																	if (opcode < 6400) {
																		if (opcode == 6300) {
																			CS2Info.intStack[CS2Info.intStackSize++] = (int) (Class5
																					.method830((byte) -55) / 60000L);
																			continue;
																		}
																		if (opcode == 6301) {
																			CS2Info.intStack[CS2Info.intStackSize++] = -11745
																					+ (int) (Class5.method830(
																							(byte) -55) / 0x5265c00L);
																			continue;
																		}
																		if (opcode == 6302) {
																			CS2Info.intStackSize -= 3;
																			int i73 = CS2Info.intStack[CS2Info.intStackSize
																					+ 2];
																			int l60 = CS2Info.intStack[CS2Info.intStackSize
																					- -1];
																			int k36 = CS2Info.intStack[CS2Info.intStackSize];
																			Class3_Sub28_Sub9.aCalendar3616.clear();
																			Class3_Sub28_Sub9.aCalendar3616.set(11, 12);
																			Class3_Sub28_Sub9.aCalendar3616.set(i73,
																					l60, k36);
																			CS2Info.intStack[CS2Info.intStackSize++] = -11745
																					+ (int) (Class3_Sub28_Sub9.aCalendar3616
																							.getTime().getTime()
																							/ 0x5265c00L);
																			continue;
																		}
																		if (6303 == opcode) {
																			Class3_Sub28_Sub9.aCalendar3616.clear();
																			Class3_Sub28_Sub9.aCalendar3616
																					.setTime(new Date(Class5
																							.method830((byte) -55)));
																			CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub9.aCalendar3616
																					.get(1);
																			continue;
																		}
																		if (opcode != 6304)
																			break;
																		boolean flag7 = true;
																		int l36 = CS2Info.intStack[--CS2Info.intStackSize];
																		if (l36 >= 0) {
																			if (l36 >= 1582) {
																				if (l36 % 4 == 0) {
																					if (l36 % 100 != 0)
																						flag7 = true;
																					else if (0 != l36 % 400)
																						flag7 = false;
																				} else {
																					flag7 = false;
																				}
																			} else {
																				flag7 = l36 % 4 == 0;
																			}
																		} else {
																			flag7 = (1 + l36) % 4 == 0;
																		}
																		CS2Info.intStack[CS2Info.intStackSize++] = flag7
																				? 1
																				: 0;
																		continue;
																	}
																	if (opcode >= 6500) {
																		if (opcode < 6600) {
																			if (opcode == 6500) {
																				if (Class143.anInt1875 != 10
																						|| Class3_Sub13_Sub31.anInt3375 != 0
																						|| 0 != Class3_Sub13_Sub25.loginStage
																						|| 0 != Canvas_Sub1.anInt23)
																					CS2Info.intStack[CS2Info.intStackSize++] = 1;
																				else
																					CS2Info.intStack[CS2Info.intStackSize++] = Class121
																							.method1735(29984) == -1 ? 0
																									: 1;
																				continue;
																			}
																			if (opcode == 6501) {
																				WorldListEntry class44_sub1 = Class140_Sub2
																						.method1953((byte) 124);
																				if (class44_sub1 == null) {
																					CS2Info.intStack[CS2Info.intStackSize++] = -1;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																					CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																					CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																				} else {
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1.worldId;
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1.settings;
																					CS2Info.field1118[CS2Info.stringStackSize++] = class44_sub1.activity;
																					WorldListCountry class26 = class44_sub1
																							.method1078(60);
																					CS2Info.intStack[CS2Info.intStackSize++] = class26.flagId;
																					CS2Info.field1118[CS2Info.stringStackSize++] = class26.name;
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1.anInt722;
																				}
																				continue;
																			}
																			if (opcode == 6502) {
																				WorldListEntry class44_sub1_1 = ItemDefinition
																						.method1107(5422);
																				if (null == class44_sub1_1) {
																					CS2Info.intStack[CS2Info.intStackSize++] = -1;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																					CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																					CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																				} else {
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1_1.worldId;
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1_1.settings;
																					CS2Info.field1118[CS2Info.stringStackSize++] = class44_sub1_1.activity;
																					WorldListCountry class26_1 = class44_sub1_1
																							.method1078(70);
																					CS2Info.intStack[CS2Info.intStackSize++] = class26_1.flagId;
																					CS2Info.field1118[CS2Info.stringStackSize++] = class26_1.name;
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1_1.anInt722;
																				}
																				continue;
																			}
																			if (opcode == 6503) {
																				int i37 = CS2Info.intStack[--CS2Info.intStackSize];
																				if (Class143.anInt1875 != 10
																						|| Class3_Sub13_Sub31.anInt3375 != 0
																						|| Class3_Sub13_Sub25.loginStage != 0
																						|| Canvas_Sub1.anInt23 != 0)
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																				else
																					CS2Info.intStack[CS2Info.intStackSize++] = Class104
																							.method1627(i37, (byte) -7)
																									? 1
																									: 0;
																				continue;
																			}
																			if (opcode == 6504) {
																				RSString.anInt2148 = CS2Info.intStack[--CS2Info.intStackSize];
																				Class119.method1730(
																						Class38.aClass87_665,
																						(byte) 14);
																				continue;
																			}
																			if (6505 == opcode) {
																				CS2Info.intStack[CS2Info.intStackSize++] = RSString.anInt2148;
																				continue;
																			}
																			if (opcode == 6506) {
																				int j37 = CS2Info.intStack[--CS2Info.intStackSize];
																				WorldListEntry class44_sub1_2 = Class3_Sub8
																						.method130(120, j37);
																				if (class44_sub1_2 == null) {
																					CS2Info.intStack[CS2Info.intStackSize++] = -1;
																					CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																					CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
																					CS2Info.intStack[CS2Info.intStackSize++] = 0;
																				} else {
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1_2.settings;
																					CS2Info.field1118[CS2Info.stringStackSize++] = class44_sub1_2.activity;
																					WorldListCountry class26_2 = class44_sub1_2
																							.method1078(-87);
																					CS2Info.intStack[CS2Info.intStackSize++] = class26_2.flagId;
																					CS2Info.field1118[CS2Info.stringStackSize++] = class26_2.name;
																					CS2Info.intStack[CS2Info.intStackSize++] = class44_sub1_2.anInt722;
																				}
																				continue;
																			}
																			if (opcode != 6507)
																				break;
																			CS2Info.intStackSize -= 4;
																			int j73 = CS2Info.intStack[CS2Info.intStackSize
																					+ 2];
																			int k37 = CS2Info.intStack[CS2Info.intStackSize];
																			boolean flag11 = CS2Info.intStack[CS2Info.intStackSize
																					- -3] == 1;
																			boolean flag8 = CS2Info.intStack[1
																					+ CS2Info.intStackSize] == 1;
																			Class134.method1808(j73, flag8, (byte) 30,
																					k37, flag11);
																			continue;
																		}
																		if (opcode >= 6700)
																			break;
																		if (6600 == opcode) {
																			Class15.aBoolean346 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
																			Class119.method1730(Class38.aClass87_665,
																					(byte) 14);
																			continue;
																		}
																		if (opcode != 6601)
																			break;
																		CS2Info.intStack[CS2Info.intStackSize++] = Class15.aBoolean346
																				? 1
																				: 0;
																		continue;
																	}
																	if (6405 == opcode) {
																		CS2Info.intStack[CS2Info.intStackSize++] = Class47
																				.method1088(false) ? 1 : 0;
																		continue;
																	}
																	if (opcode != 6406)
																		break;
																	CS2Info.intStack[CS2Info.intStackSize++] = Class159
																			.method2194(255) ? 1 : 0;
																	continue;
																}
																if (opcode == 6200) {
																	CS2Info.intStackSize -= 2;
																	Class106.aShort1444 = (short) CS2Info.intStack[CS2Info.intStackSize];
																	if (0 >= Class106.aShort1444)
																		Class106.aShort1444 = 256;
																	Class3_Sub13_Sub3.aShort3052 = (short) CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	if (Class3_Sub13_Sub3.aShort3052 <= 0)
																		Class3_Sub13_Sub3.aShort3052 = 205;
																	continue;
																}
																if (opcode == 6201) {
																	CS2Info.intStackSize -= 2;
																	OutputStream_Sub1.aShort46 = (short) CS2Info.intStack[CS2Info.intStackSize];
																	if (OutputStream_Sub1.aShort46 <= 0)
																		OutputStream_Sub1.aShort46 = 256;
																	ObjectDefinition.aShort1535 = (short) CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	if (ObjectDefinition.aShort1535 <= 0)
																		ObjectDefinition.aShort1535 = 320;
																	continue;
																}
																if (opcode == 6202) {
																	CS2Info.intStackSize -= 4;
																	Class3_Sub13_Sub19.aShort3241 = (short) CS2Info.intStack[CS2Info.intStackSize];
																	if (Class3_Sub13_Sub19.aShort3241 <= 0)
																		Class3_Sub13_Sub19.aShort3241 = 1;
																	PacketParser.aShort83 = (short) CS2Info.intStack[1
																			+ CS2Info.intStackSize];
																	if (PacketParser.aShort83 > 0) {
																		if (Class3_Sub13_Sub19.aShort3241 > PacketParser.aShort83)
																			PacketParser.aShort83 = Class3_Sub13_Sub19.aShort3241;
																	} else {
																		PacketParser.aShort83 = 32767;
																	}
																	WorldListCountry.aShort505 = (short) CS2Info.intStack[2
																			+ CS2Info.intStackSize];
																	if (WorldListCountry.aShort505 <= 0)
																		WorldListCountry.aShort505 = 1;
																	Class3_Sub13_Sub23_Sub1.aShort4038 = (short) CS2Info.intStack[CS2Info.intStackSize
																			- -3];
																	if (Class3_Sub13_Sub23_Sub1.aShort4038 > 0) {
																		if (WorldListCountry.aShort505 > Class3_Sub13_Sub23_Sub1.aShort4038)
																			Class3_Sub13_Sub23_Sub1.aShort4038 = WorldListCountry.aShort505;
																	} else {
																		Class3_Sub13_Sub23_Sub1.aShort4038 = 32767;
																	}
																	continue;
																}
																if (opcode == 6203) {
																	Class65.method1239(Class168.aClass11_2091.field2582,
																			81, 0, Class168.aClass11_2091.field2642, 0,
																			false);
																	CS2Info.intStack[CS2Info.intStackSize++] = Class96.anInt1358;
																	CS2Info.intStack[CS2Info.intStackSize++] = Canvas_Sub2.anInt31;
																	continue;
																}
																if (6204 == opcode) {
																	CS2Info.intStack[CS2Info.intStackSize++] = OutputStream_Sub1.aShort46;
																	CS2Info.intStack[CS2Info.intStackSize++] = ObjectDefinition.aShort1535;
																	continue;
																}
																if (opcode != 6205)
																	break;
																CS2Info.intStack[CS2Info.intStackSize++] = Class106.aShort1444;
																CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub3.aShort3052;
																continue;
															}
															if (opcode == 5600) {
																CS2Info.stringStackSize -= 2;
																RSString class94_27 = CS2Info.field1118[CS2Info.stringStackSize];
																RSString class94_56 = CS2Info.field1118[CS2Info.stringStackSize
																		+ 1];
																int k73 = CS2Info.intStack[--CS2Info.intStackSize];
																if (Class143.anInt1875 == 10
																		&& Class3_Sub13_Sub31.anInt3375 == 0
																		&& Class3_Sub13_Sub25.loginStage == 0
																		&& Canvas_Sub1.anInt23 == 0
																		&& Class43.anInt692 == 0)
																	Class131.method1793(class94_27, class94_56, k73,
																			(byte) -38);
																continue;
															}
															if (opcode == 5601) {
																Class110.method1681(-1);
																continue;
															}
															if (opcode == 5602) {
																if (0 == Class3_Sub13_Sub25.loginStage)
																	Class158.anInt2005 = -2;
																continue;
															}
															if (opcode == 5603) {
																CS2Info.intStackSize -= 4;
																if (Class143.anInt1875 == 10
																		&& 0 == Class3_Sub13_Sub31.anInt3375
																		&& Class3_Sub13_Sub25.loginStage == 0
																		&& Canvas_Sub1.anInt23 == 0
																		&& Class43.anInt692 == 0)
																	ScriptEvent.method377(
																			CS2Info.intStack[CS2Info.intStackSize - -2],
																			CS2Info.intStack[CS2Info.intStackSize + 3],
																			CS2Info.intStack[CS2Info.intStackSize],
																			CS2Info.intStack[CS2Info.intStackSize + 1],
																			1);
																continue;
															}
															if (opcode == 5604) {
																CS2Info.stringStackSize--;
																if (Class143.anInt1875 == 10
																		&& Class3_Sub13_Sub31.anInt3375 == 0
																		&& Class3_Sub13_Sub25.loginStage == 0
																		&& Canvas_Sub1.anInt23 == 0
																		&& Class43.anInt692 == 0)
																	Class40.method1041(
																			CS2Info.field1118[CS2Info.stringStackSize]
																					.toLong(-108),
																			-28236);
																continue;
															}
															if (opcode == 5605) {
																CS2Info.intStackSize -= 4;
																CS2Info.stringStackSize -= 2;
																if (Class143.anInt1875 == 10
																		&& 0 == Class3_Sub13_Sub31.anInt3375
																		&& Class3_Sub13_Sub25.loginStage == 0
																		&& Canvas_Sub1.anInt23 == 0
																		&& Class43.anInt692 == 0)
																	Class3_Sub28_Sub6.a(
																			CS2Info.intStack[CS2Info.intStackSize],
																			10603,
																			CS2Info.intStack[CS2Info.intStackSize - -3],
																			CS2Info.intStack[1 + CS2Info.intStackSize],
																			CS2Info.field1118[1
																					+ CS2Info.stringStackSize],
																			CS2Info.field1118[CS2Info.stringStackSize]
																					.toLong(-125),
																			CS2Info.intStack[2 + CS2Info.intStackSize]);
																continue;
															}
															if (opcode == 5606) {
																if (Canvas_Sub1.anInt23 == 0)
																	class325.anInt1711 = -2;
																continue;
															}
															if (opcode == 5607) {
																CS2Info.intStack[CS2Info.intStackSize++] = Class158.anInt2005;
																continue;
															}
															if (opcode == 5608) {
																CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub34.anInt3413;
																continue;
															}
															if (5609 == opcode) {
																CS2Info.intStack[CS2Info.intStackSize++] = class325.anInt1711;
																continue;
															}
															if (opcode == 5610) {
																for (int l37 = 0; l37 < 5; l37++)
																	CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub13_Sub33.aClass94Array3391.length <= l37
																			? Class3_Sub9.aClass94_2331
																			: Class3_Sub13_Sub33.aClass94Array3391[l37]
																					.method1545((byte) -50);

																Class3_Sub13_Sub33.aClass94Array3391 = null;
																continue;
															}
															if (opcode != 5611)
																break;
															CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub26.anInt2561;
															continue;
														}
														if (4500 != opcode)
															break;
														CS2Info.intStackSize -= 2;
														int i38 = CS2Info.intStack[CS2Info.intStackSize];
														int i61 = CS2Info.intStack[CS2Info.intStackSize - -1];
														Class3_Sub28_Sub9 class3_sub28_sub9_1 = Class61.method1210(64,
																i61);
														if (!class3_sub28_sub9_1.method585(0))
															CS2Info.intStack[CS2Info.intStackSize++] = Class72
																	.method1292((byte) 94, i38).method600(i61,
																			class3_sub28_sub9_1.anInt3614, (byte) -29);
														else
															CS2Info.field1118[CS2Info.stringStackSize++] = Class72
																	.method1292((byte) 31, i38)
																	.method604(class3_sub28_sub9_1.aClass94_3619,
																			(byte) -44, i61);
														continue;
													}
													if (opcode != 4400)
														break;
													CS2Info.intStackSize -= 2;
													int j61 = CS2Info.intStack[CS2Info.intStackSize - -1];
													int j38 = CS2Info.intStack[CS2Info.intStackSize];
													Class3_Sub28_Sub9 class3_sub28_sub9_2 = Class61.method1210(64, j61);
													if (!class3_sub28_sub9_2.method585(0))
														CS2Info.intStack[CS2Info.intStackSize++] = Class162
																.get(4, j38).method1691(
																		class3_sub28_sub9_2.anInt3614, j61, (byte) 105);
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class162
																.get(4, j38).method1698(
																		class3_sub28_sub9_2.aClass94_3619, -23085, j61);
													continue;
												}
												if (opcode == 4200) {
													int k38 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.field1118[CS2Info.stringStackSize++] = ItemDefinition
															.getItemDefinition(k38, (byte) 72).name;
													continue;
												}
												if (opcode == 4201) {
													CS2Info.intStackSize -= 2;
													int l38 = CS2Info.intStack[CS2Info.intStackSize];
													int k61 = CS2Info.intStack[CS2Info.intStackSize - -1];
													ItemDefinition class48_2 = ItemDefinition.getItemDefinition(l38,
															(byte) 77);
													if (k61 < 1 || k61 > 5 || class48_2.groundOptions[-1 + k61] == null)
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = class48_2.groundOptions[k61
																- 1];
													continue;
												}
												if (opcode == 4202) {
													CS2Info.intStackSize -= 2;
													int i39 = CS2Info.intStack[CS2Info.intStackSize];
													int l61 = CS2Info.intStack[CS2Info.intStackSize + 1];
													ItemDefinition class48_3 = ItemDefinition.getItemDefinition(i39,
															(byte) 70);
													if (l61 >= 1 && l61 <= 5
															&& null != class48_3.inventoryOptions[l61 + -1])
														CS2Info.field1118[CS2Info.stringStackSize++] = class48_3.inventoryOptions[-1
																+ l61];
													else
														CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
													continue;
												}
												if (opcode == 4203) {
													int j39 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = ItemDefinition
															.getItemDefinition(j39, (byte) 85).cost;
													continue;
												}
												if (opcode == 4204) {
													int k39 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = ItemDefinition
															.getItemDefinition(k39, (byte) 99).stackable == 1 ? 1 : 0;
													continue;
												}
												if (4205 == opcode) {
													int l39 = CS2Info.intStack[--CS2Info.intStackSize];
													ItemDefinition class48 = ItemDefinition.getItemDefinition(l39,
															(byte) 96);
													if (-1 == class48.anInt791 && class48.anInt789 >= 0)
														CS2Info.intStack[CS2Info.intStackSize++] = class48.anInt789;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = l39;
													continue;
												}
												if (opcode == 4206) {
													int i40 = CS2Info.intStack[--CS2Info.intStackSize];
													ItemDefinition class48_1 = ItemDefinition.getItemDefinition(i40,
															(byte) 126);
													if (0 <= class48_1.anInt791 && class48_1.anInt789 >= 0)
														CS2Info.intStack[CS2Info.intStackSize++] = class48_1.anInt789;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = i40;
													continue;
												}
												if (opcode == 4207) {
													int j40 = CS2Info.intStack[--CS2Info.intStackSize];
													CS2Info.intStack[CS2Info.intStackSize++] = ItemDefinition
															.getItemDefinition(j40, (byte) 121).members ? 1 : 0;
													continue;
												}
												if (opcode == 4208) {
													CS2Info.intStackSize -= 2;
													int k40 = CS2Info.intStack[CS2Info.intStackSize];
													int i62 = CS2Info.intStack[CS2Info.intStackSize - -1];
													Class3_Sub28_Sub9 class3_sub28_sub9_3 = Class61.method1210(64, i62);
													if (class3_sub28_sub9_3.method585(0))
														CS2Info.field1118[CS2Info.stringStackSize++] = ItemDefinition
																.getItemDefinition(k40, (byte) 126).method1105(107,
																		class3_sub28_sub9_3.aClass94_3619, i62);
													else
														CS2Info.intStack[CS2Info.intStackSize++] = ItemDefinition
																.getItemDefinition(k40, (byte) 79)
																.method1115(class3_sub28_sub9_3.anInt3614, -119, i62);
													continue;
												}
												if (4210 == opcode) {
													RSString class94_28 = CS2Info.field1118[--CS2Info.stringStackSize];
													int j62 = CS2Info.intStack[--CS2Info.intStackSize];
													Varbit.method1480(j62 == 1, class94_28, 102);
													CS2Info.intStack[CS2Info.intStackSize++] = Class62.anInt952;
													continue;
												}
												if (opcode == 4211) {
													if (null == Class99.aShortArray1398
															|| Class140_Sub4.anInt2756 >= Class62.anInt952)
														CS2Info.intStack[CS2Info.intStackSize++] = -1;
													else
														CS2Info.intStack[CS2Info.intStackSize++] = Script.method633(
																Class99.aShortArray1398[Class140_Sub4.anInt2756++],
																65535);
													continue;
												}
												if (4212 != opcode)
													break;
												Class140_Sub4.anInt2756 = 0;
												continue;
											}
											if (4100 == opcode) {
												RSString class94_29 = CS2Info.field1118[--CS2Info.stringStackSize];
												int k62 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = RenderAnimationDefinition
														.method903(
																new RSString[] { class94_29,
																		Class72.method1298((byte) 9, k62) },
																(byte) -94);
												continue;
											}
											if (opcode == 4101) {
												CS2Info.stringStackSize -= 2;
												RSString class94_57 = CS2Info.field1118[CS2Info.stringStackSize + 1];
												RSString class94_30 = CS2Info.field1118[CS2Info.stringStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = RenderAnimationDefinition
														.method903(new RSString[] { class94_30, class94_57 },
																(byte) -106);
												continue;
											}
											if (4102 == opcode) {
												RSString class94_31 = CS2Info.field1118[--CS2Info.stringStackSize];
												int l62 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = RenderAnimationDefinition
														.method903(
																new RSString[] { class94_31,
																		Class61.method1218(true, 127, l62) },
																(byte) -119);
												continue;
											}
											if (opcode == 4103) {
												RSString class94_32 = CS2Info.field1118[--CS2Info.stringStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = class94_32
														.method1534(-98);
												continue;
											}
											if (4104 == opcode) {
												int l40 = CS2Info.intStack[--CS2Info.intStackSize];
												long l63 = 0xec44e2dc00L + (long) l40 * 0x5265c00L;
												Class3_Sub28_Sub9.aCalendar3616.setTime(new Date(l63));
												int k78 = Class3_Sub28_Sub9.aCalendar3616.get(5);
												int k80 = Class3_Sub28_Sub9.aCalendar3616.get(2);
												int i82 = Class3_Sub28_Sub9.aCalendar3616.get(1);
												CS2Info.field1118[CS2Info.stringStackSize++] = RenderAnimationDefinition
														.method903(
																new RSString[] { Class72.method1298((byte) 9, k78),
																		Class93.aClass94_1326,
																		RenderAnimationDefinition.aClass94Array358[k80],
																		Class93.aClass94_1326,
																		Class72.method1298((byte) 9, i82) },
																(byte) -122);
												continue;
											}
											if (4105 == opcode) {
												CS2Info.stringStackSize -= 2;
												RSString class94_58 = CS2Info.field1118[CS2Info.stringStackSize + 1];
												RSString class94_33 = CS2Info.field1118[CS2Info.stringStackSize];
												if (Client.field3717.field646 == null
														|| !Client.field3717.field646.aBoolean864)
													CS2Info.field1118[CS2Info.stringStackSize++] = class94_33;
												else
													CS2Info.field1118[CS2Info.stringStackSize++] = class94_58;
												continue;
											}
											if (opcode == 4106) {
												int i41 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = Class72
														.method1298((byte) 9, i41);
												continue;
											}
											if (opcode == 4107) {
												CS2Info.stringStackSize -= 2;
												CS2Info.intStack[CS2Info.intStackSize++] = CS2Info.field1118[CS2Info.stringStackSize]
														.method1546((byte) -63,
																CS2Info.field1118[CS2Info.stringStackSize - -1]);
												continue;
											}
											if (4108 == opcode) {
												RSString class94_34 = CS2Info.field1118[--CS2Info.stringStackSize];
												CS2Info.intStackSize -= 2;
												int l73 = CS2Info.intStack[CS2Info.intStackSize - -1];
												int i63 = CS2Info.intStack[CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class86
														.method1430(-28922, l73).method684(class94_34, i63);
												continue;
											}
											if (opcode == 4109) {
												CS2Info.intStackSize -= 2;
												RSString class94_35 = CS2Info.field1118[--CS2Info.stringStackSize];
												int i74 = CS2Info.intStack[1 + CS2Info.intStackSize];
												int j63 = CS2Info.intStack[CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class86
														.method1430(-28922, i74).method680(class94_35, j63);
												continue;
											}
											if (opcode == 4110) {
												CS2Info.stringStackSize -= 2;
												RSString class94_36 = CS2Info.field1118[CS2Info.stringStackSize];
												RSString class94_59 = CS2Info.field1118[CS2Info.stringStackSize - -1];
												if (1 == CS2Info.intStack[--CS2Info.intStackSize])
													CS2Info.field1118[CS2Info.stringStackSize++] = class94_36;
												else
													CS2Info.field1118[CS2Info.stringStackSize++] = class94_59;
												continue;
											}
											if (4111 == opcode) {
												RSString class94_37 = CS2Info.field1118[--CS2Info.stringStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub28_Sub17
														.method686(class94_37);
												continue;
											}
											if (4112 == opcode) {
												RSString class94_38 = CS2Info.field1118[--CS2Info.stringStackSize];
												int k63 = CS2Info.intStack[--CS2Info.intStackSize];
												if (k63 == -1)
													throw new RuntimeException("null char");
												CS2Info.field1118[CS2Info.stringStackSize++] = class94_38
														.method1548(false, k63);
												continue;
											}
											if (opcode == 4113) {
												int j41 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class164_Sub2
														.method2248(-157, j41) ? 1 : 0;
												continue;
											}
											if (opcode == 4114) {
												int k41 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class44.method1066(k41, -32)
														? 1
														: 0;
												continue;
											}
											if (opcode == 4115) {
												int l41 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub24_Sub4
														.method487(l41, (byte) -85) ? 1 : 0;
												continue;
											}
											if (4116 == opcode) {
												int i42 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub28_Sub3
														.method544(-49, i42) ? 1 : 0;
												continue;
											}
											if (opcode == 4117) {
												RSString class94_39 = CS2Info.field1118[--CS2Info.stringStackSize];
												if (class94_39 != null)
													CS2Info.intStack[CS2Info.intStackSize++] = class94_39.length(-96);
												else
													CS2Info.intStack[CS2Info.intStackSize++] = 0;
												continue;
											}
											if (opcode == 4118) {
												CS2Info.intStackSize -= 2;
												RSString class94_40 = CS2Info.field1118[--CS2Info.stringStackSize];
												int i64 = CS2Info.intStack[CS2Info.intStackSize];
												int j74 = CS2Info.intStack[1 + CS2Info.intStackSize];
												CS2Info.field1118[CS2Info.stringStackSize++] = class94_40
														.substring(j74, 0, i64);
												continue;
											}
											if (opcode == 4119) {
												RSString class94_41 = CS2Info.field1118[--CS2Info.stringStackSize];
												RSString class94_60 = Class47.method1090((byte) -87,
														class94_41.length(-44));
												boolean flag9 = false;
												for (int l78 = 0; class94_41.length(-113) > l78; l78++) {
													int l80 = class94_41.charAt(l78, (byte) -40);
													if (l80 == 60) {
														flag9 = true;
														continue;
													}
													if (l80 == 62) {
														flag9 = false;
													} else {
														if (!flag9)
															class94_60.method1572(l80, (byte) 125);
													}
												}

												class94_60.method1576((byte) 90);
												CS2Info.field1118[CS2Info.stringStackSize++] = class94_60;
												continue;
											}
											if (opcode == 4120) {
												CS2Info.intStackSize -= 2;
												RSString class94_42 = CS2Info.field1118[--CS2Info.stringStackSize];
												int j64 = CS2Info.intStack[CS2Info.intStackSize];
												int k74 = CS2Info.intStack[1 + CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = class94_42.method1555(j64,
														k74, 1536);
												continue;
											}
											if (opcode == 4121) {
												CS2Info.stringStackSize -= 2;
												RSString class94_43 = CS2Info.field1118[CS2Info.stringStackSize];
												RSString class94_61 = CS2Info.field1118[1 + CS2Info.stringStackSize];
												int l74 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = class94_43
														.method1566(class94_61, l74, -1);
												continue;
											}
											if (opcode == 4122) {
												int j42 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = Class3_Sub13_Sub34
														.method332(2, j42);
												continue;
											}
											if (opcode == 4123) {
												int k42 = CS2Info.intStack[--CS2Info.intStackSize];
												CS2Info.intStack[CS2Info.intStackSize++] = RuntimeException_Sub1
														.method2287(k42, (byte) 59);
												continue;
											}
											if (opcode != 4124)
												break;
											boolean flag1 = CS2Info.intStack[--CS2Info.intStackSize] != 0;
											int k64 = CS2Info.intStack[--CS2Info.intStackSize];
											CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub23
													.method407(Class3_Sub20.language, flag1, 0, k64, 2);
											continue;
										}
										Widget class11_8 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
										if (opcode == 2800) {
											CS2Info.intStack[CS2Info.intStackSize++] = Client.method44(class11_8)
													.method101(-94);
											continue;
										}
										if (opcode == 2801) {
											int l64 = CS2Info.intStack[--CS2Info.intStackSize];
											l64--;
											if (class11_8.actions != null && class11_8.actions.length > l64
													&& null != class11_8.actions[l64])
												CS2Info.field1118[CS2Info.stringStackSize++] = class11_8.actions[l64];
											else
												CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
											continue;
										}
										if (opcode != 2802)
											break;
										if (class11_8.name != null)
											CS2Info.field1118[CS2Info.stringStackSize++] = class11_8.name;
										else
											CS2Info.field1118[CS2Info.stringStackSize++] = Class3_Sub9.aClass94_2331;
										continue;
									}
									if (opcode == 2700) {
										Widget class11_9 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
										CS2Info.intStack[CS2Info.intStackSize++] = class11_9.anInt192;
										continue;
									}
									if (opcode == 2701) {
										Widget class11_10 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
										if (-1 != class11_10.anInt192)
											CS2Info.intStack[CS2Info.intStackSize++] = class11_10.anInt271;
										else
											CS2Info.intStack[CS2Info.intStackSize++] = 0;
										continue;
									}
									if (opcode == 2702) {
										int l42 = CS2Info.intStack[--CS2Info.intStackSize];
										Class3_Sub31 class3_sub31 = (Class3_Sub31) Class3_Sub13_Sub17.aClass130_3208
												.method1780(l42, 0);
										if (class3_sub31 == null)
											CS2Info.intStack[CS2Info.intStackSize++] = 0;
										else
											CS2Info.intStack[CS2Info.intStackSize++] = 1;
										continue;
									}
									if (opcode == 2703) {
										Widget class11_11 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
										if (null == class11_11.children) {
											CS2Info.intStack[CS2Info.intStackSize++] = 0;
										} else {
											int i65 = class11_11.children.length;
											int i75 = 0;
											do {
												if (class11_11.children.length <= i75)
													break;
												if (null == class11_11.children[i75]) {
													i65 = i75;
													break;
												}
												i75++;
											} while (true);
											CS2Info.intStack[CS2Info.intStackSize++] = i65;
										}
										continue;
									}
									if (opcode != 2704 && 2705 != opcode)
										break;
									CS2Info.intStackSize -= 2;
									int i43 = CS2Info.intStack[CS2Info.intStackSize];
									int j65 = CS2Info.intStack[CS2Info.intStackSize + 1];
									Class3_Sub31 class3_sub31_1 = (Class3_Sub31) Class3_Sub13_Sub17.aClass130_3208
											.method1780(i43, 0);
									if (class3_sub31_1 == null || class3_sub31_1.anInt2602 != j65)
										CS2Info.intStack[CS2Info.intStackSize++] = 0;
									else
										CS2Info.intStack[CS2Info.intStackSize++] = 1;
									continue;
								}
								Widget class11_12 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
								if (2600 == opcode) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.field2646;
									continue;
								}
								if (opcode == 2601) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.field2589;
									continue;
								}
								if (opcode == 2602) {
									CS2Info.field1118[CS2Info.stringStackSize++] = class11_12.text;
									continue;
								}
								if (opcode == 2603) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.scrollWidth;
									continue;
								}
								if (opcode == 2604) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.scrollHeight;
									continue;
								}
								if (opcode == 2605) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.modelZoom;
									continue;
								}
								if (opcode == 2606) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.rotationX;
									continue;
								}
								if (opcode == 2607) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.rotationY;
									continue;
								}
								if (2608 == opcode) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.rotationZ;
									continue;
								}
								if (opcode == 2609) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.opacity;
									continue;
								}
								if (opcode == 2610) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.field2914;
									continue;
								}
								if (opcode == 2611) {
									CS2Info.intStack[CS2Info.intStackSize++] = class11_12.anInt264;
									continue;
								}
								if (2612 != opcode)
									break;
								CS2Info.intStack[CS2Info.intStackSize++] = class11_12.spriteId;
								continue;
							}
							Widget class11_13 = flag ? Class20.field115 : CS2Info.field1130;
							if (opcode == 1700) {
								CS2Info.intStack[CS2Info.intStackSize++] = class11_13.anInt192;
								continue;
							}
							if (1701 == opcode) {
								if (class11_13.anInt192 == -1)
									CS2Info.intStack[CS2Info.intStackSize++] = 0;
								else
									CS2Info.intStack[CS2Info.intStackSize++] = class11_13.anInt271;
								continue;
							}
							if (opcode != 1702)
								break;
							CS2Info.intStack[CS2Info.intStackSize++] = class11_13.index;
						} else {
							Widget class11_14;
							if (opcode < 2000) {
								class11_14 = flag ? Class20.field115 : CS2Info.field1130;
							} else {
								opcode -= 1000;
								class11_14 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
							}
							int[] ai3 = null;
							RSString var5 = CS2Info.field1118[--CS2Info.stringStackSize];
							if (var5.length(-127) > 0
									&& var5.charAt(var5.length(-92) + -1, (byte) -96) == 89) {
								int i79 = CS2Info.intStack[--CS2Info.intStackSize];
								if (i79 > 0) {
									ai3 = new int[i79];
									while (i79-- > 0)
										ai3[i79] = CS2Info.intStack[--CS2Info.intStackSize];
								}
								var5 = var5.substring(var5.length(-79) - 1, 0, 0);
							}
							Object var9[] = new Object[var5.length(-48) - -1];
							for (int var8 = -1 + var9.length; 1 <= var8; var8--)
								if (115 != var5.charAt(var8 + -1, (byte) -43))
									var9[var8] = new Integer(CS2Info.intStack[--CS2Info.intStackSize]);
								else
									var9[var8] = CS2Info.field1118[--CS2Info.stringStackSize];

							int j81 = CS2Info.intStack[--CS2Info.intStackSize];
							if (j81 == -1)
								var9 = null;
							else
								var9[0] = new Integer(j81);
							class11_14.aBoolean195 = true;
							if (1400 == opcode)
								class11_14.onClickListener = var9;
							else if (opcode == 1401)
								class11_14.onHoldListener = var9;
							else if (opcode == 1402) {
								class11_14.onReleaseListener = var9;
							} else {
								if (opcode == 1403) {
									class11_14.onMouseOverListener = var9;
								} else {
									if (opcode == 1404) {
										class11_14.onMouseLeaveListener = var9;
									} else {
										if (1405 == opcode)
											class11_14.onDragListener = var9;
										else if (1406 == opcode)
											class11_14.onTargetLeaveListener = var9;
										else if (1407 == opcode) {
											class11_14.varTransmitTriggers = ai3;
											class11_14.onVarTransmitListener = var9;

										} else {
											if (opcode == 1408)
												class11_14.onTimerListener = var9;
											else if (opcode == 1409) {
												class11_14.onOpListener = var9;
											} else {
												if (1410 == opcode) {
													class11_14.onDragCompleteListener = var9;
												} else {
													if (opcode == 1411) {
														class11_14.onClickRepeatListener = var9;
													} else {
														if (opcode == 1412)
															class11_14.onMouseRepeatListener = var9;
														else if (opcode == 1414) {
															class11_14.invTransmitTriggers = ai3;
															class11_14.onInvTransmitListener = var9;
														} else if (1415 == opcode) {
															class11_14.statTransmitTriggers = ai3;
															class11_14.onStatTransmitListener = var9;
														} else if (1416 == opcode)
															class11_14.onTargetEnterListener = var9;
														else if (1417 == opcode) {
															class11_14.onScrollWheelListener = var9;
														} else {
															if (opcode == 1418)
																class11_14.anObjectArray256 = var9;
															else if (opcode == 1419)
																class11_14.anObjectArray220 = var9;
															else if (opcode == 1420)
																class11_14.anObjectArray156 = var9;
															else if (opcode == 1421) {
																class11_14.anObjectArray313 = var9;
															} else {
																if (1422 == opcode)
																	class11_14.anObjectArray315 = var9;
																else if (1423 == opcode) {
																	class11_14.anObjectArray206 = var9;
																} else {
																	if (opcode == 1424)
																		class11_14.anObjectArray176 = var9;
																	else if (opcode == 1425)
																		class11_14.anObjectArray268 = var9;
																	else if (opcode == 1426) {
																		class11_14.anObjectArray217 = var9;
																	} else {
																		if (1427 == opcode) {
																			class11_14.anObjectArray235 = var9;
																		} else {
																			if (opcode == 1428) {
																				class11_14.anObjectArray161 = var9;
																				class11_14.anIntArray211 = ai3;
																			} else if (opcode == 1429) {
																				class11_14.anIntArray185 = ai3;
																				class11_14.anObjectArray221 = var9;
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
						continue;
					}
					Widget class11_15;
					if (opcode < 2000) {
						class11_15 = flag ? Class20.field115 : CS2Info.field1130;
					} else {
						class11_15 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
						opcode -= 1000;
					}
					Class68.method909(class11_15);
					if (opcode == 1200 || 1205 == opcode) {
						CS2Info.intStackSize -= 2;
						int j75 = CS2Info.intStack[1 + CS2Info.intStackSize];
						int k65 = CS2Info.intStack[CS2Info.intStackSize];
						if (-1 == class11_15.index) {
							Class149.method2092(class11_15.hash, (byte) -47);
							Class3_Sub13_Sub19.method265((byte) -42, class11_15.hash);
							Class107.method1649(class11_15.hash, -101);
						}
						if (-1 == k65) {
							class11_15.modelId = -1;
							class11_15.modelType = 1;
							class11_15.anInt192 = -1;
						} else {
							class11_15.anInt192 = k65;
							class11_15.anInt271 = j75;
							ItemDefinition class48_4 = ItemDefinition.getItemDefinition(k65, (byte) 108);
							class11_15.rotationY = class48_4.anInt768;
							class11_15.field2914 = class48_4.xOffset2d;
							class11_15.rotationX = class48_4.xan2d;
							class11_15.anInt264 = class48_4.yOffset2d;
							class11_15.rotationZ = class48_4.yan2d;
							class11_15.modelZoom = class48_4.zoom2d;
							if (class11_15.modelHeightOverride > 0)
								class11_15.modelZoom = (class11_15.modelZoom * 32) / class11_15.modelHeightOverride;
							else if (class11_15.originalWidth > 0)
								class11_15.modelZoom = (class11_15.modelZoom * 32) / class11_15.originalWidth;
							if (1205 == opcode)
								class11_15.aBoolean227 = false;
							else
								class11_15.aBoolean227 = true;
						}
						continue;
					}
					if (opcode == 1201) {
						class11_15.modelType = 2;
						class11_15.modelId = CS2Info.intStack[--CS2Info.intStackSize];
						if (class11_15.index == -1)
							Class162.method2206(true, class11_15.hash);
						continue;
					}
					if (opcode == 1202) {
						class11_15.modelType = 3;
						class11_15.modelId = Client.field3717.field646.method4071(-24861);
						if (class11_15.index == -1)
							Class162.method2206(true, class11_15.hash);
						continue;
					}
					if (1203 == opcode) {
						class11_15.modelType = 6;
						class11_15.modelId = CS2Info.intStack[--CS2Info.intStackSize];
						if (class11_15.index == -1)
							Class162.method2206(true, class11_15.hash);
						continue;
					}
					if (opcode != 1204)
						break;
					class11_15.modelType = 5;
					class11_15.modelId = CS2Info.intStack[--CS2Info.intStackSize];
					if (class11_15.index == -1)
						Class162.method2206(true, class11_15.hash);
					continue;
				}
				Widget class11_16;
				if (opcode < 2000) {
					class11_16 = flag ? Class20.field115 : CS2Info.field1130;
				} else {
					opcode -= 1000;
					class11_16 = Widget.get_widget(CS2Info.intStack[--CS2Info.intStackSize]);
				}
				if (opcode == 1100) {
					CS2Info.intStackSize -= 2;
					class11_16.field2646 = CS2Info.intStack[CS2Info.intStackSize];
					if (class11_16.scrollWidth + -class11_16.field2582 < class11_16.field2646)
						class11_16.field2646 = class11_16.scrollWidth + -class11_16.field2582;
					if (class11_16.field2646 < 0)
						class11_16.field2646 = 0;
					class11_16.field2589 = CS2Info.intStack[CS2Info.intStackSize + 1];
					if (class11_16.field2589 > class11_16.scrollHeight + -class11_16.field2642)
						class11_16.field2589 = class11_16.scrollHeight + -class11_16.field2642;
					if (class11_16.field2589 < 0)
						class11_16.field2589 = 0;
					Class68.method909(class11_16);
					if (-1 == class11_16.index)
						class208.method1259(class11_16.hash, (byte) 109);
					continue;
				}
				if (1101 == opcode) {
					class11_16.textColor = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					if (class11_16.index == -1)
						Canvas_Sub2.method56(class11_16.hash, 99);
					continue;
				}
				if (opcode == 1102) {
					class11_16.filled = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					Class68.method909(class11_16);
					continue;
				}
				if (1103 == opcode) {
					class11_16.opacity = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1104) {
					class11_16.lineWidth = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1105) {
					class11_16.spriteId = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (1106 == opcode) {
					class11_16.textureId = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (1107 == opcode) {
					class11_16.spriteTiling = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1108) {
					class11_16.modelType = 1;
					class11_16.modelId = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					if (class11_16.index == -1)
						Class162.method2206(true, class11_16.hash);
					continue;
				}
				if (opcode == 1109) {
					CS2Info.intStackSize -= 6;
					class11_16.field2914 = CS2Info.intStack[CS2Info.intStackSize];
					class11_16.anInt264 = CS2Info.intStack[CS2Info.intStackSize + 1];
					class11_16.rotationX = CS2Info.intStack[2 + CS2Info.intStackSize];
					class11_16.rotationZ = CS2Info.intStack[CS2Info.intStackSize - -3];
					class11_16.rotationY = CS2Info.intStack[CS2Info.intStackSize - -4];
					class11_16.modelZoom = CS2Info.intStack[5 + CS2Info.intStackSize];
					Class68.method909(class11_16);
					if (class11_16.index == -1) {
						Class3_Sub13_Sub19.method265((byte) -42, class11_16.hash);
						Class107.method1649(class11_16.hash, -106);
					}
					continue;
				}
				if (opcode == 1110) {
					int l65 = CS2Info.intStack[--CS2Info.intStackSize];
					if (class11_16.animation != l65) {
						class11_16.animation = l65;
						class11_16.anInt283 = 0;
						class11_16.anInt267 = 0;
						class11_16.anInt260 = 1;
						Class68.method909(class11_16);
					}
					if (class11_16.index == -1)
						Class108.method1657(class11_16.hash, -903);
					continue;
				}
				if (opcode == 1111) {
					class11_16.orthogonal = 1 == CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (1112 == opcode) {
					RSString class94_63 = CS2Info.field1118[--CS2Info.stringStackSize];
					if (!class94_63.equals(class11_16.text)) {
						class11_16.text = class94_63;
						Class68.method909(class11_16);
					}
					if (class11_16.index == -1)
						Class93.method1516(class11_16.hash, 91);
					continue;
				}
				if (opcode == 1113) {
					class11_16.fontId = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1114) {
					CS2Info.intStackSize -= 3;
					class11_16.xTextAlignment = CS2Info.intStack[CS2Info.intStackSize];
					class11_16.yTextAlignment = CS2Info.intStack[1 + CS2Info.intStackSize];
					class11_16.lineHeight = CS2Info.intStack[2 + CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (1115 == opcode) {
					class11_16.textShadowed = 1 == CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1116) {
					class11_16.borderType = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1117) {
					class11_16.shadowColor = CS2Info.intStack[--CS2Info.intStackSize];
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1118) {
					class11_16.flippedVertically = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1119) {
					class11_16.flippedHorizontally = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					Class68.method909(class11_16);
					continue;
				}
				if (opcode == 1120) {
					CS2Info.intStackSize -= 2;
					class11_16.scrollWidth = CS2Info.intStack[CS2Info.intStackSize];
					class11_16.scrollHeight = CS2Info.intStack[1 + CS2Info.intStackSize];
					Class68.method909(class11_16);
					if (class11_16.type == 0)
						class63.method1161(class11_16, false, -116);
					continue;
				}
				if (opcode == 1121) {
					CS2Info.intStackSize -= 2;
					class11_16.aShort293 = (short) CS2Info.intStack[CS2Info.intStackSize];
					class11_16.aShort169 = (short) CS2Info.intStack[CS2Info.intStackSize + 1];
					Class68.method909(class11_16);
					continue;
				}
				if (1122 == opcode) {
					class11_16.field2578 = CS2Info.intStack[--CS2Info.intStackSize] == 1;
					Class68.method909(class11_16);
					continue;
				}
				if (opcode != 1123)
					break;
				class11_16.modelZoom = CS2Info.intStack[--CS2Info.intStackSize];
				Class68.method909(class11_16);
				if (class11_16.index == -1)
					Class3_Sub13_Sub19.method265((byte) -42, class11_16.hash);
				continue;
			}
			if (opcode == 403) {
				CS2Info.intStackSize -= 2;
				int i66 = CS2Info.intStack[CS2Info.intStackSize - -1];
				int j43 = CS2Info.intStack[CS2Info.intStackSize];
				int k75 = 0;
				do {
					if (Class3_Sub26.anIntArray2559.length <= k75)
						break;
					if (j43 == Class3_Sub26.anIntArray2559[k75]) {
						Client.field3717.field646.method1164(k75, i66, 0);
						continue label0;
					}
					k75++;
				} while (true);
				k75 = 0;
				do {
					if (Class3_Sub13_Sub19.anIntArray3228.length <= k75)
						continue label0;
					if (Class3_Sub13_Sub19.anIntArray3228[k75] == j43) {
						Client.field3717.field646.method1164(k75, i66, 0);
						continue label0;
					}
					k75++;
				} while (true);
			}
			if (404 == opcode) {
				CS2Info.intStackSize -= 2;
				int k43 = CS2Info.intStack[CS2Info.intStackSize];
				int j66 = CS2Info.intStack[1 + CS2Info.intStackSize];
				Client.field3717.field646.method1162(k43, false, j66);
				continue;
			}
			if (opcode != 410)
				break;
			boolean flag2 = 0 != CS2Info.intStack[--CS2Info.intStackSize];
			Client.field3717.field646.method1159(flag2, true);
		} while (true);
		throw new IllegalStateException();
	}

}
