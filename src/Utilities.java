

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class Utilities {

	public static byte[] read(File file) throws IOException {
		byte[] buffer = new byte[(int) file.length()];
		InputStream ios = null;
		try {
			ios = new FileInputStream(file);
			if (ios.read(buffer) == -1) {
				throw new IOException("EOF reached while trying to read the whole file");
			}
		} finally {
			try {
				if (ios != null)
					ios.close();
			} catch (IOException e) {
			}
		}
		return buffer;
	}

	static final void fill(int[] var0, int var1, int var2, int var3) {
		for (var2 = var1 + var2 - 7; var1 < var2; var0[var1++] = var3) {
			var0[var1++] = var3;
			var0[var1++] = var3;
			var0[var1++] = var3;
			var0[var1++] = var3;
			var0[var1++] = var3;
			var0[var1++] = var3;
			var0[var1++] = var3;
		}
		for (var2 += 7; var1 < var2; var0[var1++] = var3) {
			;
		}
	}

	static final void arrayCopy(long[] var0, int var1, long[] var2, int var3, int var4) {
		if (var0 == var2) {
			if (var1 == var3) {
				return;
			}
			if (var3 > var1 && var3 < var1 + var4) {
				--var4;
				var1 += var4;
				var3 += var4;
				var4 = var1 - var4;
				for (var4 += 3; var1 >= var4; var2[var3--] = var0[var1--]) {
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
				}
				for (var4 -= 3; var1 >= var4; var2[var3--] = var0[var1--]) {
					;
				}
				return;
			}
		}
		var4 += var1;
		for (var4 -= 3; var1 < var4; var2[var3++] = var0[var1++]) {
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
		}
		for (var4 += 3; var1 < var4; var2[var3++] = var0[var1++]) {
			;
		}
	}

	public static boolean contains(final int[] array, final int key) {
		for (final int i : array) {
			if (i == key) {
				return true;
			}
		}
		return false;
	}

	static final void arrayCopy(byte[] var0, int var1, byte[] var2, int var3, int var4) {
		if (var0 == var2) {
			if (var1 == var3) {
				return;
			}
			if (var3 > var1 && var3 < var1 + var4) {
				--var4;
				var1 += var4;
				var3 += var4;
				var4 = var1 - var4;
				for (var4 += 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
				}
				for (var4 -= 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					;
				}
				return;
			}
		}
		var4 += var1;
		for (var4 -= 7; var1 < var4; var2[var3++] = var0[var1++]) {
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
		}
		for (var4 += 7; var1 < var4; var2[var3++] = var0[var1++]) {
			;
		}
	}

	static final void arrayCopy(int[] var0, int var1, int[] var2, int var3, int var4) {
		if (var0 == var2) {
			if (var1 == var3) {
				return;
			}
			if (var3 > var1 && var3 < var1 + var4) {
				--var4;
				var1 += var4;
				var3 += var4;
				var4 = var1 - var4;
				for (var4 += 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
				}
				for (var4 -= 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					;
				}
				return;
			}
		}
		var4 += var1;
		for (var4 -= 7; var1 < var4; var2[var3++] = var0[var1++]) {
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
		}
		for (var4 += 7; var1 < var4; var2[var3++] = var0[var1++]) {
			;
		}
	}

	static final void arrayCopy(float[] var0, int var1, float[] var2, int var3, int var4) {
		if (var0 == var2) {
			if (var1 == var3) {
				return;
			}
			if (var3 > var1 && var3 < var1 + var4) {
				--var4;
				var1 += var4;
				var3 += var4;
				var4 = var1 - var4;
				for (var4 += 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
				}
				for (var4 -= 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					;
				}
				return;
			}
		}
		var4 += var1;
		for (var4 -= 7; var1 < var4; var2[var3++] = var0[var1++]) {
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
		}
		for (var4 += 7; var1 < var4; var2[var3++] = var0[var1++]) {
			;
		}
	}

	static final void arrayCopy(short[] var0, int var1, short[] var2, int var3, int var4) {
		if (var0 == var2) {
			if (var1 == var3) {
				return;
			}
			if (var3 > var1 && var3 < var1 + var4) {
				--var4;
				var1 += var4;
				var3 += var4;
				var4 = var1 - var4;
				for (var4 += 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
				}
				for (var4 -= 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					;
				}
				return;
			}
		}
		var4 += var1;
		for (var4 -= 7; var1 < var4; var2[var3++] = var0[var1++]) {
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
		}
		for (var4 += 7; var1 < var4; var2[var3++] = var0[var1++]) {
			;
		}
	}

	static final void arrayCopy(Object[] var0, int var1, Object[] var2, int var3, int var4) {
		if (var0 == var2) {
			if (var1 == var3) {
				return;
			}
			if (var3 > var1 && var3 < var1 + var4) {
				--var4;
				var1 += var4;
				var3 += var4;
				var4 = var1 - var4;
				for (var4 += 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
					var2[var3--] = var0[var1--];
				}
				for (var4 -= 7; var1 >= var4; var2[var3--] = var0[var1--]) {
					;
				}
				return;
			}
		}
		var4 += var1;
		for (var4 -= 7; var1 < var4; var2[var3++] = var0[var1++]) {
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
			var2[var3++] = var0[var1++];
		}
		for (var4 += 7; var1 < var4; var2[var3++] = var0[var1++]) {
			;
		}
	}

	static final void clear(int[] data, int off, int len) {
		len = off + len - 7;
		while (off < len) {
			data[off++] = 0;
			data[off++] = 0;
			data[off++] = 0;
			data[off++] = 0;
			data[off++] = 0;
			data[off++] = 0;
			data[off++] = 0;
		}
		len += 7;
		while (off < len) {
			data[off++] = 0;
		}
	}

	static final void quicksort(int[] integers, Object[] var2) {
		quicksort(var2, integers.length - 1, integers, 0);
	}

	static final void quicksort(Object[] values, int endIdx, int[] order, int startIdx) {
		try {
			if (startIdx < endIdx) {
				int var5 = (startIdx + endIdx) / 2;
				int var7 = order[var5];
				int var6 = startIdx;
				order[var5] = order[endIdx];
				order[endIdx] = var7;
				Object var8 = values[var5];
				values[var5] = values[endIdx];
				values[endIdx] = var8;
				for (int var9 = startIdx; ~var9 > ~endIdx; ++var9) {
					if (~((var9 & 1) + var7) < ~order[var9]) {
						int var10 = order[var9];
						order[var9] = order[var6];
						order[var6] = var10;
						Object var11 = values[var9];
						values[var9] = values[var6];
						values[var6++] = var11;
					}
				}
				order[endIdx] = order[var6];
				order[var6] = var7;
				values[endIdx] = values[var6];
				values[var6] = var8;
				quicksort(values, var6 - 1, order, startIdx);
				quicksort(values, endIdx, order, 1 + var6);
			}
		} catch (RuntimeException var12) {
			throw Class44.method1067(var12, "ec.G(" + (values != null ? "{...}" : "null") + ',' + endIdx + ','
					+ (order != null ? "{...}" : "null") + ',' + startIdx + ')');
		}
	}

	static final void quicksort(short[] shorts, int var1, RSString[] strings, int var4) {
		try {
			if (~var1 < ~var4) {
				int var6 = var4;
				int var5 = (var4 - -var1) / 2;
				RSString var7 = strings[var5];
				strings[var5] = strings[var1];
				strings[var1] = var7;
				short var8 = shorts[var5];
				shorts[var5] = shorts[var1];
				shorts[var1] = var8;
				for (int var9 = var4; ~var1 < ~var9; ++var9) {
					if (var7 == null || null != strings[var9] && strings[var9].method1559(var7, var9) < (var9 & 1)) {
						RSString var10 = strings[var9];
						strings[var9] = strings[var6];
						strings[var6] = var10;
						short var11 = shorts[var9];
						shorts[var9] = shorts[var6];
						shorts[var6++] = var11;
					}
				}
				strings[var1] = strings[var6];
				strings[var6] = var7;
				shorts[var1] = shorts[var6];
				shorts[var6] = var8;
				quicksort(shorts, -1 + var6, strings, var4);
				quicksort(shorts, var1, strings, var6 - -1);
			}
		} catch (RuntimeException var12) {
			throw Class44.method1067(var12, "ed.E(" + (shorts != null ? "{...}" : "null") + ',' + var1 + ','
					+ (strings != null ? "{...}" : "null") + ',' + var4 + ')');
		}
	}

	static final void quicksort(RSString[] strings, short[] shorts) {
		quicksort(shorts, strings.length - 1, strings, 0);
	}

	static final void quicksort(Object[] values, long[] order, int start, int end) {
		if (~start > ~end) {
			int var6 = start;
			int var5 = (start + end) / 2;
			long var7 = order[var5];
			order[var5] = order[end];
			order[end] = var7;
			Object var9 = values[var5];
			values[var5] = values[end];
			values[end] = var9;
			for (int var10 = start; end > var10; ++var10) {
				if (var7 + (long) (1 & var10) > order[var10]) {
					long var11 = order[var10];
					order[var10] = order[var6];
					order[var6] = var11;
					Object var13 = values[var10];
					values[var10] = values[var6];
					values[var6++] = var13;
				}
			}
			order[end] = order[var6];
			order[var6] = var7;
			values[end] = values[var6];
			values[var6] = var9;
			quicksort(values, order, start, -1 + var6);
			quicksort(values, order, var6 - -1, end);
		}
	}

	static final void quicksort(long[] values, Object[] order) {
		quicksort(order, values, 0, values.length - 1);
	}

	static final short[] arrayCopy(short[] var1) {
		if (null != var1) {
			short[] var2 = new short[var1.length];
			arrayCopy(var1, 0, var2, 0, var1.length);
			return var2;
		} else {
			return null;
		}
	}

	static final int[] arrayCopy(int[] var0) {
		if (null != var0) {
			int[] var2 = new int[var0.length];
			arrayCopy(var0, 0, var2, 0, var0.length);
			return var2;
		} else {
			return null;
		}
	}

	public static int getHash(long value) {
		return (int) (value >>> 32) & Integer.MAX_VALUE;
	}

	static final void quicksort(long[] var0, int var1, int var2, int[] var3) {
		if (~var1 > ~var2) {
			int var6 = var1;
			int var5 = (var2 + var1) / 2;
			long var7 = var0[var5];
			var0[var5] = var0[var2];
			var0[var2] = var7;
			int var9 = var3[var5];
			var3[var5] = var3[var2];
			var3[var2] = var9;
			for (int var10 = var1; var2 > var10; ++var10) {
				if (var0[var10] < var7 - -((long) (1 & var10))) {
					long var11 = var0[var10];
					var0[var10] = var0[var6];
					var0[var6] = var11;
					int var13 = var3[var10];
					var3[var10] = var3[var6];
					var3[var6++] = var13;
				}
			}
			var0[var2] = var0[var6];
			var0[var6] = var7;
			var3[var2] = var3[var6];
			var3[var6] = var9;
			quicksort(var0, var1, -1 + var6, var3);
			quicksort(var0, 1 + var6, var2, var3);
		}
	}

	static final void quicksort(long[] var1, int[] var2) {
		quicksort(var1, 0, var1.length - 1, var2);
	}
}
