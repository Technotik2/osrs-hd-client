
final class class208 implements Runnable {

	static Class93 aClass93_1013 = new Class93(100);
	static byte[][][] aByteArrayArrayArray1014;
	boolean aBoolean1015 = true;
	Object anObject1016 = new Object();
	static Widget aClass11_1017;
	int anInt1018 = 0;
	int[] anIntArray1019 = new int[500];
	int[] anIntArray1020 = new int[500];

	public static void method1257(int var0) {
		try {
			if (var0 == 25951) {
				aClass11_1017 = null;
				aByteArrayArrayArray1014 = (byte[][][]) null;
				aClass93_1013 = null;
			}
		} catch (RuntimeException var2) {
			throw Class44.method1067(var2, "jd.B(" + var0 + ')');
		}
	}

	public final void run() {
		try {
			for (; this.aBoolean1015; Class3_Sub13_Sub34.method331(50L, 64)) {
				Object var1 = this.anObject1016;
				synchronized (var1) {
					if (-501 < ~this.anInt1018) {
						this.anIntArray1020[this.anInt1018] = Class126.anInt1676;
						this.anIntArray1019[this.anInt1018] = class325.anInt1709;
						++this.anInt1018;
					}
				}
			}

		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "jd.run()");
		}
	}

	static final int method1258(byte var0) {
		try {
			Class136.anInt1780 = 0;
			if (var0 != -53) {
				method1258((byte) -35);
			}

			return Class3_Sub13_Sub17.method251(-1);
		} catch (RuntimeException var2) {
			throw Class44.method1067(var2, "jd.D(" + var0 + ')');
		}
	}

	static final void method1259(int var0, byte var1) {
		try {
			if (var1 > 12) {
				Class3_Sub28_Sub6 var2 = Class3_Sub24_Sub3.method466(4, 12, var0);
				var2.a(true);
			}
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "jd.A(" + var0 + ',' + var1 + ')');
		}
	}

	static final void method1260(int var0, int var1, Widget[] var2) {
		try {
			for (int var3 = 0; var3 < var2.length; ++var3) {
				Widget var4 = var2[var3];
				if (null != var4 && ~var4.parentId == ~var1 && (!var4.isIf3 || !Client.method51(var4))) {
					if (-1 == ~var4.type) {
						if (!var4.isIf3 && Client.method51(var4) && var4 != Class107.aClass11_1453) {
							continue;
						}

						method1260(var0 ^ 0, var4.hash, var2);
						if (var4.children != null) {
							method1260(23206, var4.hash, var4.children);
						}

						Class3_Sub31 var5 = (Class3_Sub31) Class3_Sub13_Sub17.aClass130_3208
								.method1780((long) var4.hash, 0);
						if (var5 != null) {
							Class52.method1160(-111, var5.anInt2602);
						}
					}

					if (var4.type == 6) {
						int var6;
						if (0 != ~var4.animation || ~var4.alternateAnimation != 0) {
							boolean var9 = Class3_Sub28_Sub12.method609(var4, var0 + -23173);
							if (var9) {
								var6 = var4.alternateAnimation;
							} else {
								var6 = var4.animation;
							}

							if (var6 != -1) {
								Class142 var7 = Client.method45(var6, (byte) -20);
								if (null != var7) {
									for (var4.anInt267 += Class106.anInt1446; ~var4.anInt267 < ~var7.anIntArray1869[var4.anInt283]; Class68
											.method909(var4)) {
										var4.anInt267 -= var7.anIntArray1869[var4.anInt283];
										++var4.anInt283;
										if (var7.anIntArray1851.length <= var4.anInt283) {
											var4.anInt283 -= var7.anInt1865;
											if (~var4.anInt283 > -1 || var7.anIntArray1851.length <= var4.anInt283) {
												var4.anInt283 = 0;
											}
										}

										var4.anInt260 = var4.anInt283 + 1;
										if (var7.anIntArray1851.length <= var4.anInt260) {
											var4.anInt260 -= var7.anInt1865;
											if (~var4.anInt260 > -1 || var7.anIntArray1851.length <= var4.anInt260) {
												var4.anInt260 = -1;
											}
										}
									}
								}
							}
						}

						if (0 != var4.anInt237 && !var4.isIf3) {
							int var10 = var4.anInt237 >> 16;
							var10 *= Class106.anInt1446;
							var6 = var4.anInt237 << 16 >> 16;
							var4.rotationX = 2047 & var10 + var4.rotationX;
							var6 *= Class106.anInt1446;
							var4.rotationZ = var4.rotationZ - -var6 & 2047;
							Class68.method909(var4);
						}
					}
				}
			}

			if (var0 != 23206) {
				method1257(107);
			}

		} catch (RuntimeException var8) {
			throw Class44.method1067(var8, "jd.E(" + var0 + ',' + var1 + ',' + (var2 != null ? "{...}" : "null") + ')');
		}
	}

	static final RSString method3900(RSString[] var2, int var0, int var1, int var3) {
		try {
			int var4 = 0;

			for (int var5 = 0; var1 > var5; ++var5) {
				if (null == var2[var0 - -var5]) {
					var2[var5 + var0] = Class3_Sub13_Sub27.aClass94_3339;
				}

				var4 += var2[var5 + var0].length;
			}

			byte[] var10 = new byte[var4];
			int var6 = 0;

			for (int var7 = 0; var1 > var7; ++var7) {
				RSString var8 = var2[var7 + var0];
				Class76.method1357(var8.buf, 0, var10, var6, var8.length);
				var6 += var8.length;
			}

			RSString var11 = new RSString();
			var11.length = var4;
			if (var3 != 2774) {
				method1262(83, 28);
			}

			var11.buf = var10;
			return var11;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	static final int method1262(int var0, int var1) {
		try {
			if (var0 < 20) {
				method1262(15, 87);
			}

			return var1 & 127;
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "jd.F(" + var0 + ',' + var1 + ')');
		}
	}

}
