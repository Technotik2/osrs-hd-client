final class Script extends Node {

	int intArgumentCount;
	static class325 aClass130_3679 = new class325(16);
	int localIntCount;
	static int anInt3681;
	int stringArgumentCount;
	int[] instructions;
	static int anInt3684 = 10;
	class325[] field1304;
	RSString name;
	int localStringCount;
	RSString[] stringValues;
	static int anInt3689 = 0;
	int[] intValues;
	private int scriptId;

	private static RSString aClass94_3692 = Class3_Sub4.createRSString("Members object", (byte) -122);
	static int[] anIntArray3693 = new int[1000];
	static Class3_Sub19[] aClass3_Sub19Array3694;
	static int anInt3695;
	static RSString aClass94_3691 = aClass94_3692;

	static final Class100 method629(boolean var0, int var1) {
		try {
			Class100 var2 = (Class100) Class44.aClass93_725.get((long) var1, (byte) 121);
			if (var2 == null) {
				if (!var0) {
					return (Class100) null;
				} else {
					byte[] var3 = Class3_Sub23.aClass153_2536.getFile(1, var1);
					var2 = new Class100();
					if (null != var3) {
						var2.method1601(var1, new Buffer(var3), 255);
					}

					Class44.aClass93_725.put((byte) -104, var2, (long) var1);
					return var2;
				}
			} else {
				return var2;
			}
		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "qc.B(" + var0 + ',' + var1 + ')');
		}
	}

	static final int method630(byte var0, int var1) {
		try {
			if (var0 != -34) {
				aClass94_3692 = (RSString) null;
			}

			return 127 & var1 >> 11;
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "qc.A(" + var0 + ',' + var1 + ')');
		}
	}

	static final void method631(boolean var0, JS5Index var1) {
		try {
			if (!var0) {
				Class3_Sub28_Sub5.aClass153_3580 = var1;
				ScriptState.anInt869 = Class3_Sub28_Sub5.aClass153_3580.getChildCount(4, (byte) 112);
			}
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "qc.D(" + var0 + ',' + (var1 != null ? "{...}" : "null") + ')');
		}
	}

	public static void method632(int var0) {
		try {
			aClass94_3692 = null;
			aClass94_3691 = null;
			aClass3_Sub19Array3694 = null;
			aClass130_3679 = null;
			anIntArray3693 = null;
			if (var0 != -30497) {
				aClass3_Sub19Array3694 = (Class3_Sub19[]) null;
			}

		} catch (RuntimeException var2) {
			throw Class44.method1067(var2, "qc.E(" + var0 + ')');
		}
	}

	static int method633(int var0, int var1) {
		try {
			return var0 & var1;
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "qc.C(" + var0 + ',' + var1 + ')');
		}
	}

	static final Script get_script(int script_id, boolean osrs) {
		try {
			Script script = null;
			if (script != null) {
				return script;
			} else {
				byte[] cs2_data;
				if (osrs) {
					cs2_data = JS5Group.index32.getFile(script_id, 0);
					if (cs2_data == null) {
						System.err.println("cs2 data is null");
					} else {
						System.out.println("Fetched " + script_id + ".");
					}
				} else {
					cs2_data = JS5Group.index12.getFile(script_id, 0);
				}
				if (cs2_data != null) {
					script = new Script();
					script.scriptId = script_id;
					if (osrs) {
						Buffer buf = new Buffer(cs2_data);

						buf.pos = buf.buffer.length - 2;
						int switchLength = buf.readUnsignedShort();

						int endIdx = buf.buffer.length - 2 - switchLength - 12;
						buf.pos = endIdx;
						int opcodeCount = buf.readInt();
						script.localIntCount = buf.readUnsignedShort();
						script.localStringCount = buf.readUnsignedShort();
						script.intArgumentCount = buf.readUnsignedShort();
						script.stringArgumentCount = buf.readUnsignedShort();

						int numSwitches = buf.readUnsignedByte();
						if (numSwitches > 0) {
							for (int i = 0; i < numSwitches; i++) {
//				                switches[i] = new HashMap<>();

								int count = buf.readUnsignedShort();
								while (count-- > 0) {
									int key = buf.readInt(); // int from stack is compared to this
									int pcOffset = buf.readInt(); // pc jumps by this

//				                    switches[i].put(key, pcOffset);
								}
							}
						}

						buf.pos = 0;
						script.name = buf.getFastJagexString();
						script.instructions = new int[opcodeCount];
						script.stringValues = new RSString[opcodeCount];
						script.intValues = new int[opcodeCount];

						int opcodeId;
						for (int i = 0; buf.pos < endIdx; script.instructions[i++] = opcodeId) {
							opcodeId = buf.readUnsignedShort();
							if (opcodeId != 3) {
								if (opcodeId < 100 && opcodeId != 21 && opcodeId != 38 && opcodeId != 39) {
									script.intValues[i] = buf.readInt();
								} else {
									script.intValues[i] = buf.readUnsignedByte();
								}
							} else {
								script.stringValues[i] = buf.readJagexString();
							}
						}
						Class56.aClass47_885.method1097(script, (long) script_id, (byte) -87);
						return script;
					} else {
						Buffer buffer = new Buffer(cs2_data);
						buffer.pos = -2 + buffer.buffer.length;
						int var5 = buffer.method5549((byte) 101);
						int var6 = buffer.buffer.length - 2 - var5 - 12;
						buffer.pos = var6;
						int var7 = buffer.readInt();
						script.localIntCount = buffer.readUnsignedShort();
						script.localStringCount = buffer.readUnsignedShort();
						script.intArgumentCount = buffer.readUnsignedShort();
						script.stringArgumentCount = buffer.readUnsignedShort();
						int var9, var10;
						int numSwitches = buffer.readUnsignedByte();
						if (numSwitches > 0) {
							script.field1304 = new class325[numSwitches];

							for (var9 = 0; var9 < numSwitches; ++var9) {
								var10 = buffer.readUnsignedShort();
								class325 var11 = new class325(class216.method4012(var10, (byte) 119));
								script.field1304[var9] = var11;

								while (-1 > ~(var10--)) {
									int var12 = buffer.readInt();
									int var13 = buffer.readInt();
									var11.method5963(new class188(var13), (long) var12);
								}
							}
						}

						buffer.pos = 0;
						script.name = buffer.method750((byte) 78);
						script.instructions = new int[var7];
						script.stringValues = new RSString[var7];
						var9 = 0;

						for (script.intValues = new int[var7]; ~var6 < ~buffer.pos; script.instructions[var9++] = var10) {
							var10 = buffer.readUnsignedShort();
							if (var10 != 3) {
								if (var10 < 100 && 21 != var10 && -39 != ~var10 && 39 != var10) {
									script.intValues[var9] = buffer.readInt();
								} else {
									script.intValues[var9] = buffer.readUnsignedByte();
								}
							} else {
								script.stringValues[var9] = buffer.readString();
							}
						}

						Class56.aClass47_885.method1097(script, (long) script_id, (byte) -87);
						return script;
					}
				} else {
					return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
