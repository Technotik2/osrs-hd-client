
public class class222 {

   public static int field2530;
   public static char field2531;
   public int field2528;
   public int field2529;
   public int field2532;


   public class222(class222 var1) {
      this.field2528 = var1.field2528;
      this.field2532 = var1.field2532;
      this.field2529 = var1.field2529;
   }

   public class222(int var1, int var2, int var3) {
      this.field2528 = var1;
      this.field2532 = var2;
      this.field2529 = var3;
   }

   public class222(int var1) {
      if(var1 == -1) {
         this.field2528 = -1;
      } else {
         this.field2528 = var1 >> 28 & 3;
         this.field2532 = var1 >> 14 & 16383;
         this.field2529 = var1 & 16383;
      }

   }

   String method4047(String var1, int var2) {
      return this.field2528 + var1 + (this.field2532 >> 6) + var1 + (this.field2529 >> 6) + var1 + (this.field2532 & 63) + var1 + (this.field2529 & 63);
   }

   public int method4043(int var1) {
      return this.field2528 << 28 | this.field2532 << 14 | this.field2529;
   }

   boolean method4042(class222 var1, int var2) {
      return this.field2528 != var1.field2528?false:(this.field2532 != var1.field2532?false:this.field2529 == var1.field2529);
   }

   public boolean equals(Object var1) {
      return this == var1?true:(!(var1 instanceof class222)?false:this.method4042((class222)var1, -2112462573));
   }

   public String toString() {
      return this.method4047(",", 404618696);
   }

   public String aab() {
      return this.method4047(",", -2099021951);
   }

   public String aau() {
      return this.method4047(",", -332870423);
   }

   public String aak() {
      return this.method4047(",", 406555596);
   }

   public int hashCode() {
      return this.method4043(-1927907066);
   }

}
