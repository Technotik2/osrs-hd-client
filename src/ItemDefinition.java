import java.awt.Component;

final class ItemDefinition {

	private short[] textureReplace;
	private int anInt752;
	private int anInt753 = -1;
	int yOffset2d;
	private int inventoryModelId;
	int anInt756 = -1;
	int cost = 1;
	int anInt758;
	static int anInt759;
	private int anInt760 = 0;
	int femaleModel0;
	int anInt762;
	static int anInt763;
	int stackable;
	private short[] textureFind;
	int[] countCo;
	int anInt767;
	int anInt768;
	private int anInt769;
	RSString name;
	private int maleModel1;
	private short[] colorReplace;
	private int anInt773 = -1;
	private short[] colorFind;
	private int anInt775;
	private int anInt776 = -1;
	private int anInt777;
	private int anInt778;
	boolean members;
	private int anInt780;
	static int[] anIntArray781 = new int[99];
	int anInt782 = 0;
	RSString[] inventoryOptions;
	private int anInt784;
	private byte[] aByteArray785;
	int xan2d;
	int itemId;
	int anInt788;
	int anInt789;
	private int anInt790;
	int anInt791;
	int xOffset2d;
	int maleModel0;
	private int femaleModel1;
	int anInt795;
	private int anInt796;
	private int anInt797;
	class325 aClass130_798;
	int yan2d;
	int anInt800;
	RSString[] groundOptions;
	private int anInt802;
	private int anInt803;
	int[] countObj;
	private int anInt805;
	static RSString aClass94_806;
	boolean aBoolean807;
	static RSString aClass94_808;
	static RSString aClass94_809;
	int zoom2d;
	private static RSString aClass94_811;

	final boolean method1102(boolean var1, boolean var2) {
		try {
			int var3 = this.anInt803;
			int var4 = this.anInt796;
			if (var1) {
				var3 = this.anInt773;
				var4 = this.anInt753;
			}

			if (var3 == -1) {
				return true;
			} else {
				boolean var5 = true;
				if (!get_item_model_data().method2129((byte) -75, 0, var3)) {
					var5 = false;
				}

				if (var4 != -1 && !get_item_model_data().method2129((byte) 58, 0, var4)) {
					var5 = false;
				}

				return var5;
			}
		} catch (RuntimeException var6) {
			throw Class44.method1067(var6, "h.G(" + var1 + ',' + var2 + ')');
		}
	}

	public static final boolean native_530(int item_id) {
		return (false);
	}

	public static final boolean osrs(int item_id) {
		if (!native_530(item_id)) {
			if (Settings.osrs_chars) {
				return true;
			}
		}
		return (item_id == 13239);
	}

	public JS5Index get_item_config_data() {
		if (osrs(this.itemId)) {
			return Class97.item_config_data_osrs;
		} else {
			return Class97.item_config_data_530;
		}
	}

	public static JS5Index get_item_config_data(int item_id) {
		if (osrs(item_id)) {
			return Class97.item_config_data_osrs;
		} else {
			return Class97.item_config_data_530;
		}
	}

	public JS5Index get_item_model_data() {
		if (osrs(this.itemId)) {
			return Class3_Sub29.item_model_data_osrs;
		} else {
			return Class3_Sub29.item_model_data_530;
		}
	}

	static final void method1103(JS5Index var0, JS5Index var1, boolean var2) {
		try {
			Class3_Sub13_Sub14.aClass153_3173 = var0;
			Class29.aClass153_557 = var1;
			if (!var2) {
				;
			}
		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "h.B(" + (var0 != null ? "{...}" : "null") + ','
					+ (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
		}
	}

	final RSString method1105(int var1, RSString var2, int var3) {
		try {
			if (this.aClass130_798 == null) {
				return var2;
			} else {
				if (var1 < 90) {
					method1111(-111);
				}

				Class3_Sub29 var4 = (Class3_Sub29) this.aClass130_798.method1780((long) var3, 0);
				return null != var4 ? var4.aClass94_2586 : var2;
			}
		} catch (RuntimeException var5) {
			throw Class44.method1067(var5, "h.S(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
		}
	}

	final ItemDefinition method1106(int var1, int var2) {
		try {
			int var3 = 58 % ((-28 - var2) / 48);
			if (this.countObj != null && var1 > 1) {
				int var4 = -1;

				for (int var5 = 0; 10 > var5; ++var5) {
					if (this.countCo[var5] <= var1 && this.countCo[var5] != 0) {
						var4 = this.countObj[var5];
					}
				}

				if (var4 != -1) {
					return ItemDefinition.getItemDefinition(var4, (byte) 97);
				}
			}

			return this;
		} catch (RuntimeException var6) {
			throw Class44.method1067(var6, "h.H(" + var1 + ',' + var2 + ')');
		}
	}

	static final WorldListEntry method1107(int var0) {
		try {
			if (Class3_Sub13_Sub16.aClass44_Sub1Array3201.length > Class3_Sub6.anInt2291) {
				return Class3_Sub13_Sub16.aClass44_Sub1Array3201[Class3_Sub6.anInt2291++];
			} else {
				if (var0 != 5422) {
					method1107(-66);
				}

				return null;
			}
		} catch (RuntimeException var2) {
			throw Class44.method1067(var2, "h.R(" + var0 + ')');
		}
	}

	final boolean method1108(byte var1, boolean var2) {
		try {
			int var4 = this.maleModel1;
			int var3 = this.maleModel0;
			int var6 = 106 % ((var1 - 24) / 58);
			int var5 = this.anInt769;
			if (var2) {
				var5 = this.anInt776;
				var3 = this.femaleModel0;
				var4 = this.femaleModel1;
			}

			if (var3 != -1) {
				boolean var7 = true;
				if (!get_item_model_data().method2129((byte) -90, 0, var3)) {
					var7 = false;
				}

				if (var4 != -1 && !get_item_model_data().method2129((byte) -114, 0, var4)) {
					var7 = false;
				}

				if (-1 != var5 && !get_item_model_data().method2129((byte) 83, 0, var5)) {
					var7 = false;
				}

				return var7;
			} else {
				return true;
			}
		} catch (RuntimeException var8) {
			throw Class44.method1067(var8, "h.C(" + var1 + ',' + var2 + ')');
		}
	}

	final void method1109(byte var1, ItemDefinition var2, ItemDefinition var3) {
		try {
			this.aByteArray785 = var2.aByteArray785;
			this.anInt778 = var2.anInt778;
			this.aClass130_798 = var2.aClass130_798;
			this.anInt769 = var2.anInt769;
			this.femaleModel0 = var2.femaleModel0;
			this.anInt775 = var2.anInt775;
			this.inventoryOptions = new RSString[5];
			this.inventoryModelId = var3.inventoryModelId;
			this.zoom2d = var3.zoom2d;
			this.cost = 0;
			this.anInt782 = var2.anInt782;
			this.anInt773 = var2.anInt773;
			this.colorFind = var2.colorFind;
			this.anInt768 = var3.anInt768;
			this.maleModel1 = var2.maleModel1;
			this.yan2d = var3.yan2d;
			this.anInt803 = var2.anInt803;
			this.anInt796 = var2.anInt796;
			this.anInt760 = var2.anInt760;
			this.xan2d = var3.xan2d;
			this.yOffset2d = var3.yOffset2d;
			this.anInt753 = var2.anInt753;
			this.anInt777 = var2.anInt777;
			this.colorReplace = var2.colorReplace;
			this.anInt802 = var2.anInt802;
			this.anInt752 = var2.anInt752;
			this.xOffset2d = var3.xOffset2d;
			if (var1 != 69) {
				this.cost = 109;
			}

			this.maleModel0 = var2.maleModel0;
			this.femaleModel1 = var2.femaleModel1;
			this.name = var2.name;
			this.textureReplace = var2.textureReplace;
			this.textureFind = var2.textureFind;
			this.groundOptions = var2.groundOptions;
			this.members = var2.members;
			this.anInt776 = var2.anInt776;
			if (null != var2.inventoryOptions) {
				for (int var4 = 0; var4 < 4; ++var4) {
					this.inventoryOptions[var4] = var2.inventoryOptions[var4];
				}
			}

			this.inventoryOptions[4] = RenderAnimationDefinition.aClass94_361;
		} catch (RuntimeException var5) {
			throw Class44.method1067(var5, "h.J(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ','
					+ (var3 != null ? "{...}" : "null") + ')');
		}
	}

	final Class140_Sub1 method1110(int var1, int var2, int var3, Class142 var4, int var5, int var6) {
		try {
			if (var1 < 94) {
				this.inventoryModelId = -67;
			}

			if (this.countObj != null && var5 > 1) {
				int var7 = -1;

				for (int var8 = 0; var8 < 10; ++var8) {
					if (this.countCo[var8] <= var5 && this.countCo[var8] != 0) {
						var7 = this.countObj[var8];
					}
				}

				if (var7 != -1) {
					return ItemDefinition.getItemDefinition(var7, (byte) 109).method1110(113, var2, var3, var4, 1,
							var6);
				}
			}

			Class140_Sub1 var11 = (Class140_Sub1) Class143.aClass93_1874.get((long) this.itemId, (byte) 121);
			if (var11 == null) {
				Class140_Sub5 var12 = Class140_Sub5.method2015(get_item_model_data(), this.inventoryModelId, 0);
				if (null == var12) {
					return null;
				}

				int var9;
				if (null != this.colorFind) {
					for (var9 = 0; this.colorFind.length > var9; ++var9) {
						if (null != this.aByteArray785 && this.aByteArray785.length > var9) {
							var12.method2016(this.colorFind[var9],
									Class3_Sub13_Sub38.aShortArray3453[this.aByteArray785[var9] & 255]);
						} else {
							var12.method2016(this.colorFind[var9], this.colorReplace[var9]);
						}
					}
				}

				if (this.textureFind != null) {
					for (var9 = 0; var9 < this.textureFind.length; ++var9) {
						var12.method1998(this.textureFind[var9], this.textureReplace[var9]);
					}
				}

				var11 = var12.method2008(this.anInt784 + 64, 768 + this.anInt790, -50, -10, -50);
				if (this.anInt805 != 128 || this.anInt780 != 128 || this.anInt797 != 128) {
					var11.method1881(this.anInt805, this.anInt780, this.anInt797);
				}

				var11.aBoolean2699 = true;
				if (Class138.aBoolean1807) {
					((Class140_Sub1_Sub1) var11).method1920(false, false, false, true, false, false, true);
				}

				Class143.aClass93_1874.put((byte) -123, var11, (long) this.itemId);
			}

			if (var4 != null) {
				var11 = var4.method2055(var11, (byte) -88, var2, var3, var6);
			}

			return var11;
		} catch (RuntimeException var10) {
			throw Class44.method1067(var10, "h.E(" + var1 + ',' + var2 + ',' + var3 + ','
					+ (var4 != null ? "{...}" : "null") + ',' + var5 + ',' + var6 + ')');
		}
	}

	public static void method1111(int var0) {
		try {
			aClass94_808 = null;
			anIntArray781 = null;
			aClass94_809 = null;
			if (var0 == 3327) {
				aClass94_806 = null;
				aClass94_811 = null;
			}
		} catch (RuntimeException var2) {
			throw Class44.method1067(var2, "h.P(" + var0 + ')');
		}
	}

	final void method1112(int var1) {
		try {
			if (var1 != 5401) {
				method1103((JS5Index) null, (JS5Index) null, true);
			}

		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "h.O(" + var1 + ')');
		}
	}

	final void decode(Buffer buffer) {
		while (true) {
			int opcode = buffer.readUnsignedByte();
			if (opcode == 0) {
				break;
			}
			if (osrs(this.itemId)) {
				this.parse_op_osrs(buffer, opcode);
			} else {
				this.parse_op_530(buffer, opcode);
			}
		}
	}

	private final void parse_op_530(Buffer buffer, int opcode) {
		try {
			if (opcode == 1) {
				this.inventoryModelId = buffer.readUnsignedShort();
			} else if (opcode == 2) {
				this.name = buffer.readString();
			} else if (opcode == 4) {
				this.zoom2d = buffer.readUnsignedShort();
			} else if (opcode == 5) {
				this.xan2d = buffer.readUnsignedShort();
			} else if (opcode == 6) {
				this.yan2d = buffer.readUnsignedShort();
			} else if (opcode == 7) {
				this.xOffset2d = buffer.readUnsignedShort();
				if (this.xOffset2d > 32767) {
					this.xOffset2d -= 65536;
				}
			} else if (opcode == 8) {
				this.yOffset2d = buffer.readUnsignedShort();
				if (this.yOffset2d > 32767) {
					this.yOffset2d -= 65536;
				}
			} else if (opcode == 11) {
				this.stackable = 1;
			} else if (opcode == 12) {
				this.cost = buffer.readInt();
			} else if (opcode == 16) {
				this.members = true;
			} else if (opcode == 23) {
				this.maleModel0 = buffer.readUnsignedShort();
			} else if (opcode == 24) {
				this.maleModel1 = buffer.readUnsignedShort();
			} else if (opcode == 25) {
				this.femaleModel0 = buffer.readUnsignedShort();
			} else if (opcode == 26) {
				this.femaleModel1 = buffer.readUnsignedShort();
			} else if (opcode >= 30 && opcode < 35) {
				this.groundOptions[-30 + opcode] = buffer.readString();
				if (this.groundOptions[opcode + -30].method1531(25,
						Class3_Sub13_Sub3.aClass94_3051)) {
					this.groundOptions[-30 + opcode] = null;
				}
			} else if (35 <= opcode && 40 > opcode) {
				this.inventoryOptions[-35 + opcode] = buffer.readString();
			} else {
				int var5;
				int var6;
				if (opcode == 40) {
					var5 = buffer.readUnsignedByte();
					this.colorReplace = new short[var5];
					this.colorFind = new short[var5];

					for (var6 = 0; var5 > var6; ++var6) {
						this.colorFind[var6] = (short) buffer.readUnsignedShort();
						this.colorReplace[var6] = (short) buffer.readUnsignedShort();
					}
				} else if (opcode == 41) {
					var5 = buffer.readUnsignedByte();
					this.textureReplace = new short[var5];
					this.textureFind = new short[var5];

					for (var6 = 0; var6 < var5; ++var6) {
						this.textureFind[var6] = (short) buffer.readUnsignedShort();
						this.textureReplace[var6] = (short) buffer.readUnsignedShort();
					}
				} else if (42 == opcode) {
					var5 = buffer.readUnsignedByte();
					this.aByteArray785 = new byte[var5];

					for (var6 = 0; var5 > var6; ++var6) {
						this.aByteArray785[var6] = buffer.readByte();
					}
				} else if (opcode == 65) {
					this.aBoolean807 = true;
				} else if (opcode == 78) {
					this.anInt769 = buffer.readUnsignedShort();
				} else if (opcode == 79) {
					this.anInt776 = buffer.readUnsignedShort();
				} else if (90 == opcode) {
					this.anInt803 = buffer.readUnsignedShort();
				} else if (opcode == 91) {
					this.anInt773 = buffer.readUnsignedShort();
				} else if (opcode == 92) {
					this.anInt796 = buffer.readUnsignedShort();
				} else if (opcode == 93) {
					this.anInt753 = buffer.readUnsignedShort();
				} else if (opcode == 95) {
					this.anInt768 = buffer.readUnsignedShort();
				} else if (opcode == 96) {
					this.anInt800 = buffer.readUnsignedByte();
				} else if (opcode == 97) {
					this.anInt789 = buffer.readUnsignedShort();
				} else if (opcode == 98) {
					this.anInt791 = buffer.readUnsignedShort();
				} else if (opcode >= 100 && opcode < 110) {
					if (null == this.countObj) {
						this.countObj = new int[10];
						this.countCo = new int[10];
					}

					this.countObj[-100 + opcode] = buffer.readUnsignedShort();
					this.countCo[opcode + -100] = buffer.readUnsignedShort();
				} else if (opcode == 110) {
					this.anInt805 = buffer.readUnsignedShort();
				} else if (opcode == 111) {
					this.anInt780 = buffer.readUnsignedShort();
				} else if (opcode == 112) {
					this.anInt797 = buffer.readUnsignedShort();
				} else if (opcode == 113) {
					this.anInt784 = buffer.readByte();
				} else if (opcode == 114) {
					this.anInt790 = 5 * buffer.readByte();
				} else if (opcode == 115) {
					this.anInt782 = buffer.readUnsignedByte();
				} else if (opcode == 121) {
					this.anInt795 = buffer.readUnsignedShort();
				} else if (opcode == 122) {
					this.anInt762 = buffer.readUnsignedShort();
				} else if (opcode == 125) {
					this.anInt760 = buffer.readByte();
					this.anInt778 = buffer.readByte();
					this.anInt775 = buffer.readByte();
				} else if (opcode == 126) {
					this.anInt777 = buffer
							.readByte();
					this.anInt802 = buffer
							.readByte();
					this.anInt752 = buffer
							.readByte();
				} else if (opcode == 127) {
					this.anInt767 = buffer.readUnsignedByte();
					this.anInt758 = buffer.readUnsignedShort();
				} else if (opcode == 128) {
					this.anInt788 = buffer.readUnsignedByte();
					this.anInt756 = buffer.readUnsignedShort();
				} else if (opcode == 129) {
					buffer.readUnsignedByte();
					buffer.readUnsignedShort();
				} else if (opcode == 130) {
					buffer.readUnsignedByte();
					buffer.readUnsignedShort();
				} else if (opcode == 249) {
					var5 = buffer.readUnsignedByte();
					if (null == this.aClass130_798) {
						var6 = class216
								.method4012(
										var5,
										(byte) 97);
						this.aClass130_798 = new class325(
								var6);
					}

					for (var6 = 0; var6 < var5; ++var6) {
						boolean var7 = buffer
								.readUnsignedByte() == 1;
						int var8 = buffer
								.getTriByte(
										(byte) 122);
						Object var9;
						if (!var7) {
							var9 = new class188(
									buffer.readInt());
						} else {
							var9 = new Class3_Sub29(
									buffer.readString());
						}

						this.aClass130_798
								.method5963((Class3) var9,
										(long) var8);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private final void parse_op_osrs(Buffer buffer, int opcode) {
		if (opcode == 1) {
			this.inventoryModelId = buffer.readUnsignedShort();
		} else if (opcode == 2) {
			this.name = buffer.readString();
		} else if (opcode == 4) {
			this.zoom2d = buffer.readUnsignedShort();
		} else if (opcode == 5) {
			this.xan2d = buffer.readUnsignedShort();
		} else if (opcode == 6) {
			this.yan2d = buffer.readUnsignedShort();
		} else if (opcode == 7) {
			this.xOffset2d = buffer.readUnsignedShort();
			if (~this.xOffset2d < -32768) {
				this.xOffset2d -= 65536;
			}
		} else if (opcode == 8) {
			this.yOffset2d = buffer.readUnsignedShort();
			if (this.yOffset2d > 32767) {
				this.yOffset2d -= 65536;
			}
		} else if (opcode == 11) {
			this.stackable = 1;
		} else if (opcode == 12) {
			this.cost = buffer.readInt();
		} else if (opcode == 16) {
			this.members = true;
		} else if (opcode == 23) {
			this.maleModel0 = buffer.readUnsignedShort();
			buffer.readUnsignedByte();
		} else if (opcode == 24) {
			this.maleModel1 = buffer.readUnsignedShort();
		} else if (opcode == 25) {
			this.femaleModel0 = buffer.readUnsignedShort();
			buffer.readUnsignedByte();
		} else if (opcode == 26) {
			this.femaleModel1 = buffer.readUnsignedShort();
		} else if (opcode >= 30 && -36 < ~opcode) {
			this.groundOptions[-30 + opcode] = buffer.readString();
			if (this.groundOptions[opcode + -30].method1531(25, Class3_Sub13_Sub3.aClass94_3051)) {
				this.groundOptions[-30 + opcode] = null;
			}
		} else if (35 <= opcode && 40 > opcode) {
			this.inventoryOptions[-35 + opcode] = buffer.readString();
		} else {
			int var5;
			int var6;
			if (opcode == 40) {
				var5 = buffer.readUnsignedByte();
				this.colorReplace = new short[var5];
				this.colorFind = new short[var5];

				for (var6 = 0; var5 > var6; ++var6) {
					this.colorFind[var6] = (short) buffer.readUnsignedShort();
					this.colorReplace[var6] = (short) buffer.readUnsignedShort();
				}
			} else if (opcode == 41) {
				var5 = buffer.readUnsignedByte();
				this.textureReplace = new short[var5];
				this.textureFind = new short[var5];

				for (var6 = 0; ~var5 < ~var6; ++var6) {
					this.textureFind[var6] = (short) buffer.readUnsignedShort();
					this.textureReplace[var6] = (short) buffer.readUnsignedShort();
				}
			} else if (opcode == 42) {
				buffer.readUnsignedByte();
			} else if (opcode == 65) {
				this.aBoolean807 = true;
			} else if (opcode == 78) {
				this.anInt769 = buffer.readUnsignedShort();
			} else if (opcode == 79) {
				this.anInt776 = buffer.readUnsignedShort();
			} else if (opcode == 90) {
				this.anInt803 = buffer.readUnsignedShort();
			} else if (opcode == 91) {
				this.anInt773 = buffer.readUnsignedShort();
			} else if (opcode == 92) {
				this.anInt796 = buffer.readUnsignedShort();
			} else if (opcode == 93) {
				this.anInt753 = buffer.readUnsignedShort();
			} else if (opcode == 95) {
				this.anInt768 = buffer.readUnsignedShort();
			} else if (opcode == 97) {
				this.anInt789 = buffer.readUnsignedShort();
			} else if (opcode == 98) {
				this.anInt791 = buffer.readUnsignedShort();
			} else if (-101 >= ~opcode && ~opcode > -111) {
				if (null == this.countObj) {
					this.countObj = new int[10];
					this.countCo = new int[10];
				}

				this.countObj[-100 + opcode] = buffer.readUnsignedShort();
				this.countCo[opcode + -100] = buffer.readUnsignedShort();
			} else if (opcode == 110) {
				this.anInt805 = buffer.readUnsignedShort();
			} else if (opcode == 111) {
				this.anInt780 = buffer.readUnsignedShort();
			} else if (opcode == 112) {
				this.anInt797 = buffer.readUnsignedShort();
			} else if (opcode == 113) {
				this.anInt784 = buffer.readUnsignedByte();
			} else if (opcode == 114) {
				this.anInt790 = buffer.readUnsignedByte();
			} else if (opcode == 115) {
				this.anInt782 = buffer.readUnsignedByte();
			} else if (opcode == 139) {
				int boughtId = buffer.readUnsignedShort();
			} else if (opcode == 140) {
				int boughtTemplateId = buffer.readUnsignedShort();
			} else if (opcode == 148) {
				int placeholderId = buffer.readUnsignedShort();
			} else if (opcode == 149) {
				int placeholderTemplateId = buffer.readUnsignedShort();
			} else if (opcode == 249) {
				var5 = buffer.readUnsignedByte();
				if (null == this.aClass130_798) {
					var6 = class216.method4012(var5, (byte) 97);
					this.aClass130_798 = new class325(var6);
				}

				for (var6 = 0; var6 < var5; ++var6) {
					boolean var7 = buffer.readUnsignedByte() == 1;
					int var8 = buffer.getTriByte((byte) 122);
					Object var9;
					if (!var7) {
						var9 = new class188(buffer.readInt());
					} else {
						var9 = new Class3_Sub29(buffer.readString());
					}

					this.aClass130_798.method5963((Class3) var9, (long) var8);
				}
			}
		}
	}

	final int method1115(int var1, int var2, int var3) {
		try {
			int var4 = -82 % ((-63 - var2) / 55);
			if (this.aClass130_798 != null) {
				class188 var5 = (class188) this.aClass130_798.method1780((long) var3, 0);
				return null != var5 ? var5.anInt2467 : var1;
			} else {
				return var1;
			}
		} catch (RuntimeException var6) {
			throw Class44.method1067(var6, "h.I(" + var1 + ',' + var2 + ',' + var3 + ')');
		}
	}

	final Class140_Sub5 method1116(boolean var1, byte var2) {
		try {
			int var4 = this.anInt796;
			if (var2 != -109) {
				return (Class140_Sub5) null;
			} else {
				int var3 = this.anInt803;
				if (var1) {
					var4 = this.anInt753;
					var3 = this.anInt773;
				}

				if (-1 != var3) {
					Class140_Sub5 var5 = Class140_Sub5.method2015(get_item_model_data(), var3, 0);
					if (-1 != var4) {
						Class140_Sub5 var6 = Class140_Sub5.method2015(get_item_model_data(), var4, 0);
						Class140_Sub5[] var7 = new Class140_Sub5[] { var5, var6 };
						var5 = new Class140_Sub5(var7, 2);
					}

					int var9;
					if (this.colorFind != null) {
						for (var9 = 0; var9 < this.colorFind.length; ++var9) {
							var5.method2016(this.colorFind[var9], this.colorReplace[var9]);
						}
					}

					if (this.textureFind != null) {
						for (var9 = 0; var9 < this.textureFind.length; ++var9) {
							var5.method1998(this.textureFind[var9], this.textureReplace[var9]);
						}
					}

					return var5;
				} else {
					return null;
				}
			}
		} catch (RuntimeException var8) {
			throw Class44.method1067(var8, "h.A(" + var1 + ',' + var2 + ')');
		}
	}

	final Class140_Sub5 method1117(boolean var1, int var2) {
		try {
			int var3 = this.maleModel0;
			if (var2 < 77) {
				this.aClass130_798 = (class325) null;
			}

			int var4 = this.maleModel1;
			int var5 = this.anInt769;
			if (var1) {
				var5 = this.anInt776;
				var3 = this.femaleModel0;
				var4 = this.femaleModel1;
			}

			if (var3 == -1) {
				return null;
			} else {
				Class140_Sub5 var6 = Class140_Sub5.method2015(get_item_model_data(), var3, 0);
				if (var4 != -1) {
					Class140_Sub5 var7 = Class140_Sub5.method2015(get_item_model_data(), var4, 0);
					if (-1 == var5) {
						Class140_Sub5[] var8 = new Class140_Sub5[] { var6, var7 };
						var6 = new Class140_Sub5(var8, 2);
					} else {
						Class140_Sub5 var12 = Class140_Sub5.method2015(get_item_model_data(), var5, 0);
						Class140_Sub5[] var9 = new Class140_Sub5[] { var6, var7, var12 };
						var6 = new Class140_Sub5(var9, 3);
					}
				}

				if (!var1 && (this.anInt760 != 0 || this.anInt778 != 0 || this.anInt775 != 0)) {
					var6.method2001(this.anInt760, this.anInt778, this.anInt775);
				}

				if (var1 && (this.anInt777 != 0 || this.anInt802 != 0 || this.anInt752 != 0)) {
					var6.method2001(this.anInt777, this.anInt802, this.anInt752);
				}

				int var11;
				if (this.colorFind != null) {
					for (var11 = 0; var11 < this.colorFind.length; ++var11) {
						var6.method2016(this.colorFind[var11], this.colorReplace[var11]);
					}
				}

				if (this.textureFind != null) {
					for (var11 = 0; var11 < this.textureFind.length; ++var11) {
						var6.method1998(this.textureFind[var11], this.textureReplace[var11]);
					}
				}

				return var6;
			}
		} catch (RuntimeException var10) {
			throw Class44.method1067(var10, "h.D(" + var1 + ',' + var2 + ')');
		}
	}

	final void method1118(ItemDefinition var1, ItemDefinition var2, boolean var3) {
		try {
			this.name = var1.name;
			this.zoom2d = var2.zoom2d;
			if (var3) {
				this.anInt780 = -70;
			}

			this.colorFind = var2.colorFind;
			this.colorReplace = var2.colorReplace;
			this.xan2d = var2.xan2d;
			this.yan2d = var2.yan2d;
			this.textureReplace = var2.textureReplace;
			this.inventoryModelId = var2.inventoryModelId;
			this.aByteArray785 = var2.aByteArray785;
			this.anInt768 = var2.anInt768;
			this.cost = var1.cost;
			this.stackable = 1;
			this.yOffset2d = var2.yOffset2d;
			this.xOffset2d = var2.xOffset2d;
			this.textureFind = var2.textureFind;
			this.members = var1.members;
		} catch (RuntimeException var5) {
			throw Class44.method1067(var5, "h.N(" + (var1 != null ? "{...}" : "null") + ','
					+ (var2 != null ? "{...}" : "null") + ',' + var3 + ')');
		}
	}

	static final void method1119(Component var0, boolean var1) {
		try {
			var0.addMouseListener(Class3_Sub28_Sub7_Sub1.aClass149_4047);
			if (var1) {
				aClass94_811 = (RSString) null;
			}

			var0.addMouseMotionListener(Class3_Sub28_Sub7_Sub1.aClass149_4047);
			var0.addFocusListener(Class3_Sub28_Sub7_Sub1.aClass149_4047);
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "h.K(" + (var0 != null ? "{...}" : "null") + ',' + var1 + ')');
		}
	}

	final Class140_Sub1_Sub2 method1120(int var1) {
		try {
			Class140_Sub5 var2 = Class140_Sub5.method2015(get_item_model_data(), this.inventoryModelId, 0);
			if (var2 == null) {
				return null;
			} else {
				int var3;
				if (this.colorFind != null) {
					for (var3 = 0; this.colorFind.length > var3; ++var3) {
						if (null != this.aByteArray785 && this.aByteArray785.length > var3) {
							var2.method2016(this.colorFind[var3],
									Class3_Sub13_Sub38.aShortArray3453[this.aByteArray785[var3] & 255]);
						} else {
							var2.method2016(this.colorFind[var3], this.colorReplace[var3]);
						}
					}
				}

				if (this.textureFind != null) {
					for (var3 = 0; var3 < this.textureFind.length; ++var3) {
						var2.method1998(this.textureFind[var3], this.textureReplace[var3]);
					}
				}

				Class140_Sub1_Sub2 var5 = var2.method2000(64 - -this.anInt784, 768 - -this.anInt790, -50, -10, -50);
				if (var1 != 18206) {
					this.method1105(-67, (RSString) null, -37);
				}

				if (this.anInt805 != 128 || this.anInt780 != 128 || this.anInt797 != 128) {
					var5.method1881(this.anInt805, this.anInt780, this.anInt797);
				}

				return var5;
			}
		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "h.L(" + var1 + ')');
		}
	}

	static final ItemDefinition getItemDefinition(int itemId, byte var1) {
		try {
			ItemDefinition var2 = null;
			if (var2 == null) {
				byte[] data;
				if (osrs(itemId)) {
					data = get_item_config_data(itemId).getFile(10, itemId);
				} else {
					data = get_item_config_data(itemId).getFile(itemId >>> 8, 255 & itemId);
				}
				
				var2 = new ItemDefinition();
				var2.itemId = itemId;
				if (data != null) {
					var2.decode(new Buffer(data));
				}
				var2.method1112(5401);
				if (var2.anInt791 != -1) {
					var2.method1118(getItemDefinition(var2.anInt789, (byte) 70),
							getItemDefinition(var2.anInt791, (byte) 73), false);
				}

				if (var2.anInt762 != -1) {
					var2.method1109((byte) 69, getItemDefinition(var2.anInt795, (byte) 111),
							getItemDefinition(var2.anInt762, (byte) 86));
				}

				if (!Class139.aBoolean1827 && var2.members) {
					var2.name = Script.aClass94_3691;
					var2.anInt782 = 0;
					var2.inventoryOptions = RuntimeException_Sub1.aClass94Array2119;
					var2.aBoolean807 = false;
					var2.groundOptions = Buffer.aClass94Array2596;
				}

				Class3_Sub28_Sub4.aClass93_3572.put((byte) -107, var2, (long) itemId);
				return var2;
			} else {
				return var2;
			}
		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "fk.F(" + itemId + ',' + var1 + ')');
		}
	}

	public ItemDefinition() {
		this.name = Class40.aClass94_672;
		this.anInt775 = 0;
		this.anInt784 = 0;
		this.anInt769 = -1;
		this.anInt796 = -1;
		this.anInt791 = -1;
		this.anInt777 = 0;
		this.anInt780 = 128;
		this.anInt767 = -1;
		this.anInt758 = -1;
		this.anInt768 = 0;
		this.anInt762 = -1;
		this.anInt795 = -1;
		this.femaleModel0 = -1;
		this.maleModel1 = -1;
		this.yOffset2d = 0;
		this.xan2d = 0;
		this.yan2d = 0;
		this.anInt800 = 0;
		this.stackable = 0;
		this.anInt789 = -1;
		this.femaleModel1 = -1;
		this.anInt788 = -1;
		this.anInt797 = 128;
		this.members = false;
		this.anInt752 = 0;
		this.xOffset2d = 0;
		this.anInt803 = -1;
		this.anInt802 = 0;
		this.maleModel0 = -1;
		this.groundOptions = new RSString[] { null, null, Class3_Sub13_Sub33.aClass94_3397, null, null };
		this.anInt805 = 128;
		this.anInt790 = 0;
		this.anInt778 = 0;
		this.inventoryOptions = new RSString[] { null, null, null, null, Class140_Sub3.aClass94_2744 };
		this.zoom2d = 2000;
		this.aBoolean807 = false;
	}

	static {
		int var0 = 0;

		for (int var1 = 0; var1 < 99; ++var1) {
			int var2 = 1 + var1;
			int var3 = (int) (Math.pow(2.0D, (double) var2 / 7.0D) * 300.0D + (double) var2);
			var0 += var3;
			anIntArray781[var1] = var0 / 4;
		}

		aClass94_808 = Class3_Sub4.createRSString(" zuerst von Ihrer Ignorieren)2Liste(Q", (byte) -120);
		aClass94_811 = Class3_Sub4.createRSString("green:", (byte) -118);
		aClass94_809 = aClass94_811;
		aClass94_806 = aClass94_811;
	}
}
