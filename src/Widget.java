
final class Widget {

	boolean isHidden = false;
	Object[] anObjectArray156;
	boolean field2578;
	static long aLong1489;
	Object[] onStatTransmitListener;
	Object[] onLoadListener;
	int anInt160 = 1;
	Object[] anObjectArray161;
	byte yPositionMode = 0;
	boolean aBoolean163;
	int modelZoom = 100;
	Object[] onClickListener;
	int originalY;
	boolean lineDirection;
	int field2582;
	short aShort169 = 3000;
	Object[] onClickRepeatListener;
	RSString[] actions;
	RSString alternateText;
	RSString[] configActions;
	Object[] onInvTransmitListener;
	int[] invTransmitTriggers;
	Object[] anObjectArray176;
	int originalWidth;
	boolean flippedVertically;
	int dragDeadTime = 0;
	Object[] onHoldListener;
	boolean orthogonal = false;
	int rotationX = 0;
	Object[] onScrollWheelListener;
	int modelHeightOverride;
	int[] anIntArray185;
	boolean spriteTiling = false;
	int type;
	boolean aBoolean188 = false;
	int contentType;
	int parentId;
	int index = -1;
	int anInt192;
	int field2642 = 0;
	int xTextAlignment = 0;
	boolean aBoolean195;
	private int alternateModelId;
	int[] sprites;
	int alternateAnimation = -1;
	boolean flippedHorizontally;
	boolean dragRenderBehavior;
	int modelId;
	int modelType;
	Object[] onTargetEnterListener;
	int anInt204;
	int lineHeight = 0;
	Object[] anObjectArray206;
	int[] anIntArray207;
	int field2589 = 0;
	static RSString aClass94_209 = Class3_Sub4.createRSString("event_opbase", (byte) -127);
	int field2653 = 0;
	int[] anIntArray211;
	int hoveredSiblingId;
	int anInt213;
	int dragDeadZone = 0;
	boolean textShadowed;
	int anInt216;
	Object[] anObjectArray217;
	int textColor;
	boolean noClickThrough, noScrollThrough;
	Object[] anObjectArray220;
	Object[] anObjectArray221;
	int alternateHoveredTextColor;
	int opacity;
	int spriteId = -1;
	int yTextAlignment;
	boolean filled = false;
	boolean aBoolean227;
	int hoveredTextColor;
	Object[] onDragCompleteListener;
	int offsetY2d = 0;
	byte[] aByteArray231;
	RSString text;
	boolean isIf3;
	int anInt234;
	Object[] anObjectArray235;
	static boolean aBoolean236 = true;
	int anInt237;
	int anInt238 = -1;
	Object[] onReleaseListener;
	int scrollWidth;
	byte heightMode;
	int anInt242;
	RSString aClass94_243;
	int originalHeight;
	RSString targetVerb;
	static float aFloat246;
	int field2646;
	Object[] onMouseOverListener;
	int[] anIntArray249;
	int lineWidth = 1;
	static RSString aClass94_251 = null;
	int scrollHeight;
	int alternateTextColor;
	int[] itemQuantities;
	int anInt255;
	Object[] anObjectArray256;
	ClickMask clickMask;
	int field2914;
	int offsetX2d;
	int anInt260;
	static long aLong261 = 0L;
	Widget[] children;
	byte[] aByteArray263;
	int anInt264;
	int anInt265;
	int anInt266;
	int anInt267;
	Object[] anObjectArray268;
	Object[] onTimerListener;
	int fontId;
	int anInt271;
	int[] yOffsets;
	byte xPositionMode;
	int[] statTransmitTriggers;
	int[] alternateOperators;
	Object[] onMouseRepeatListener;
	RSString name;
	static int anInt278 = -1;
	int hash;
	int rotationY;
	Object[] onMouseLeaveListener;
	Object[] onVarTransmitListener;
	int anInt283;
	int anInt284;
	int xPitch;
	int[] varTransmitTriggers;
	int shadowColor;
	int borderType;
	RSString tooltip;
	int yPitch;
	int[] anIntArray291;
	int anInt292;
	short aShort293;
	private int anInt294;
	Object[] onDragListener;
	int alternateSpriteId;
	static RSString aClass94_297 = Class3_Sub4.createRSString("Nehmen", (byte) -118);
	int[][] clientScripts;
	int[] anIntArray299;
	int[] xOffsets;
	int textureId;
	Widget field2648;
	Object[] onTargetLeaveListener;
	byte widthMode;
	int animation;
	int field2580;
	int[] alternateRhs;
	int rotationZ;
	boolean aBoolean309;
	int[] anIntArray310;
	int anInt311;
	int field2683;
	Object[] anObjectArray313;
	Object[] onOpListener;
	Object[] anObjectArray315;
	int originalX;
	int[] itemIds;
	int menuType;
	public boolean field2601;
	public boolean field2625;
	public int field2599;
	static boolean[] interfaceLoaded;
	static JS5Index widget_model_data_530, widget_model_data_osrs;
	static JS5Index widget_data_530, widget_data_osrs;
	static JS5Index widget_font_data_530, widget_font_data_osrs;
	static JS5Index widget_sprite_data_530, widget_sprite_data_osrs;

	public static JS5Index get_widget_model_data(int widget_id) {
		if (osrs(widget_id)) {
			return widget_model_data_osrs;
		} else {
			return widget_model_data_530;
		}
	}

	public static JS5Index get_widget_data(int widget_id) {
		if (osrs(widget_id)) {
			return widget_data_osrs;
		} else {
			return widget_data_530;
		}
	}

	public static JS5Index get_widget_font_data(int widget_id) {
		if (osrs(widget_id)) {
			return widget_font_data_osrs;
		} else {
			return widget_font_data_530;
		}
	}

	public JS5Index get_widget_model_data() {
		if (osrs(this.hash)) {
			return widget_model_data_osrs;
		} else {
			return widget_model_data_530;
		}
	}

	public JS5Index get_widget_data() {
		if (osrs(this.hash)) {
			return widget_data_osrs;
		} else {
			return widget_data_530;
		}
	}

	public JS5Index get_widget_font_data() {
		if (osrs(this.hash)) {
			return widget_font_data_osrs;
		} else {
			return widget_font_data_530;
		}
	}

	public JS5Index get_widget_sprite_data() {
		int id = this.hash >> 16;
		if (osrs(id)) {
			return widget_sprite_data_osrs;
		} else {
			return widget_sprite_data_530;
		}
	}

	public static final boolean osrs(int widget_id) {
		if (widget_id > 2000) {
			widget_id = widget_id >> 16;
		}
		return (widget_id == 483 || widget_id == 482 || widget_id == 481 || widget_id == 476 || widget_id == 261 || widget_id == 629 || widget_id == 12 || widget_id == 4 || widget_id == 84 || widget_id == 50
				|| widget_id == 160 || widget_id == 320 || widget_id == 87 || widget_id == 99 || widget_id == 60
				|| widget_id == 122);
	}

	final void method854(int var1, int var2, byte var3) {
		try {
			if (this.anIntArray249 == null || ~this.anIntArray249.length >= ~var1) {
				int[] var4 = new int[1 + var1];
				if (this.anIntArray249 != null) {
					int var5;
					for (var5 = 0; this.anIntArray249.length > var5; ++var5) {
						var4[var5] = this.anIntArray249[var5];
					}

					for (var5 = this.anIntArray249.length; ~var1 < ~var5; ++var5) {
						var4[var5] = -1;
					}
				}

				this.anIntArray249 = var4;
			}

			this.anIntArray249[var1] = var2;
			if (var3 != 43) {
				this.anIntArray211 = (int[]) null;
			}

		} catch (RuntimeException var6) {
			throw Class44.method1067(var6, "be.P(" + var1 + ',' + var2 + ',' + var3 + ')');
		}
	}

	final boolean method855(int var1) {
		try {
			if (this.anIntArray207 != null) {
				return true;
			} else {
				Class109_Sub1 var2 = RSString.method1539(0, true, this.spriteId, get_widget_sprite_data());
				if (null == var2) {
					return false;
				} else {
					var2.method1675();
					this.anIntArray207 = new int[var2.anInt1468];
					this.anIntArray291 = new int[var2.anInt1468];
					int var3 = 0;

					while (~var3 > ~var2.anInt1468) {
						int var4 = 0;
						int var5 = var2.anInt1461;
						int var6 = 0;

						while (true) {
							if (~var6 > ~var2.anInt1461) {
								if (-1 == ~var2.aByteArray2674[var2.anInt1461 * var3 + var6]) {
									++var6;
									continue;
								}

								var4 = var6;
							}

							for (var6 = var4; var2.anInt1461 > var6; ++var6) {
								if (0 == var2.aByteArray2674[var3 * var2.anInt1461 + var6]) {
									var5 = var6;
									break;
								}
							}

							this.anIntArray207[var3] = var4;
							this.anIntArray291[var3] = var5 - var4;
							++var3;
							break;
						}
					}

					if (var1 != -30721) {
						this.lineHeight = -68;
					}

					return true;
				}
			}
		} catch (RuntimeException var7) {
			throw Class44.method1067(var7, "be.G(" + var1 + ')');
		}
	}

	static final RSString method856(boolean var0) {
		try {
			if (!var0) {
				method869(127, -68);
			}

			RSString var1 = Class3_Sub28_Sub7_Sub1.aClass94_4052;
			RSString var2 = Class3_Sub28_Sub14.aClass94_3672;
			if (-1 != ~Class44.anInt718) {
				var1 = Player.aClass94_3971;
			}

			if (null != Class163_Sub2.aClass94_2996) {
				var2 = RenderAnimationDefinition.method903(
						new RSString[] { Class3_Sub28_Sub11.aClass94_3637, Class163_Sub2.aClass94_2996 }, (byte) -64);
			}

			return RenderAnimationDefinition.method903(
					new RSString[] { Class30.aClass94_577, var1, Class3_Sub28_Sub7.aClass94_3601,
							Class72.method1298((byte) 9, Class3_Sub20.language), Class151.aClass94_1932,
							Class72.method1298((byte) 9, Class3_Sub26.anInt2554), var2, Class140_Sub3.aClass94_2735 },
					(byte) -61);
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "be.N(" + var0 + ')');
		}
	}

	final void method857(int var3, RSString var2) {
		if (null == this.actions || ~this.actions.length >= ~var3) {
			RSString[] var4 = new RSString[1 + var3];
			if (null != this.actions) {
				for (int var5 = 0; ~this.actions.length < ~var5; ++var5) {
					var4[var5] = this.actions[var5];
				}
			}

			this.actions = var4;
		}

		this.actions[var3] = var2;
	}

	final void decode_if1_530(Buffer var2) {
		int var1 = 1;
		try {
			if (var1 >= -94) {
				this.dragDeadZone = -74;
			}

			this.isIf3 = false;
			this.type = var2.readUnsignedByte();
			this.menuType = var2.readUnsignedByte();
			this.contentType = var2.readUnsignedShort();
			this.originalX = var2.readShort();
			this.originalY = var2.readShort();
			this.originalWidth = var2.readUnsignedShort();
			this.originalHeight = var2.readUnsignedShort();
			this.widthMode = 0;
			this.heightMode = 0;
			this.xPositionMode = 0;
			this.yPositionMode = 0;
			this.opacity = var2.readUnsignedByte();
			this.parentId = var2.readUnsignedShort();
			if (~this.parentId != -65536) {
				this.parentId += -65536 & this.hash;
			} else {
				this.parentId = -1;
			}

			this.hoveredSiblingId = var2.readUnsignedShort();
			if (-65536 == ~this.hoveredSiblingId) {
				this.hoveredSiblingId = -1;
			}

			int var3 = var2.readUnsignedByte();
			int var4;
			if (-1 > ~var3) {
				this.alternateRhs = new int[var3];
				this.alternateOperators = new int[var3];

				for (var4 = 0; ~var3 < ~var4; ++var4) {
					this.alternateOperators[var4] = var2.readUnsignedByte();
					this.alternateRhs[var4] = var2.readUnsignedShort();
				}
			}

			var4 = var2.readUnsignedByte();
			int var5;
			int var6;
			int var7;
			if (-1 > ~var4) {
				this.clientScripts = new int[var4][];

				for (var5 = 0; ~var4 < ~var5; ++var5) {
					var6 = var2.readUnsignedShort();
					this.clientScripts[var5] = new int[var6];

					for (var7 = 0; ~var7 > ~var6; ++var7) {
						this.clientScripts[var5][var7] = var2.readUnsignedShort();
						if (~this.clientScripts[var5][var7] == -65536) {
							this.clientScripts[var5][var7] = -1;
						}
					}
				}
			}

			if (-1 == ~this.type) {
				this.scrollHeight = var2.readUnsignedShort();
				this.isHidden = 1 == var2.readUnsignedByte();
			}

			if (~this.type == -2) {
				var2.readUnsignedShort();
				var2.readUnsignedByte();
			}

			var5 = 0;
			if (~this.type == -3) {
				this.heightMode = 3;
				this.itemIds = new int[this.originalWidth * this.originalHeight];
				this.itemQuantities = new int[this.originalHeight * this.originalWidth];
				this.widthMode = 3;
				var6 = var2.readUnsignedByte();
				var7 = var2.readUnsignedByte();
				if (~var6 == -2) {
					var5 |= 268435456;
				}

				int var8 = var2.readUnsignedByte();
				if (~var7 == -2) {
					var5 |= 1073741824;
				}

				if (1 == var8) {
					var5 |= Integer.MIN_VALUE;
				}

				int var9 = var2.readUnsignedByte();
				if (var9 == 1) {
					var5 |= 536870912;
				}

				this.xPitch = var2.readUnsignedByte();
				this.yPitch = var2.readUnsignedByte();
				this.xOffsets = new int[20];
				this.yOffsets = new int[20];
				this.sprites = new int[20];

				int var10;
				for (var10 = 0; 20 > var10; ++var10) {
					int var11 = var2.readUnsignedByte();
					if (var11 == 1) {
						this.yOffsets[var10] = var2.readShort();
						this.xOffsets[var10] = var2.readShort();
						this.sprites[var10] = var2.readInt();
					} else {
						this.sprites[var10] = -1;
					}
				}

				this.configActions = new RSString[5];

				for (var10 = 0; var10 < 5; ++var10) {
					RSString var14 = var2.readString();
					if (~var14.length(-28) < -1) {
						this.configActions[var10] = var14;
						var5 |= 1 << 23 - -var10;
					}
				}
			}

			if (3 == this.type) {
				this.filled = 1 == var2.readUnsignedByte();
			}

			if (this.type == 4 || 1 == this.type) {
				this.xTextAlignment = var2.readUnsignedByte();
				this.yTextAlignment = var2.readUnsignedByte();
				this.lineHeight = var2.readUnsignedByte();
				this.fontId = var2.readUnsignedShort();
				if (~this.fontId == -65536) {
					this.fontId = -1;
				}

				this.textShadowed = 1 == var2.readUnsignedByte();
			}

			if (this.type == 4) {
				this.text = var2.readString();
				this.alternateText = var2.readString();
			}

			if (this.type == 1 || this.type == 3 || 4 == this.type) {
				this.textColor = var2.readInt();
			}

			if (~this.type == -4 || ~this.type == -5) {
				this.alternateTextColor = var2.readInt();
				this.hoveredTextColor = var2.readInt();
				this.alternateHoveredTextColor = var2.readInt();
			}

			if (-6 == ~this.type) {
				this.spriteId = var2.readInt();
				this.alternateSpriteId = var2.readInt();
			}

			if (6 == this.type) {
				this.modelType = 1;
				this.modelId = var2.readUnsignedShort();
				this.anInt294 = 1;
				if (this.modelId == '\uffff') {
					this.modelId = -1;
				}

				this.alternateModelId = var2.readUnsignedShort();
				if (this.alternateModelId == '\uffff') {
					this.alternateModelId = -1;
				}

				this.animation = var2.readUnsignedShort();
				if (~this.animation == -65536) {
					this.animation = -1;
				}

				this.alternateAnimation = var2.readUnsignedShort();
				if ('\uffff' == this.alternateAnimation) {
					this.alternateAnimation = -1;
				}

				this.modelZoom = var2.readUnsignedShort();
				this.rotationX = var2.readUnsignedShort();
				this.rotationZ = var2.readUnsignedShort();
			}

			if (7 == this.type) {
				this.heightMode = 3;
				this.widthMode = 3;
				this.itemIds = new int[this.originalHeight * this.originalWidth];
				this.itemQuantities = new int[this.originalWidth * this.originalHeight];
				this.xTextAlignment = var2.readUnsignedByte();
				this.fontId = var2.readUnsignedShort();
				if (~this.fontId == -65536) {
					this.fontId = -1;
				}

				this.textShadowed = ~var2.readUnsignedByte() == -2;
				this.textColor = var2.readInt();
				this.xPitch = var2.readShort();
				this.yPitch = var2.readShort();
				var6 = var2.readUnsignedByte();
				if (-2 == ~var6) {
					var5 |= 1073741824;
				}

				this.configActions = new RSString[5];

				for (var7 = 0; var7 < 5; ++var7) {
					RSString var13 = var2.readString();
					if (var13.length(-121) > 0) {
						this.configActions[var7] = var13;
						var5 |= 1 << 23 - -var7;
					}
				}
			}

			if (8 == this.type) {
				this.text = var2.readString();
			}

			if (-3 == ~this.menuType || ~this.type == -3) {
				this.targetVerb = var2.readString();
				this.aClass94_243 = var2.readString();
				var6 = 63 & var2.readUnsignedShort();
				var5 |= var6 << 11;
			}

			if (this.menuType == 1 || this.menuType == 4 || -6 == ~this.menuType || this.menuType == 6) {
				this.tooltip = var2.readString();
				if (this.tooltip.length(-33) == 0) {
					if (~this.menuType == -2) {
						this.tooltip = Class115.aClass94_1583;
					}

					if (-5 == ~this.menuType) {
						this.tooltip = Class131.aClass94_1722;
					}

					if (5 == this.menuType) {
						this.tooltip = Class131.aClass94_1722;
					}

					if (this.menuType == 6) {
						this.tooltip = Class60.aClass94_935;
					}
				}
			}

			if (-2 == ~this.menuType || -5 == ~this.menuType || -6 == ~this.menuType) {
				var5 |= 4194304;
			}

			if (~this.menuType == -7) {
				var5 |= 1;
			}

			this.clickMask = new ClickMask(var5, -1);
		} catch (RuntimeException var12) {
			throw Class44.method1067(var12, "be.M(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ')');
		}
	}

	private void decode_if1_osrs(Buffer var1) {
		this.isIf3 = false;
		this.type = var1.readUnsignedByte();
		this.menuType = var1.readUnsignedByte();
		this.contentType = var1.readUnsignedShort();
		this.originalX = var1.readShort();
		this.originalY = var1.readShort();
		this.originalWidth = var1.readUnsignedShort();
		this.originalHeight = var1.readUnsignedShort();
		this.opacity = var1.readUnsignedByte();
		this.parentId = var1.readUnsignedShort();
		if (this.parentId == 0xFFFF) {
			this.parentId = -1;
		} else {
			this.parentId += this.hash & ~0xFFFF;
		}

		this.hoveredSiblingId = var1.readUnsignedShort();
		if (this.hoveredSiblingId == 0xFFFF) {
			this.hoveredSiblingId = -1;
		}

		int var2 = var1.readUnsignedByte();
		int var3;
		if (var2 > 0) {
			this.alternateOperators = new int[var2];
			this.alternateRhs = new int[var2];

			for (var3 = 0; var3 < var2; ++var3) {
				this.alternateOperators[var3] = var1.readUnsignedByte();
				this.alternateRhs[var3] = var1.readUnsignedShort();
			}
		}

		var3 = var1.readUnsignedByte();
		int var4;
		int var5;
		int var6;
		if (var3 > 0) {

			this.clientScripts = new int[var3][];
			for (var5 = 0; ~var3 < ~var5; ++var5) {
				var6 = var1.readUnsignedShort();
				this.clientScripts[var5] = new int[var6];
				int var7;
				for (var7 = 0; ~var7 > ~var6; ++var7) {
					this.clientScripts[var5][var7] = var1.readUnsignedShort();
					if (~this.clientScripts[var5][var7] == -65536) {
						this.clientScripts[var5][var7] = -1;
					}
				}
			}
			/*
			 * for (var4 = 0; var4 < var3; ++var4) { var5 = var1.readUnsignedShort(); int[]
			 * bytecode = new int[var5];
			 * 
			 * for (var6 = 0; var6 < var5; ++var6) { bytecode[var6] =
			 * var1.readUnsignedShort(); if (bytecode[var6] == 0xFFFF) { bytecode[var6] =
			 * -1; }
			 * 
			 * List<ClientScript1Instruction> instructions = new ArrayList<>(); for (int i =
			 * 0; i < bytecode.length;) { ClientScript1Instruction ins = new
			 * ClientScript1Instruction();
			 * 
			 * ins.opcode = ClientScript1Instruction.Opcode.values()[bytecode[i++]];
			 * 
			 * int ac = ins.opcode.argumentCount; ins.operands =
			 * Arrays.copyOfRange(bytecode, i, i + ac);
			 * 
			 * instructions.add(ins); i += ac; } this.clientScripts[var4] =
			 * instructions.toArray(new ClientScript1Instruction[0]); } }
			 */
		}

		if (this.type == 0) {
			this.scrollHeight = var1.readUnsignedShort();
			this.isHidden = var1.readUnsignedByte() == 1;
		}

		if (this.type == 1) {
			var1.readUnsignedShort();
			var1.readUnsignedByte();
		}

		if (this.type == 2) {
			int clickMask = 0;
			this.itemIds = new int[this.originalWidth * this.originalHeight];
			this.itemQuantities = new int[this.originalHeight * this.originalWidth];
			var4 = var1.readUnsignedByte();
			if (var4 == 1) {
				clickMask |= 268435456;
			}

			var5 = var1.readUnsignedByte();
			if (var5 == 1) {
				clickMask |= 1073741824;
			}

			var6 = var1.readUnsignedByte();
			if (var6 == 1) {
				clickMask |= Integer.MIN_VALUE;
			}

			int var7 = var1.readUnsignedByte();
			if (var7 == 1) {
				clickMask |= 536870912;
			}

			this.xPitch = var1.readUnsignedByte();
			this.yPitch = var1.readUnsignedByte();
			this.xOffsets = new int[20];
			this.yOffsets = new int[20];
			this.sprites = new int[20];

			int var8;
			for (var8 = 0; var8 < 20; ++var8) {
				int var9 = var1.readUnsignedByte();
				if (var9 == 1) {
					this.xOffsets[var8] = var1.readShort();
					this.yOffsets[var8] = var1.readShort();
					this.sprites[var8] = var1.readInt();
				} else {
					this.sprites[var8] = -1;
				}
			}

			this.configActions = new RSString[5];

			for (var8 = 0; var8 < 5; ++var8) {
				RSString var11 = var1.readString();
				if (var11.length(-33) > 0) {
					this.configActions[var8] = var11;
					clickMask |= 1 << var8 + 23;
				}
			}
			this.clickMask = new ClickMask(clickMask, 1);
		}

		if (this.type == 3) {
			this.filled = var1.readUnsignedByte() == 1;
		}

		if (this.type == 4 || this.type == 1) {
			this.xTextAlignment = var1.readUnsignedByte();
			this.yTextAlignment = var1.readUnsignedByte();
			this.lineHeight = var1.readUnsignedByte();
			this.fontId = var1.readUnsignedShort();
			if (this.fontId == 0xFFFF) {
				this.fontId = -1;
			}

			this.textShadowed = var1.readUnsignedByte() == 1;
		}

		if (this.type == 4) {
			this.text = var1.readString();
			this.alternateText = var1.readString();
		}

		if (this.type == 1 || this.type == 3 || this.type == 4) {
			this.textColor = var1.readInt();
		}

		if (this.type == 3 || this.type == 4) {
			this.alternateTextColor = var1.readInt();
			this.hoveredTextColor = var1.readInt();
			this.alternateHoveredTextColor = var1.readInt();
		}

		if (this.type == 5) {
			this.spriteId = var1.readInt();
			this.alternateSpriteId = var1.readInt();
		}

		if (this.type == 6) {
			this.modelType = 1;
			this.modelId = var1.readUnsignedShort();
			if (this.modelId == 0xFFFF) {
				this.modelId = -1;
			}

			this.alternateModelId = var1.readUnsignedShort();
			if (this.alternateModelId == 0xFFFF) {
				this.alternateModelId = -1;
			}

			this.animation = var1.readUnsignedShort();
			if (this.animation == 0xFFFF) {
				this.animation = -1;
			}

			this.alternateAnimation = var1.readUnsignedShort();
			if (this.alternateAnimation == 0xFFFF) {
				this.alternateAnimation = -1;
			}

			this.modelZoom = var1.readUnsignedShort();
			this.rotationX = var1.readUnsignedShort();
			this.rotationZ = var1.readUnsignedShort();
		}

		if (this.type == 7) {
			this.itemIds = new int[this.originalWidth * this.originalHeight];
			this.itemQuantities = new int[this.originalWidth * this.originalHeight];
			this.xTextAlignment = var1.readUnsignedByte();
			this.fontId = var1.readUnsignedShort();
			if (this.fontId == 0xFFFF) {
				this.fontId = -1;
			}

			this.textShadowed = var1.readUnsignedByte() == 1;
			this.textColor = var1.readInt();
			this.xPitch = var1.readShort();
			this.yPitch = var1.readShort();
			var4 = var1.readUnsignedByte();
			int clickMask = 0;
			if (var4 == 1) {
				clickMask |= 1073741824;
			}

			this.configActions = new RSString[5];

			for (var5 = 0; var5 < 5; ++var5) {
				RSString var10 = var1.readString();
				if (var10.length(-33) > 0) {
					this.configActions[var5] = var10;
					clickMask |= 1 << var5 + 23;
					this.clickMask = new ClickMask(clickMask, 1);
				}
			}
		}

		if (this.type == 8) {
			this.text = var1.readString();
		}
		int clickMask = 0;
		if (this.menuType == 2 || this.type == 2) {
			this.targetVerb = var1.readString();
			var1.readString(); // spellname
			var4 = var1.readUnsignedShort() & 63;
			clickMask |= var4 << 11;
			this.clickMask = new ClickMask(clickMask, 1);
		}

		if (this.menuType == 1 || this.menuType == 4 || this.menuType == 5 || this.menuType == 6) {
			this.tooltip = var1.readString();
			if (this.tooltip.length(-33) == 0) {
				if (~this.menuType == -2) {
					this.tooltip = Class115.aClass94_1583;
				}

				if (-5 == ~this.menuType) {
					this.tooltip = Class131.aClass94_1722;
				}

				if (5 == this.menuType) {
					this.tooltip = Class131.aClass94_1722;
				}

				if (this.menuType == 6) {
					this.tooltip = Class60.aClass94_935;
				}
			}
		}
		clickMask = 0;
		if (this.menuType == 1 || this.menuType == 4 || this.menuType == 5) {
			clickMask |= 4194304;
			this.clickMask = new ClickMask(clickMask, 1);
		}

		if (this.menuType == 6) {
			clickMask |= 1;
			this.clickMask = new ClickMask(clickMask, 1);
		}

	}

	final Class3_Sub28_Sub16 method859(boolean var1, int var2) {
		try {
			Applet_Sub1.aBoolean6 = false;
			if (!var1) {
				return (Class3_Sub28_Sub16) null;
			} else if (~var2 <= -1 && var2 < this.sprites.length) {
				int var3 = this.sprites[var2];
				if (~var3 != 0) {
					Class3_Sub28_Sub16 var4 = (Class3_Sub28_Sub16) Class114.aClass93_1569.get((long) var3, (byte) 121);
					if (var4 == null) {
						var4 = Class3_Sub28_Sub11.method602(0, var3, (byte) -18, get_widget_sprite_data());
						if (null != var4) {
							Class114.aClass93_1569.put((byte) -126, var4, (long) var3);
						} else {
							Applet_Sub1.aBoolean6 = true;
						}

						return var4;
					} else {
						return var4;
					}
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (RuntimeException var5) {
			throw Class44.method1067(var5, "be.I(" + var1 + ',' + var2 + ')');
		}
	}

	public static void method860(int var0) {
		try {
			aClass94_297 = null;
			aClass94_209 = null;
			if (var0 < 63) {
				method860(42);
			}

			aClass94_251 = null;
		} catch (RuntimeException var2) {
			throw Class44.method1067(var2, "be.F(" + var0 + ')');
		}
	}

	static final int method861(int var0, int var1, int var2) {
		try {
			Class3_Sub25 var3 = (Class3_Sub25) Class3_Sub2.aClass130_2220.method1780((long) var0, 0);
			return null == var3 ? -1
					: (0 <= var2 && var2 < var3.anIntArray2547.length ? (var1 < 39 ? -69 : var3.anIntArray2547[var2])
							: -1);
		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "be.J(" + var0 + ',' + var1 + ',' + var2 + ')');
		}
	}

	private final Object[] decodeListener(Buffer var1) {
		try {
			int var2 = var1.readUnsignedByte();
			if (var2 == 0) {
				return null;
			} else {
				Object[] var3 = new Object[var2];

				for (int var4 = 0; var4 < var2; ++var4) {
					int var5 = var1.readUnsignedByte();
					if (var5 == 0) {
						var3[var4] = new Integer(var1.readInt());
					} else if (var5 == 1) {
						var3[var4] = var1.readString();
					}
				}

				aBoolean195 = true;
				return var3;
			}
		} catch (Exception e) {
			System.err.println("Error decoding listeners");
			return null;
		}
	}

	private final int[] decodeTriggers(Buffer var1) {
		try {
			int var2 = var1.readUnsignedByte();
			if (var2 == 0) {
				return null;
			} else {
				int[] var3 = new int[var2];

				for (int var4 = 0; var4 < var2; ++var4) {
					var3[var4] = var1.readInt();
				}

				return var3;
			}
		} catch (Exception e) {
			System.err.println("Error decoding triggers");
			return null;
		}
	}

	final void method864(int var1, int var2, int var3) {
		try {
			int var4 = this.itemQuantities[var2];
			this.itemQuantities[var2] = this.itemQuantities[var1];
			this.itemQuantities[var1] = var4;
			var4 = this.itemIds[var2];
			this.itemIds[var2] = this.itemIds[var1];
			this.itemIds[var1] = var4;
		} catch (RuntimeException var5) {
			throw Class44.method1067(var5, "be.L(" + var1 + ',' + var2 + ',' + var3 + ')');
		}
	}

	final Class140_Sub1 method865(int var1, Class142 var2, int var3, int var4, int var5, boolean var6, Class52 var7) {
		try {
			Applet_Sub1.aBoolean6 = false;
			int var8;
			int var9;
			if (var6) {
				var8 = this.anInt294;
				var9 = this.alternateModelId;
			} else {
				var9 = this.modelId;
				var8 = this.modelType;
			}

			if (var4 < 125) {
				return (Class140_Sub1) null;
			} else if (-1 != ~var8) {
				if (-2 == ~var8 && var9 == -1) {
					return null;
				} else {
					Class140_Sub1 var10;
					if (1 == var8) {
						var10 = (Class140_Sub1) Class3_Sub15.aClass93_2428.get((long) ((var8 << 16) - -var9),
								(byte) 121);
						if (var10 == null) {
							Class140_Sub5 var18 = Class140_Sub5.method2015(get_widget_model_data(), var9, 0);
							if (var18 == null) {
								Applet_Sub1.aBoolean6 = true;
								return null;
							}

							var10 = var18.method2008(64, 768, -50, -10, -50);
							Class3_Sub15.aClass93_2428.put((byte) -115, var10, (long) (var9 + (var8 << 16)));
						}

						if (var2 != null) {
							var10 = var2.method2055(var10, (byte) 119, var1, var5, var3);
						}

						return var10;
					} else if (var8 != 2) {
						if (3 != var8) {
							if (4 == var8) {
								ItemDefinition var16 = ItemDefinition.getItemDefinition(var9, (byte) 94);
								Class140_Sub1 var17 = var16.method1110(110, var1, var5, var2, 10, var3);
								if (var17 != null) {
									return var17;
								} else {
									Applet_Sub1.aBoolean6 = true;
									return null;
								}
							} else if (var8 != 6) {
								if (~var8 != -8) {
									return null;
								} else if (var7 != null) {
									int var15 = this.modelId >>> 16;
									int var11 = this.modelId & '\uffff';
									int var12 = this.anInt265;
									Class140_Sub1 var13 = var7.method1157(var1, var12, var15, var5, var2, var3, var11,
											-2012759707);
									if (var13 == null) {
										Applet_Sub1.aBoolean6 = true;
										return null;
									} else {
										return var13;
									}
								} else {
									return null;
								}
							} else {
								var10 = Node.method522(var9, 27112).method1476((Class145[]) null, 0, (byte) -120, 0,
										var1, var5, var3, (Class142) null, 0, var2);
								if (null != var10) {
									return var10;
								} else {
									Applet_Sub1.aBoolean6 = true;
									return null;
								}
							}
						} else if (null == var7) {
							return null;
						} else {
							var10 = var7.method1167(var5, (byte) 127, var2, var3, var1);
							if (null == var10) {
								Applet_Sub1.aBoolean6 = true;
								return null;
							} else {
								return var10;
							}
						}
					} else {
						var10 = Node.method522(var9, 27112).method1482(var2, var5, var1, 27, var3);
						if (null != var10) {
							return var10;
						} else {
							Applet_Sub1.aBoolean6 = true;
							return null;
						}
					}
				}
			} else {
				return null;
			}
		} catch (RuntimeException var14) {
			throw Class44.method1067(var14, "be.E(" + var1 + ',' + (var2 != null ? "{...}" : "null") + ',' + var3 + ','
					+ var4 + ',' + var5 + ',' + var6 + ',' + (var7 != null ? "{...}" : "null") + ')');
		}
	}

	final Class3_Sub28_Sub16 method866(byte var1, boolean var2) {
		try {
			Applet_Sub1.aBoolean6 = false;
			int var3;
			if (var2) {
				var3 = this.alternateSpriteId;
			} else {
				var3 = this.spriteId;
			}

			if (0 == ~var3) {
				return null;
			} else {
				long var4 = ((this.flippedVertically ? 1L : 0L) << 38) + ((!this.field2578 ? 0L : 1L) << 35)
						+ (long) var3 + ((long) this.borderType << 36) + ((this.flippedHorizontally ? 1L : 0L) << 39)
						+ ((long) this.shadowColor << 40);
				Class3_Sub28_Sub16 var6 = (Class3_Sub28_Sub16) Class114.aClass93_1569.get(var4, (byte) 121);
				if (var6 != null) {
					return var6;
				} else {
					Class3_Sub28_Sub16_Sub2 var7;
					if (this.field2578) {
						var7 = Class3_Sub28_Sub7.method562(get_widget_sprite_data(), 0, var3, (byte) 39);
					} else {
						var7 = Class40.method1043(0, get_widget_sprite_data(), -3178, var3);
					}

					if (null == var7) {
						Applet_Sub1.aBoolean6 = true;
						return null;
					} else if (var1 != -113) {
						return (Class3_Sub28_Sub16) null;
					} else {
						if (this.flippedVertically) {
							var7.method663();
						}

						if (this.flippedHorizontally) {
							var7.method653();
						}

						if (this.borderType > 0) {
							var7.method652(this.borderType);
						}

						if (~this.borderType <= -2) {
							var7.method657(1);
						}

						if (2 <= this.borderType) {
							var7.method657(16777215);
						}

						if (this.shadowColor != 0) {
							var7.method668(this.shadowColor);
						}

						Object var9;
						if (Class138.aBoolean1807) {
							if (!(var7 instanceof Class3_Sub28_Sub16_Sub2_Sub1)) {
								var9 = new Class3_Sub28_Sub16_Sub1(var7);
							} else {
								var9 = new Class3_Sub28_Sub16_Sub1_Sub1(var7);
							}
						} else {
							var9 = var7;
						}

						Class114.aClass93_1569.put((byte) -75, var9, var4);
						return (Class3_Sub28_Sub16) var9;
					}
				}
			}
		} catch (RuntimeException var8) {
			throw Class44.method1067(var8, "be.O(" + var1 + ',' + var2 + ')');
		}
	}

	final void decode_if3_530(Buffer buffer) {
		try {
			this.isIf3 = true;
			++buffer.pos;
			this.type = buffer.readUnsignedByte();
			if (-1 != ~(128 & this.type)) {
				this.type &= 127;
				buffer.readString();
			}

			this.contentType = buffer.readUnsignedShort();
			this.originalX = buffer.readShort();
			this.originalY = buffer.readShort();
			this.originalWidth = buffer.readUnsignedShort();
			this.originalHeight = buffer.readUnsignedShort();
			this.widthMode = buffer.readByte();
			this.heightMode = buffer.readByte();
			this.xPositionMode = buffer.readByte();
			this.yPositionMode = buffer.readByte();
			this.parentId = buffer.readUnsignedShort();
			if (-65536 == ~this.parentId) {
				this.parentId = -1;
			} else {
				this.parentId = (this.hash & -65536) - -this.parentId;
			}

			this.isHidden = -2 == ~buffer.readUnsignedByte();
			if (~this.type == -1) {
				this.scrollWidth = buffer.readUnsignedShort();
				this.scrollHeight = buffer.readUnsignedShort();
				this.noClickThrough = -2 == ~buffer.readUnsignedByte();
			}
			int var1 = 0;
			int clickMask;
			if (~this.type == -6) {

				this.spriteId = buffer.readInt();
				this.textureId = buffer.readUnsignedShort();
				clickMask = buffer.readUnsignedByte();
				this.field2578 = -1 != ~(2 & clickMask);
				this.spriteTiling = ~(1 & clickMask) != -1;
				this.opacity = buffer.readUnsignedByte();
				this.borderType = buffer.readUnsignedByte();
				this.shadowColor = buffer.readInt();
				this.flippedVertically = ~buffer.readUnsignedByte() == -2;
				this.flippedHorizontally = 1 == buffer.readUnsignedByte();
			}

			if (~this.type == -7) {
				this.modelType = 1;
				this.modelId = buffer.readUnsignedShort();
				if (~this.modelId == -65536) {
					this.modelId = -1;
				}

				this.offsetX2d = buffer.readShort();
				this.offsetY2d = buffer.readShort();
				this.rotationX = buffer.readUnsignedShort();
				this.rotationZ = buffer.readUnsignedShort();
				this.rotationY = buffer.readUnsignedShort();
				this.modelZoom = buffer.readUnsignedShort();
				this.animation = buffer.readUnsignedShort();
				if ('\uffff' == this.animation) {
					this.animation = -1;
				}

				this.orthogonal = buffer.readUnsignedByte() == 1;
				this.aShort293 = (short) buffer.readUnsignedShort();
				this.aShort169 = (short) buffer.readUnsignedShort();
				this.aBoolean309 = 1 == buffer.readUnsignedByte();
				if (this.widthMode != 0) {
					this.modelHeightOverride = buffer.readUnsignedShort();
				}

				if (this.heightMode != 0) {
					this.field2683 = buffer.readUnsignedShort();
				}
			}

			if (~this.type == -5) {
				this.fontId = buffer.readUnsignedShort();
				if (~this.fontId == -65536) {
					this.fontId = -1;
				}

				this.text = buffer.readString();
				this.lineHeight = buffer.readUnsignedByte();
				this.xTextAlignment = buffer.readUnsignedByte();
				this.yTextAlignment = buffer.readUnsignedByte();
				this.textShadowed = buffer.readUnsignedByte() == 1;
				this.textColor = buffer.readInt();
			}

			if (this.type == 3) {
				this.textColor = buffer.readInt();
				this.filled = 1 == buffer.readUnsignedByte();
				this.opacity = buffer.readUnsignedByte();
			}

			if (-10 == ~this.type) {
				this.lineWidth = buffer.readUnsignedByte();
				this.textColor = buffer.readInt();
				this.lineDirection = 1 == buffer.readUnsignedByte();
			}

			clickMask = buffer.getTriByte((byte) 87);
			int var4 = buffer.readUnsignedByte();
			int var5;
			if (var4 != 0) {
				this.anIntArray299 = new int[10];
				this.aByteArray263 = new byte[10];

				for (this.aByteArray231 = new byte[10]; ~var4 != -1; var4 = buffer.readUnsignedByte()) {
					var5 = (var4 >> 4) - 1;
					var4 = buffer.readUnsignedByte() | var4 << 8;
					var4 &= 4095;
					if (4095 == var4) {
						this.anIntArray299[var5] = -1;
					} else {
						this.anIntArray299[var5] = var4;
					}

					this.aByteArray263[var5] = buffer.readByte();
					this.aByteArray231[var5] = buffer.readByte();
				}
			}

			this.name = buffer.readString();
			var5 = buffer.readUnsignedByte();
			int var6 = var5 & 15;
			int var8;
			if (0 < var6) {
				this.actions = new RSString[var6];

				for (var8 = 0; var6 > var8; ++var8) {
					this.actions[var8] = buffer.readString();
				}
			}

			int var7 = var5 >> 4;
			if (var7 > 0) {
				var8 = buffer.readUnsignedByte();
				this.anIntArray249 = new int[var8 + 1];

				for (int var9 = 0; var9 < this.anIntArray249.length; ++var9) {
					this.anIntArray249[var9] = -1;
				}

				this.anIntArray249[var8] = buffer.readUnsignedShort();
			}

			if (1 < var7) {
				var8 = buffer.readUnsignedByte();
				this.anIntArray249[var8] = buffer.readUnsignedShort();
			}

			this.dragDeadZone = buffer.readUnsignedByte();
			this.dragDeadTime = buffer.readUnsignedByte();
			this.dragRenderBehavior = buffer.readUnsignedByte() == 1;
			var8 = var1;
			this.targetVerb = buffer.readString();
			if (0 != Script.method630((byte) -34, clickMask)) {
				var8 = buffer.readUnsignedShort();
				this.anInt266 = buffer.readUnsignedShort();
				if (-65536 == ~var8) {
					var8 = -1;
				}

				if ('\uffff' == this.anInt266) {
					this.anInt266 = -1;
				}

				this.anInt238 = buffer.readUnsignedShort();
				if (this.anInt238 == '\uffff') {
					this.anInt238 = -1;
				}
			}

			this.clickMask = new ClickMask(clickMask, var8);
			this.onLoadListener = this.decodeListener(buffer);
			this.onMouseOverListener = this.decodeListener(buffer);
			this.onMouseLeaveListener = this.decodeListener(buffer);
			this.onTargetLeaveListener = this.decodeListener(buffer);
			this.onTargetEnterListener = this.decodeListener(buffer);
			this.onVarTransmitListener = this.decodeListener(buffer);
			this.onInvTransmitListener = this.decodeListener(buffer);
			this.onStatTransmitListener = this.decodeListener(buffer);
			this.onTimerListener = this.decodeListener(buffer);
			this.onOpListener = this.decodeListener(buffer);
			this.onMouseRepeatListener = this.decodeListener(buffer);
			this.onClickListener = this.decodeListener(buffer);
			this.onClickRepeatListener = this.decodeListener(buffer);
			this.onReleaseListener = this.decodeListener(buffer);
			this.onHoldListener = this.decodeListener(buffer);
			this.onDragListener = this.decodeListener(buffer);
			this.onDragCompleteListener = this.decodeListener(buffer);
			this.onScrollWheelListener = this.decodeListener(buffer);
			this.anObjectArray161 = this.decodeListener(buffer);
			this.anObjectArray221 = this.decodeListener(buffer);
			this.varTransmitTriggers = this.decodeTriggers(buffer);
			this.invTransmitTriggers = this.decodeTriggers(buffer);
			this.statTransmitTriggers = this.decodeTriggers(buffer);
			this.anIntArray211 = this.decodeTriggers(buffer);
			this.anIntArray185 = this.decodeTriggers(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	final void decode_if3_osrs(Buffer buffer) {
		buffer.readUnsignedByte();
		this.isIf3 = true;
		this.type = buffer.readUnsignedByte();
		this.contentType = buffer.readUnsignedShort();
		this.originalX = buffer.readShort();
		this.originalY = buffer.readShort();
		this.originalWidth = buffer.readUnsignedShort();
		if (this.type == 9) {
			this.originalHeight = buffer.readShort();
		} else {
			this.originalHeight = buffer.readUnsignedShort();
		}

		this.widthMode = buffer.readByte();
		this.heightMode = buffer.readByte();
		this.xPositionMode = buffer.readByte();
		this.yPositionMode = buffer.readByte();
		this.parentId = buffer.readUnsignedShort();
		if (this.parentId == 0xFFFF) {
			this.parentId = -1;
		} else {
			this.parentId += this.hash & ~0xFFFF;
		}

		this.isHidden = buffer.readUnsignedByte() == 1;
		if (this.type == 0) {
			this.scrollWidth = buffer.readUnsignedShort();
			this.scrollHeight = buffer.readUnsignedShort();
			this.noClickThrough = buffer.readUnsignedByte() == 1;
		}

		if (this.type == 5) {
			this.spriteId = buffer.readInt();
			this.textureId = buffer.readUnsignedShort();
			this.spriteTiling = buffer.readUnsignedByte() == 1;
			this.opacity = buffer.readUnsignedByte();
			this.borderType = buffer.readUnsignedByte();
			this.shadowColor = buffer.readInt();
			this.flippedVertically = buffer.readUnsignedByte() == 1;
			this.flippedHorizontally = buffer.readUnsignedByte() == 1;
		}

		if (this.type == 6) {
			this.modelType = 1;
			this.modelId = buffer.readUnsignedShort();
			if (this.modelId == 0xFFFF) {
				this.modelId = -1;
			}

			this.offsetX2d = buffer.readShort();
			this.offsetY2d = buffer.readShort();
			this.rotationX = buffer.readUnsignedShort();
			this.rotationZ = buffer.readUnsignedShort();
			this.rotationY = buffer.readUnsignedShort();
			this.modelZoom = buffer.readUnsignedShort();
			this.animation = buffer.readUnsignedShort();
			if (this.animation == 0xFFFF) {
				this.animation = -1;
			}

			this.orthogonal = buffer.readUnsignedByte() == 1;
			buffer.readUnsignedShort();
			if (this.widthMode != 0) {
				this.modelHeightOverride = buffer.readUnsignedShort();
			}

			if (this.heightMode != 0) {
				buffer.readUnsignedShort();
			}
		}

		if (this.type == 4) {
			this.fontId = buffer.readUnsignedShort();
			if (this.fontId == 0xFFFF) {
				this.fontId = -1;
			}

			this.text = buffer.readString();
			this.lineHeight = buffer.readUnsignedByte();
			this.xTextAlignment = buffer.readUnsignedByte();
			this.yTextAlignment = buffer.readUnsignedByte();
			this.textShadowed = buffer.readUnsignedByte() == 1;
			this.textColor = buffer.readInt();
		}

		if (this.type == 3) {
			this.textColor = buffer.readInt();
			this.filled = buffer.readUnsignedByte() == 1;
			this.opacity = buffer.readUnsignedByte();
		}

		if (this.type == 9) {
			this.lineWidth = buffer.readUnsignedByte();
			this.textColor = buffer.readInt();
			this.lineDirection = buffer.readUnsignedByte() == 1;
		}

		this.clickMask = new ClickMask(buffer.method5674((short) 3112), 1);
		this.name = buffer.readString();
		int var2 = buffer.readUnsignedByte();
		if (var2 > 0) {
			this.actions = new RSString[var2];

			for (int var3 = 0; var3 < var2; ++var3) {
				this.actions[var3] = buffer.readString();
			}
		}

		this.dragDeadZone = buffer.method5547((byte)104);
	      this.dragDeadTime = buffer.method5547((byte)65);
	      this.dragRenderBehavior = buffer.method5547((byte)109) == 1;
	      this.targetVerb = buffer.method5667(-1640765421);
	      this.onLoadListener = this.decodeListener(buffer);
	      this.onMouseOverListener = this.decodeListener(buffer);
	      this.onMouseLeaveListener = this.decodeListener(buffer);
	      this.onTargetLeaveListener = this.decodeListener(buffer);
	      this.onTargetEnterListener = this.decodeListener(buffer);
	      this.onVarTransmitListener = this.decodeListener(buffer);
	      this.onInvTransmitListener = this.decodeListener(buffer);
	      this.onStatTransmitListener = this.decodeListener(buffer);
	      this.onTimerListener = this.decodeListener(buffer);
	      this.onOpListener = this.decodeListener(buffer);
	      this.onMouseRepeatListener = this.decodeListener(buffer);
	      this.onClickListener = this.decodeListener(buffer);
	      this.onClickRepeatListener = this.decodeListener(buffer);
	      this.onReleaseListener = this.decodeListener(buffer);
	      this.onHoldListener = this.decodeListener(buffer);
	      this.onDragListener = this.decodeListener(buffer);
	      this.onDragCompleteListener = this.decodeListener(buffer);
	      this.onScrollWheelListener = this.decodeListener(buffer);
	      this.varTransmitTriggers = this.decodeTriggers(buffer);
	      this.invTransmitTriggers = this.decodeTriggers(buffer);
	      this.statTransmitTriggers = this.decodeTriggers(buffer);
	}

	final Class3_Sub28_Sub17 method868(Class109[] var1, int var2) {
		try {
			Applet_Sub1.aBoolean6 = false;
			if (0 == ~this.fontId) {
				return null;
			} else {
				Class3_Sub28_Sub17 var3 = (Class3_Sub28_Sub17) Class47.aClass93_743.get((long) this.fontId, (byte) 121);
				if (null != var3) {
					return var3;
				} else {
					var3 = Class73.method1300(var2, this.fontId, (byte) 127, get_widget_sprite_data(),
							widget_font_data_530);
					if (null == var3) {
						Applet_Sub1.aBoolean6 = true;
					} else {
						var3.method697(var1, (int[]) null);
						Class47.aClass93_743.put((byte) -77, var3, (long) this.fontId);
					}

					return var3;
				}
			}
		} catch (RuntimeException var4) {
			throw Class44.method1067(var4, "be.A(" + (var1 != null ? "{...}" : "null") + ',' + var2 + ')');
		}
	}

	static final Widget get_widget(int interfaceHash) {
		try {
			int windowId = interfaceHash >> 16;

			int componentId = '\uffff' & interfaceHash;
			if (Class140.aClass11ArrayArray1834.length <= windowId || windowId < 0) {
				return null;
			}
			if (Class140.aClass11ArrayArray1834[windowId] == null
					|| Class140.aClass11ArrayArray1834[windowId].length <= componentId
					|| null == Class140.aClass11ArrayArray1834[windowId][componentId]) {
				boolean var4 = loaded_widget(windowId, 104);
				if (!var4) {
					return null;
				}
			}
			return Class140.aClass11ArrayArray1834[windowId][componentId];
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	static final boolean loaded_widget(int widget_id, int var1) {
		try {
			if (!interfaceLoaded[widget_id]) {
				if (get_widget_data(widget_id).allFilesComplete(-99, widget_id)) {
					int fileAmount = get_widget_data(widget_id).getChildCount(widget_id, (byte) 94);
					if (fileAmount == 0) {
						interfaceLoaded[widget_id] = true;
						return true;
					} else {
						if (null == Class140.aClass11ArrayArray1834[widget_id]) {
							Class140.aClass11ArrayArray1834[widget_id] = new Widget[fileAmount];
						}

						for (int index = 0; index < fileAmount; ++index) {
							if (null == Class140.aClass11ArrayArray1834[widget_id][index]) {
								byte[] widget_data;
								if (Widget.osrs(widget_id)) {
									widget_data = get_widget_data(widget_id).getFile(widget_id, index);
								} else {
									widget_data = get_widget_data(widget_id).getFile(widget_id, index);
								}
								if (widget_data != null) {
									Widget widget = Class140.aClass11ArrayArray1834[widget_id][index] = new Widget();
									widget.hash = index + (widget_id << 16);
									if (widget_data[0] == -1) {
										if (Widget.osrs(widget_id)) {
											widget.decode_if3_osrs(new Buffer(widget_data));
										} else {
											widget.decode_if3_530(new Buffer(widget_data));
										}
									} else {
										if (Widget.osrs(widget_id)) {
											widget.decode_if1_osrs(new Buffer(widget_data));
										} else {
											widget.decode_if1_530(new Buffer(widget_data));
										}
									}
								} else {
									System.out.println("widget data is null");
								}
							}
						}

						interfaceLoaded[widget_id] = true;
						if (var1 != 104) {
							Canvas_Sub2.parsePlayerMasks(100);
						}

						return true;
					}
				} else {
					return false;
				}
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	static final int method869(int var0, int var1) {
		try {
			return ~var1 != -16711936 ? (var0 < 97 ? -63 : Class56.method1186(0, var1)) : -1;
		} catch (RuntimeException var3) {
			throw Class44.method1067(var3, "be.D(" + var0 + ',' + var1 + ')');
		}
	}

	public Widget() {
		this.aClass94_243 = Class104.aClass94_2171;
		this.aBoolean163 = false;
		this.yTextAlignment = 0;
		this.hoveredSiblingId = -1;
		this.lineDirection = false;
		this.anInt266 = -1;
		this.heightMode = 0;
		this.scrollHeight = 0;
		this.dragRenderBehavior = false;
		this.textShadowed = false;
		this.anInt204 = -1;
		this.anInt260 = 1;
		this.hoveredTextColor = 0;
		this.isIf3 = false;
		this.clickMask = Class158_Sub1.aClass3_Sub1_2980;
		this.alternateTextColor = 0;
		this.text = Class104.aClass94_2171;
		this.field2582 = 0;
		this.field2646 = 0;
		this.noClickThrough = false;
		this.alternateModelId = -1;
		this.parentId = -1;
		this.anInt216 = 1;
		this.anInt192 = -1;
		this.alternateHoveredTextColor = 0;
		this.anInt264 = 0;
		this.name = Class104.aClass94_2171;
		this.anInt284 = 0;
		this.originalWidth = 0;
		this.xPitch = 0;
		this.anInt234 = -1;
		this.field2578 = false;
		this.modelHeightOverride = 0;
		this.opacity = 0;
		this.field2914 = 0;
		this.targetVerb = Class104.aClass94_2171;
		this.anInt237 = 0;
		this.alternateText = Class104.aClass94_2171;
		this.borderType = 0;
		this.anInt265 = -1;
		this.anInt242 = 0;
		this.offsetX2d = 0;
		this.yPitch = 0;
		this.originalHeight = 0;
		this.hash = -1;
		this.alternateSpriteId = -1;
		this.xPositionMode = 0;
		this.anInt267 = 0;
		this.fontId = -1;
		this.scrollWidth = 0;
		this.anInt255 = 0;
		this.aShort293 = 0;
		this.textureId = 0;
		this.animation = -1;
		this.tooltip = Class115.aClass94_1583;
		this.rotationY = 0;
		this.anInt271 = 0;
		this.anInt292 = -1;
		this.contentType = 0;
		this.shadowColor = 0;
		this.field2648 = null;
		this.anInt311 = 0;
		this.modelType = 1;
		this.aBoolean309 = false;
		this.widthMode = 0;
		this.anInt294 = 1;
		this.field2683 = 0;
		this.rotationZ = 0;
		this.aBoolean195 = false;
		this.originalX = 0;
		this.field2580 = 0;
		this.originalY = 0;
		this.aBoolean227 = true;
		this.anInt283 = 0;
		this.anInt213 = 0;
		this.textColor = 0;
		this.menuType = 0;
	}

}
