
public class JS5Group {
	
	public static final int INDEX_COUNT = 33;

	static int[] js5_index_weights = new int[]{4, 4, 1, 2, 6, 4, 2, 49, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

	static JS5Index index0;
	static JS5Index index1;
	static JS5Index index2;
	static JS5Index index3;
	static JS5Index index4;
	static JS5Index index5;
	static JS5Index index6;
	static JS5Index index7;
	static JS5Index index8;
	static JS5Index index9;
	static JS5Index index10;
	static JS5Index index11;
	static JS5Index index12;
	static JS5Index index13;
	static JS5Index index14;
	static JS5Index index15;
	static JS5Index index16;
	static JS5Index index17;
	static JS5Index index18;
	static JS5Index index19;
	static JS5Index index20;
	static JS5Index index21;
	static JS5Index index22;
	static JS5Index index23;
	static JS5Index index24;
	static JS5Index index25;
	static JS5Index index26;
	static JS5Index index27;
	static JS5Index index28;
	static JS5Index index29, index30, index31, index32;

}
